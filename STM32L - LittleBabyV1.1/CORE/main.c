/**********************************
说明：低功耗传感器主函数
作者：关宇晟
版本：V2018.4.3
***********************************/

#include "main.h"
#include "sys.h"

int main(void)
{	
	u8 i;
	
	static volatile SYSTEMCONFIG *p_sys;

	SysInit();	//系统外设、参数初始化
	
	p_sys = GetSystemConfig();
		
	while (1)
	{					
		Updata_IWDG(GetSystem100msCount());	//看门狗
		
		TaskForDebugCOM();		//串口测试

		if(g_ComTestFlag == FALSE)		//调试关闭
		{	
			switch(p_sys->Type)
			{
//				case NETTYPE_LORA:
//					LoraProcess(GetSystem10msCount());	//LORA 初始化					
//					TCProtocolForLoraProcess();
//					break;
				
				case NETTYPE_GPRS:			
					GPRS_Process(GetSystem10msCount());//GPRS直接上报						
					TCProtocolProcess();					
					LED_Blink(GetSystem10msCount());					
					break;
				
				case NETTYPE_SIM800C:			
					if(GetGPSTaskRunFlag())		//L76X  GPS定位
					{
						GPS_Process(GetSystem10msCount());
					}
					else
					{
						SIM800CProcess(GetSystem10msCount());//SIM800C直接上报
						
						TCProtocolProcess();									
					}
				
					LED_Blink(GetSystem10msCount());
					
					break;
				
				case NETTYPE_WLAN:
					
					if(GetGPSTaskRunFlag())		//L76X  GPS定位
					{
						GPS_Process(GetSystem10msCount());
					}
					else
					{
						WiFiProcess();
				
						TCProtocolProcess();									
					}
					
					LED_Blink(GetSystem10msCount());
				break;
				
				
				case NETTYPE_SIM7600CE:
					if(GetGPSTaskRunFlag())		//L76X  GPS定位
					{
						GPS_Process(GetSystem10msCount());
					}
					else
					{
						SIM7600CEProcess(GetSystem10msCount());//SIM7600CE直接上报
						
						TCProtocolProcess();					
					}
					
					LED_Blink(GetSystem10msCount());
					break;
					
				default :
					TaskForTCRxObser();
					break;
			}
		}
		else
		{
			if(g_Uart2RxFlag == TRUE)
			{			
				u1_printf("\r\nRec:");
				for(i=0; i<g_USART2_RX_CNT; i++)
				{
					u1_printf("%02X ", g_USART2_RX_BUF[i]);
				}
				u1_printf("\r\n");
				
				u1_printf("%s\r\n", g_USART2_RX_BUF);

				Clear_Uart2Buff();
			}
		}

		Power_Vol_Detect(GetSystem10msCount());	//ADC电池电压检测

		StopModeProcess();				//停止模式入口
		
		LowPower_Process();				//唤醒后其他外设初始化
				
		SHT_Process(GetSystem10msCount());		//SHT31温湿度
	
		MAX_Process(GetSystem10msCount());		//MAX44009光照度
	
		BMP_Process(GetSystem10msCount());		//BMP280大气压力
	}
}


