#include "main.h"
#include "SysInit.h"

SYSTEM_CTRL 	g_SysCtrl;
SYSTEMCONFIG	g_SystemConfig;
SYSTEM_INFO		g_SystemInfo;


static volatile u8 s_SysData[100] ;

void InitSystemConfig(void);
	
const SYSTEMCONFIG DefaultSystemCofig = 
{
	19040000,	   		//设备ID
	NETTYPE_SIM800C,	//网络连接类型
	9999,			//网络号
	9999,			//节点号
	9999,			//网关地址
	22,				//通信频道
	0,				//拓扑结构
	{47,106,119,252}, //IP
	6070,			//端口
	3,				//重发次数
	14400,				//心跳间隔时间间隔
	600,				//数据上报时间间隔
	600,				//状态上报时间间隔
	1,
	304,
};

static const SYSTEM_INFO default_sys_info = 
{
	SOFTWARE_VERSION,					//软件版本
	HARDWARE_VERSION,					//硬件版本
	{'2','0','1','9','1','2','0','4'},//序号
#if( HARDWARE_VERSION == HARDWARE_VERSION_V1_0 ) 
	{'Y','X','B','_','V','1','0','0'}

#elif( HARDWARE_VERSION == HARDWARE_VERSION_V1_2 )
	{'Y','X','B','_','V','1','2','0'}
#endif

};

SYSTEM_INFO 	*Get_SystemInfo(void)
{
	return g_SysCtrl.SysInfo;
}

void SetOnline( unsigned char ucData)
{
	g_SysCtrl.is_online = ucData;
}
void NVIC_Config(void)
{
	NVIC_InitTypeDef   NVIC_InitStructure;	
		/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
		/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1 ;//抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
}
//RCC_MSIRange_0:/*!< MSI = 65.536 KHz  */
//RCC_MSIRange_1:/*!< MSI = 131.072 KHz */
//RCC_MSIRange_2:/*!< MSI = 262.144 KHz */
//RCC_MSIRange_3: /*!< MSI = 524.288 KHz */
//RCC_MSIRange_4:/*!< MSI = 1.048 MHz   */
//RCC_MSIRange_5:/*!< MSI = 2.097 MHz   */
//RCC_MSIRange_6:/*!< MSI = 4.194 MHz   */

void SysInit(void)
{	
	RCC_ClocksTypeDef RCC_Clocks;
			
	SysClockForMSI(RCC_MSIRange_6);
	
	RCC_GetClocksFreq(&RCC_Clocks);	
	
	Delay_Init(SYS_CLOCK);
	
	STM32_GPIO_Init();
	
	IIC_Init();
	
	IIC_InterfaceReset();
	
	NVIC_Config();
	
	USART1_Config(115200);
	
	DMA_Config();
	
	u1_printf("\r\n SYSCLK_Frequency: %dHz\r\n",RCC_Clocks.SYSCLK_Frequency);
	
	u1_printf("\r\n Project Build Data: %s - %s \r\n",  __DATE__ , __TIME__); //编译日期
	
	Debug_Config_Init();
	
	RTC_Config_Init();	
		
	RTC_AlarmConfig();
	
	SetIWDG(26);
		
	SensorPowerReset();
	
	SHT_Init();
	
	MAX44009_Init();	
	
	BMP280_Init();
	
	Adc_Init();				
		
	ReadBQ24650Flag();		//读取太阳能充电芯片状态
	
	InitSystemConfig();		//初始化系统配置
	
	LogStorePointerInit();
	
	CommunicatinLogInit();
	
	Debug_Out("\r\n  STM32L151C8T6 Run...  \r\n");			
	
	TIM4_Init(10, SYS_CLOCK); //10ms产生一次定时器中断
}

void Wake_SysInit(void)
{	
	WakeUp_GPIO_Init();	
	
	USART1_Config(115200);	
		
	IIC_Init();
		
	TIM4_Init(10, SYS_CLOCK);
}


unsigned int Check_Area_Valid(unsigned int base_addr)
{
	SYS_TAG tag;
	unsigned char chksum;
	unsigned char data[4];
	
	EEPROM_ReadBytes(base_addr, data, 4);

	tag.magic = data[0];
	tag.length = (data[1]) + ((data[2]) << 8);
	tag.chksum = data[3] ;
	
	if (tag.magic != VAILD)
	{
//		u1_printf("VAILD\r\n");
		return 0;		
	}

	chksum = EEPROM_CheckSum((base_addr+sizeof(SYS_TAG)), tag.length);

	if (tag.chksum != chksum) 
	{
//		u1_printf("Check\r\n");
		return 0;
	}
	
	if(tag.length == 0)
	{
		return 1;
	}
	else
	{
		return tag.length;
	}
}
SYSTEMCONFIG * GetSystemConfig(void)
{
	return g_SysCtrl.SysConfig;
}
static void Init_SystemInfo(void)
{		
	g_SysCtrl.SysInfo = (SYSTEM_INFO *)&default_sys_info;
}

unsigned char Set_System_Config(SYSTEMCONFIG *SysConfig)
{
	SYSCFGSAVE SysCfgSave;
	
	SysCfgSave.magic = VAILD;
	SysCfgSave.length = sizeof(SYSTEMCONFIG);
	SysCfgSave.chksum = Calc_Checksum((unsigned char *)SysConfig, sizeof(SYSTEMCONFIG));

	if(SysConfig->Type > 10)
	{
		return FALSE;
	}
	if(SysConfig->Device_ID >= 0x10000000)
	{
		return FALSE;
	}	
	if(SysConfig->Channel > 32)
	{
		return FALSE;
	}

	if(SysConfig->Gateway != SysConfig->Net)
	{
		return FALSE;
	}

	memcpy((void *)&SysCfgSave.Device_ID, SysConfig, sizeof(SYSTEMCONFIG));

	if((sizeof(SYSCFGSAVE)%4) == 0)
	{
		EEPROM_WriteWords(SYSTEM_CFG_ADDR, (unsigned int *)&SysCfgSave, sizeof(SYSCFGSAVE)/4);
	}
	else
	{
		EEPROM_WriteWords(SYSTEM_CFG_ADDR, (unsigned int *)&SysCfgSave, sizeof(SYSCFGSAVE)/4+1);
	}
	
//	EEPROM_WriteBytes(SYSTEM_CFG_ADDR, (unsigned char *)&SysCfgSave, sizeof(SYSCFGSAVE));
	if (Check_Area_Valid(SYSTEM_CFG_ADDR))
	{
		EEPROM_ReadBytes(SYSTEM_CFG_ADDR, s_SysData, sizeof(SYSCFGSAVE));
		g_SysCtrl.SysConfig = (SYSTEMCONFIG *)(&s_SysData[4]);
	}
	else
	{
		//使用默认数据
		g_SysCtrl.SysConfig = (SYSTEMCONFIG *)&DefaultSystemCofig;
	}
	
	return TRUE;
}
static void System_Option_Init(void)
{
	u8  i;
	memset(&g_SysCtrl, 0, sizeof(SYSTEM_CTRL));
	
	//参数配置信息===============================================================

	if (Check_Area_Valid(SYSTEM_CFG_ADDR))
	{
		EEPROM_ReadBytes(SYSTEM_CFG_ADDR, s_SysData, sizeof(SYSCFGSAVE));
		g_SysCtrl.SysConfig = (SYSTEMCONFIG *)(&s_SysData[4]);

		u1_printf("\r\n [System]读取配置信息成功\r\n");
	}
	else
	{
		//使用默认数据
		g_SysCtrl.SysConfig = (SYSTEMCONFIG *)&DefaultSystemCofig;
		
		u1_printf("\r\n [System]读取配置信息失败，加载默认配置信息\r\n");
	}
	
	//软件版本信息==================================================================
	g_SysCtrl.SysInfo = (SYSTEM_INFO *)&default_sys_info;
	u1_printf("\r\n 系统版本：软件：%d  硬件：%d  时间：", g_SysCtrl.SysInfo->software_ver, g_SysCtrl.SysInfo->hardware_ver);		
	for(i=0; i<sizeof(g_SysCtrl.SysInfo->pn); i++)
		u1_printf("%c", g_SysCtrl.SysInfo->pn[i]);
	u1_printf("    版本号：");
	for(i=0; i<sizeof(g_SysCtrl.SysInfo->sn); i++)
		u1_printf("%c", g_SysCtrl.SysInfo->sn[i]);
	u1_printf("\r\n");
}

void InitSystemConfig(void)
{
	u8 i;
	SYSTEMCONFIG *p_sys;
	
	System_Option_Init();
	Init_SystemInfo();
	p_sys = GetSystemConfig();		
	
	u1_printf("\r\n 设备ID:%d\r\n", p_sys->Device_ID);
	u1_printf("\r\n LORA ID:%d\r\n", p_sys->Node);
	u1_printf("\r\n 网络连接类型:%d   ", p_sys->Type);
	
	switch(p_sys->Type)
	{
		case NETTYPE_SIM800C:
			u1_printf("SIM800C\r\n");
		break;
		case NETTYPE_LORA:
			u1_printf("LORA\r\n");
		break;
		case NETTYPE_GPRS:
			u1_printf("GPRS\r\n");
		break;
		case NETTYPE_NB_IOT:
			u1_printf("NB\r\n");
		break;
		case NETTYPE_WLAN:
			u1_printf("WLAN\r\n");
		break;
		case NETTYPE_SIM7600CE:
			u1_printf("SIM7600CE\r\n");
		break;
		default: 
			u1_printf("未识别\r\n");
		break;
	}
	u1_printf("\r\n 网络号:%d\r\n", p_sys->Net);
	u1_printf("\r\n 通信频道:%d\r\n", p_sys->Channel);
	u1_printf("\r\n 目标IP地址:");
	for(i=0; i<4; i++)
		u1_printf("%d ", p_sys->Gprs_ServerIP[i]);
	u1_printf("\r\n");
	u1_printf("\r\n 目标端口号:%d\r\n", p_sys->Gprs_Port);
	u1_printf("\r\n 重发次数:%d\r\n", p_sys->Retry_Times);
	u1_printf("\r\n 心跳间隔:%d\r\n", p_sys->Heart_interval);
	u1_printf("\r\n 数据上报间隔:%d\r\n", p_sys->Data_interval);
	u1_printf("\r\n 传感器等待时长:%d\r\n", p_sys->State_interval);
	u1_printf("\r\n 唤醒间隔:%d\r\n", STOP_TIME);
	u1_printf("\r\n 重启标志: %d\r\n", p_sys->AutoResetFlag);
	u1_printf("\r\n 重启时间 %02d:%02d\r\n",p_sys->AutoResetTime>>8, p_sys->AutoResetTime&0xff);	
}

