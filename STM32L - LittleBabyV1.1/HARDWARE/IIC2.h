#ifndef		_IIC2_H_
#define		_IIC2_H_

//#define ACK 	1
//#define NOACK	0

void IIC2_Init(void);

void IIC2_Start(void);

void IIC2_Stop(void);

unsigned char IIC2_WaitACK(void);

void IIC2_WriteByte(unsigned char data);

unsigned char IIC2_ReadByte(unsigned char IsAck);

void IIC2_InterfaceReset(void);

#endif
