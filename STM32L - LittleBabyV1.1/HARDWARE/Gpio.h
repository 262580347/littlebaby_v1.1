#ifndef __LED_H
#define __LED_H	 

#define		LED_GPIO_PIN	GPIO_Pin_11
#define		LED_GPIO_TYPE	GPIOA

void STM32_GPIO_Init(void);

void RunLED(void);

void WakeUp_GPIO_Init(void);

void LED_Blink(unsigned short nMain100ms);

void Stop_GPIO_Init(void);
	
void Stop_GPIO_Init_SIM(void);

void TXB0108_GPIO_Reset(void);

#endif
