#ifndef __ADC_H
#define __ADC_H	
#include "compileconfig.h"



#define		BQ24650_EN_PIN		GPIO_Pin_14
#define		BQ24650_EN_TYPE		GPIOB

#define		POWER_ADC_PIN		GPIO_Pin_0
#define		POWER_ADC_TYPE		GPIOB

#define		BAT_FULL_PIN		GPIO_Pin_15
#define		BAT_FULL_TYPE		GPIOB

#if( HARDWARE_VERSION == HARDWARE_VERSION_V1_0 )
#define		ADC_ENABLE_GPIO_TYPE		GPIOB
#define 	ADC_ENABLE_GPIO_PIN			GPIO_Pin_13

#elif( HARDWARE_VERSION == HARDWARE_VERSION_V1_2 )
#define		ADC_ENABLE_GPIO_TYPE		GPIOA
#define 	ADC_ENABLE_GPIO_PIN			GPIO_Pin_4
#endif

#define		ADC_PIN_ENABLE()		GPIO_SetBits(ADC_ENABLE_GPIO_TYPE, ADC_ENABLE_GPIO_PIN)
#define		ADC_PIN_DISABLE()		GPIO_ResetBits(ADC_ENABLE_GPIO_TYPE, ADC_ENABLE_GPIO_PIN)

#define		BQ24650_ENABLE()		GPIO_ResetBits(BQ24650_EN_TYPE, BQ24650_EN_PIN)
#define		BQ24650_DISABLE()		GPIO_SetBits(BQ24650_EN_TYPE, BQ24650_EN_PIN)

void Adc_Init(void);

void Open_AdcChannel(void);

void Adc_Reset(void);



#endif 
