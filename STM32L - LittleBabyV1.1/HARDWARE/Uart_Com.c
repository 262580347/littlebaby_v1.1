/**********************************
说明：调试使用的串口代码
	  
作者：关宇晟
版本：V2017.4.3
***********************************/
#include "Uart_Com.h"
#include "main.h"

unsigned char g_ComTestFlag = FALSE;

void ShowTestCmd(void)
{
	if(g_Uart2RxFlag == TRUE)
	{
		g_Uart2RxFlag = FALSE;
		
		u1_printf("%s\r\n", g_USART2_RX_BUF);
		
		Clear_Uart2Buff();
	}
}

static void COM1Protocol(void)
{
	unsigned char PackBuff[100], DataPack[100];
	u16   PackLength = 0, RecLength = 0, i;
	static CLOUD_HDR *hdr;
	
	UnPackMsg(g_USART_RX_BUF+1, g_USART_RX_CNT-2, PackBuff, &PackLength);	//解包

	if (PackBuff[(PackLength)-1] == Calc_Checksum(PackBuff, (PackLength)-1))
	{	
		hdr = (CLOUD_HDR *)PackBuff;
		hdr->protocol = swap_word(hdr->protocol);
		hdr->device_id = swap_dword(hdr->device_id);
		hdr->seq_no = swap_word(hdr->seq_no);
		hdr->payload_len = swap_word(hdr->payload_len);
		
		RecLength = hdr->payload_len + sizeof(CLOUD_HDR) + 1;
					
		if (RecLength != (PackLength))
		{
			u1_printf(" Pack Length Err\r\n");
		}
		else
		{
			PackLength--;	
			memcpy(&DataPack, PackBuff, sizeof(CLOUD_HDR));

			if(hdr->payload_len != 0)
				memcpy(&DataPack[sizeof(CLOUD_HDR)], &PackBuff[sizeof(CLOUD_HDR)], hdr->payload_len);

			OnDebug(DataPack, PackLength);
		
		}	
	}
	else
	{
		for(i=0; i<PackLength; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLength)-1));
	}	
}

void TaskForDebugCOM(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;	
	CHARGING_CFG*	p_Charg;
	u16   i, j;
	u8  Num, data[64];
	unsigned int  databuf4[64];
	unsigned int Addr;
	float	temperature;        // temperature [°C]
    float	humidity;	        // relative humidity [%RH]
    etError   error;        // error code
	
	if(g_UartRxFlag == TRUE)
	{
		g_UartRxFlag = FALSE;
		
		if(strncmp((char *)g_USART_RX_BUF, "RESET", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n RESET CMD\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CLEAR", 5) == 0)
		{		
			StartSensorMeasure();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SHOW", 4) == 0)
		{		
			u1_printf(" Temp: %2.2f, Humi: %2.2f, Ill:%2.2f, Pre: %2.2f, Vol: %d\r\n",\
			Get_Temp_Value(),\
			Get_Humi_Value(),\
			Get_Ill_Value(),\
			GetBMPPress(),\
			Get_Battery_Vol());
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SensorPowerOn", g_USART_RX_CNT) == 0)
		{		
			u1_printf("SensorPowerOn\r\n");
			SensorPowerOn();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SensorPowerOff", g_USART_RX_CNT) == 0)
		{		
			u1_printf("SensorPowerOff\r\n");
			SensorPowerOff();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "STATUS", 6) == 0)
		{		
				u1_printf("Status: %d %d %d %d\r\n", Sht31Status, MaxStatus, BmpStatus, BatStatus);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "TEST MODE", g_USART_RX_CNT) == 0)
		{
			if(g_ComTestFlag == FALSE)
			{
				g_ComTestFlag = TRUE;
								
				SIM800CPortInit();
				SIM_PWR_ON();
				USART2_Config(SIM_BAND_RATE);
			}
			else
			{
				u1_printf("\r\n Exit Test.\r\n");
				g_ComTestFlag = FALSE;
			}
			
		}
		else if(strncmp((char *)g_USART_RX_BUF, "DTR", g_USART_RX_CNT) == 0)
		{
			u1_printf("DTR\r\n");
			
			GPIO_ToggleBits(SIM7600CE_DTR_TYPE, SIM7600CE_DTR_PIN);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SLEEP", g_USART_RX_CNT) == 0)
		{
			u1_printf("SLEEP\r\n");
			
			LoraPort_Init();
		
			LORA_SLEEP_MODE();
					
			LORA_PWR_ON();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "AT", 2) == 0)
		{
			u1_printf("%s\r\n", g_USART_RX_BUF);
			u2_printf("%s\r\n", g_USART_RX_BUF);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPS", g_USART_RX_CNT) == 0)
		{
			u1_printf("跳过GPS \r\n");
			SetGPSTaskRunFlag(FALSE);
			GPSPowerOff();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "TCP", 3) == 0)
		{
			u2_printf("AT+CIPOPEN=0,\"TCP\",\"42.159.233.88\",6070\r");
		}
		else if(strncmp((char *)g_USART_RX_BUF, "+++", 3) == 0)
		{
			u2_printf("%s", g_USART_RX_BUF);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Alive", g_USART_RX_CNT) == 0)
		{
			Mod_Send_Alive(WIFI_COM);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SimData", g_USART_RX_CNT) == 0)
		{
			Mod_Report_LowPower_Data(SIM_COM);
		}
		
		else if(strncmp((char *)g_USART_RX_BUF, "Status", g_USART_RX_CNT) == 0)
		{
			Mod_Report_LowPower_Data(WIFI_COM);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Start Charging", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Start Charging\r\n");
			p_Charg = GetChargingStatus();
			
			p_Charg->ForceFlag = FORCED_CHARGE_ENABLE;
			p_Charg->Chksum = Calc_Checksum((unsigned char *)&p_Charg->Enable, 2);
			EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)&p_Charg, sizeof(CHARGING_CFG));

			SetBQ24650ENFlag(TRUE);	//关闭充电芯片的唯一入口	
			StartGetBatVol();
			
			GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
			BQ24650_ENABLE();
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "Stop Charging", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Stop Charging\r\n");
			p_Charg = GetChargingStatus();
			
			p_Charg->ForceFlag = FORCED_CHARGE_DISABLE;
			p_Charg->Chksum = Calc_Checksum((unsigned char *)&p_Charg->Enable, 2);
			EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)&p_Charg, sizeof(CHARGING_CFG));

			SetBQ24650ENFlag(FALSE);	//关闭充电芯片的唯一入口	
			StartGetBatVol();
			GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
			BQ24650_DISABLE();
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "PowerDetect", g_USART_RX_CNT) == 0)
		{
			u1_printf("PowerDetect\r\n");
			StartGetBatVol();
		}		

		else if(strncmp((char *)g_USART_RX_BUF, "STM32 STOP", g_USART_RX_CNT) == 0)
		{
			u1_printf(" Stop\r\n");
			SetStopModeTime( TRUE, 10);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "READ4", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\nEEPROM Read4\r\n");
					
			for(j=0; j<64; j++)
			{
				EEPROM_ReadWords(EEPROM_BASE_ADDR + j*64, databuf4, 16); 
				
				for(i=0; i<16; i++)
				{
					u1_printf("%08X ", databuf4[i]);
				}
				u1_printf("\r\n");
			}
			u1_printf("\r\n");
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Erase1 ", 7) == 0)
		{
			Addr = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);

			u1_printf("\r\nErase :%08X\r\n", Addr*64 + EEPROM_BASE_ADDR);
			
			EEPROM_EraseWords(Addr);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "EraseAll", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Clear All EEPROM Data\r\n");
			for(i=0; i< EEPROM_BLOCK_COUNT; i++)
			{
				EEPROM_EraseWords(i);
				u1_printf(" Clear %d Block\r\n", i+1);
			}
		}
		else if(strncmp((char *)g_USART_RX_BUF, "EraseLog", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Erase EEPROM Data\r\n");
			for(i=9; i< EEPROM_BLOCK_COUNT; i++)
			{
				EEPROM_EraseWords(i);
				u1_printf(" Clear %d Block\r\n", i+1);
			}
			
			LogStorePointerInit();
		}		
		else if(strncmp((char *)g_USART_RX_BUF, "EraseCom", g_USART_RX_CNT) == 0)
		{
			ClearCommunicationCount();
			CommunicatinLogInit();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "ShowCom", g_USART_RX_CNT) == 0)
		{
			CommunicatinLogInit();
		}
		else if((strncmp((char *)g_USART_RX_BUF, "log", g_USART_RX_CNT) == 0) || (strncmp((char *)g_USART_RX_BUF, "LOG", g_USART_RX_CNT) == 0))
		{
			ShowLogContent();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "485Code ", 8) == 0)
		{
			Num = (g_USART_RX_CNT - 7)/3;
			u1_printf("Send 485 Code:");
			for(i=0; i<Num; i++)
			{
				StrToHex(&data[i], &g_USART_RX_BUF[8+i*3], Num);
				u1_printf("%02X ", data[i]);
			}				
			u1_printf("\r\n");
			Uart_Send_Data(USART3, data, Num);

		}
		else if(strncmp((char *)g_USART_RX_BUF, "sht", g_USART_RX_CNT) == 0)
		{

			error = SHT3X_GetTempAndHumi(&temperature, &humidity, REPEATAB_HIGH, MODE_POLLING, 100);
			
			u1_printf("Temperature=%.2f℃\tHumidity=%.2f%%\tError=%d\r\n",(float)temperature,(float)humidity,(u8)error);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SHT", g_USART_RX_CNT) == 0)
		{
			StartSHTSensor();	
		}
		else
		{		
			if(g_USART_RX_BUF[0] == BOF_VAL && g_USART_RX_BUF[g_USART_RX_CNT-1] == EOF_VAL && g_USART_RX_CNT >= 15)
			{
				COM1Protocol();
			}
		}
		Clear_Uart1Buff();
	}	
	

}

















