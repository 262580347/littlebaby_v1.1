#include "main.h"
#include "IIC2.h"

#define	SDA2_PIN		GPIO_Pin_6
#define	SDA2_TYPE		GPIOB
#define	SCK2_PIN		GPIO_Pin_7
#define	SCK2_TYPE		GPIOB

#define SDA2_IN()  											\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA2_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		\
	GPIO_Init(SDA2_TYPE, &GPIO_InitStructure);			\
}
#define SDA2_OUT()   										\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA2_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA2_TYPE, &GPIO_InitStructure);			\
}


#define IIC2_SCK_HIGH()    GPIO_SetBits(SCK2_TYPE, SCK2_PIN)
#define IIC2_SCK_LOW() 	GPIO_ResetBits(SCK2_TYPE, SCK2_PIN)
#define IIC2_SDA_HIGH()    GPIO_SetBits(SDA2_TYPE, SDA2_PIN)
#define IIC2_SDA_LOW() 	GPIO_ResetBits(SDA2_TYPE, SDA2_PIN)
#define READ_SDA()   		GPIO_ReadInputDataBit(SDA2_TYPE, SDA2_PIN)

#define	IIC2_DELAY()	delay_us(4)

void IIC2_InterfaceReset(void)
{
	u8 i;
	
	SDA2_OUT();
	IIC2_SCK_HIGH();
	IIC2_SDA_HIGH();
	delay_ms(10);
	
	for(i=0; i<18; i++)
	{
		IIC2_SCK_LOW();
		delay_us(100);
		IIC2_SDA_HIGH();
		delay_us(100);
	}
	
	delay_ms(10);
}

void IIC2_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;  	
	
	GPIO_InitStructure.GPIO_Pin = SDA2_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA2_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK2_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK2_TYPE, &GPIO_InitStructure);	

	
	GPIO_SetBits(SDA2_TYPE, SDA2_PIN);
	GPIO_SetBits(SCK2_TYPE, SCK2_PIN);
}

void IIC2_Start(void)
{
	SDA2_OUT();
	IIC2_SDA_HIGH();
	IIC2_SCK_HIGH();
	IIC2_DELAY();
	IIC2_SDA_LOW();
	IIC2_DELAY();
	IIC2_SCK_LOW();
}

void IIC2_Stop(void)
{
	SDA2_OUT();
	IIC2_SCK_LOW();
	IIC2_SDA_LOW();
	IIC2_DELAY();
	IIC2_SCK_HIGH();
	IIC2_DELAY();
	IIC2_SDA_HIGH();
	IIC2_DELAY();
}

static void IIC2_ACK(void)
{
	IIC2_SCK_LOW();
	SDA2_OUT();
	IIC2_SDA_LOW();
	IIC2_DELAY();
	IIC2_SCK_HIGH();
	IIC2_DELAY();
	IIC2_SCK_LOW();
}

static void IIC2_NoACK(void)
{
	IIC2_SCK_LOW();	
	SDA2_OUT();
	IIC2_SDA_HIGH();
	IIC2_DELAY();
	IIC2_SCK_HIGH();
	IIC2_DELAY();
	IIC2_SCK_LOW();
}

unsigned char IIC2_WaitACK(void)
{
	unsigned char ucErrTime=0;
	SDA2_IN();
	IIC2_SDA_HIGH();	
	IIC2_DELAY();	
	IIC2_SCK_HIGH();
	IIC2_DELAY();
	while(READ_SDA())
	{
		ucErrTime++;
		if(ucErrTime >250)
		{
			IIC2_Stop();
			return 1;
		}
	}
	IIC2_SCK_LOW();
	return 0;
}

void IIC2_WriteByte(unsigned char data)
{
	unsigned char i;
	
	SDA2_OUT();
	IIC2_SCK_LOW();
	for(i=0; i<8; i++)
	{
		if(data & 0x80)
			IIC2_SDA_HIGH();
		else
			IIC2_SDA_LOW();
		data <<= 1;
		IIC2_DELAY();
		IIC2_SCK_HIGH();
		IIC2_DELAY();
		IIC2_SCK_LOW();
		IIC2_DELAY();
	}	
}

unsigned char IIC2_ReadByte(unsigned char IsAck)
{
	unsigned char i, Receive = 0;
	
	SDA2_IN();
	for(i=0; i<8; i++)
	{
		IIC2_SCK_LOW();
		IIC2_DELAY();
		IIC2_SCK_HIGH();
		IIC2_DELAY();
		Receive <<= 1;
		if(READ_SDA())
			Receive++;
		IIC2_DELAY();
	}
	if(IsAck)
		IIC2_ACK();
	else
		IIC2_NoACK();
	
	return Receive;
}	
