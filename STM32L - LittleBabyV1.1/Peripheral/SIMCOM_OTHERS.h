//底层相关接口，自己根据自己的平台进行开发的接口

//发送数据接口
static bool GPRS_UART_SendData(u8 DataBuff[], u16 DataLen)
{
	UARTx_SendData(SIMCOM_UART_CH, DataBuff, DataLen);
	
	return TRUE;
}
 
 
 
//接收数据接口
static int GPRS_UART_ReadData(u8 **pDataBuff,u8 ByteTimeOutMs, u16 TimeOutMs, u16 *pReceiveDelayMs)
{
	u32 cnt = 0;
	u16 TempTime;
	
	if(ByteTimeOutMs < 1) ByteTimeOutMs = 1;	//字节超时时间，2个帧之间的间隔最小时间
	TimeOutMs /= ByteTimeOutMs;
	TimeOutMs += 1;
	TempTime = TimeOutMs;
	while(TimeOutMs --)
	{
		cnt = UARTx_GetRxCnt(SIMCOM_UART_CH);
		OSTimeDlyHMSM(0,0,0,ByteTimeOutMs);;
		if((cnt > 0) && (cnt == UARTx_GetRxCnt(SIMCOM_UART_CH)))
		{
			if(pReceiveDelayMs!=NULL)	//需要返回延时
			{
				*pReceiveDelayMs = (TempTime-TimeOutMs)*ByteTimeOutMs;
			}
			*pDataBuff = g_SIMCOM_Buff;						//接收缓冲区
			
			return cnt;
		}
#if SYS_WDG_EN_
		IWDG_Feed();										//喂狗
#endif			
	}
	
	return 0;
}
 
 
//清除接收缓冲区
static void GPRS_UART_ClearData(void)
{
	UARTx_ClearRxCnt(SIMCOM_UART_CH);	//清除串口缓冲区
}













//////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
//SIM900/SIM800通信支持
//GSM模块相关定义
#define SIMCOM_UART_CH			UART_CH3			//串口3
#define SIMCOM_UART_BAUD		115200				//波特率
#define SIMCOM_UART_BUFF_SIZE	(1024*4)			//接收缓冲区大小
 
//相关控制引脚
__inline void SIMCOM_SetDTR(u8 Level)		{(PGout(4)=Level);}			//DTR
__inline void SIMCOM_SetPWRKEY(u8 Level)	{(PGout(3)=Level);}			//PWRKEY
__inline u8 SIMCOM_GetSTATUS(void)			{return PGin(5)?1:0;}		//STATUS
__inline u8 SIMCOM_GetDCD(void)				{return PDin(11)?1:0;}		//DCD-上拉输入，高电平AT指令模式，低电平为透传模式
 
//引脚初始化
__inline void SIMCOM_IO_Init(void)	
{
	SYS_DeviceClockEnable(DEV_GPIOD, TRUE);					//使能GPIOD时钟
	SYS_DeviceClockEnable(DEV_GPIOG, TRUE);					//使能GPIOG时钟
	SYS_GPIOx_Init(GPIOG, BIT3|BIT4, OUT_PP, SPEED_2M);		//推挽输出
	SYS_GPIOx_OneInit(GPIOD, 11, IN_IPU, IN_NONE);			//DCD 上拉输入
	SYS_GPIOx_OneInit(GPIOG, 5, IN_IPD, IN_NONE);			//STATUS 下拉输入
}  


////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
//另外还有一个看门狗清除的回调函数

//喂独立看门狗
__inline void IWDG_Feed(void) {IWDG->KR=0XAAAA;}


////////////////////////////////////////////////////////////////////////////////////////////
//实现了以上的接口就可以调用了

//下面是应用层调用，使用了ucos的一个任务

SIMCOM_HANDLE	g_SIMCOM_Handle;								//SIMCOM通信模块句柄
u8 g_SIMCOM_Buff[SIMCOM_UART_BUFF_SIZE+1];						//串口接收缓冲区
 
 
 
static bool GPRS_UART_SendData(u8 DataBuff[], u16 DataLen);		//发送数据接口
static int GPRS_UART_ReadData(u8 **pDataBuff,u8 ByteTimeOutMs, u16 TimeOutMs, u16 *pReceiveDelayMs);		//接收数据接口
static void GPRS_UART_ClearData(void);							//清除接收缓冲区
 
 
//GPRS通信任务
void GPRS_Task(void *pdata)
{
	const char *pModeInof;
		
	UARTx_Init(SIMCOM_UART_CH, SIMCOM_UART_BAUD, ENABLE);													//初始化串口
	UARTx_SetRxBuff(SIMCOM_UART_CH, g_SIMCOM_Buff,  SIMCOM_UART_BUFF_SIZE);									//设置串口接收缓冲区
	SIMCOM_IO_Init();																						//SIMCOM相关IO初始化
	OSTimeDlyHMSM(0,0,0,20);
	
	//初始化SIMCOM句柄接口
	SIMCOM_Init(&g_SIMCOM_Handle,
		GPRS_UART_SendData,		//发送数据接口，如果发送失败，返回FALSE,成功返回TRUE;
		GPRS_UART_ReadData,		//接收数据接口，返回数据长度，如果失败返回<=0,成功，返回数据长度
		GPRS_UART_ClearData,	//清除接收缓冲区函数，用于清除接收数据缓冲区数据
		SIMCOM_SetDTR,			//DTR引脚电平控制-用于控制sleep模式或者退出透传模式
		SIMCOM_SetPWRKEY,		//PWRKEY开机引脚电平控制-用于开机
		SIMCOM_GetSTATUS,		//获取STATUS引脚电平-用于指示模块上电状态
		SIMCOM_GetDCD,			//DCD-上拉输入，高电平AT指令模式，低电平为透传模式
		SYS_DelayMS,			//系统延时函数
		IWDG_Feed				//清除系统看门狗(可以为空)
	);
		
	while(1)
	{
		SIMCOM_RegisNetwork(&g_SIMCOM_Handle, 6, 60, &pModeInof);//SIMCOM模块上电初始化并注册网络
		//SIMCOM 初始化获取短信中心号码以及本机号码,信号强度,CIMI（结果存放在句柄pHandle中）
		if(SIMCOM_PhoneMessageNumberInitialize(&g_SIMCOM_Handle, 3) == FALSE)
		{
			uart_printf("\r\n警告：初始化获取相关信息失败！\r\n");
		}
		
		
		OSTimeDlyHMSM(0,5,0,20);
	}
}

/////////////////////////////////////////////////////////////////////////////////
//以上代码实现了通信模块的初始化，开机，联网，读取本机号码，中心站号码，SIMI码，信号等操作，感兴趣的朋友可以依照此模板进行后续的开发，使用底层分离的方式可以让代码解耦便于在各个平台上面移植，同时增加了稳定性。

//下面是串口打印的信息，命令执行的很快，我使用的是SIM7600CE模块，全网通4G，初始化非常快，每个文件都有定义调试开关，可以动态使用一个变量控制开关，也可以使用宏定义控制调试信息开关。


SIMCOM(26B)->
SIMCOM_SIM7600CE

OK

OK 返回成功!
[DTU]通信模块为SIM7600

SIMCOM(22B)->
+CPIN: READY

OK

OK 返回成功!

SIM卡准备就绪!

SIMCOM(20B)->
+CREG: 0,1

OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!
[DTU]网络注册状态1!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(22B)->
+CPIN: READY

OK

OK 返回成功!

SIM卡准备就绪!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(35B)->
OK

SIMCOM INCORPORATED

OK

OK 返回成功!

SIMCOM(26B)->
SIMCOM_SIM7600CE

OK

OK 返回成功!

SIMCOM(34B)->
+GMR: LE11B02SIM7600CE-A

OK

OK 返回成功!

SIMCOM(25B)->
861477030032237

OK

OK 返回成功!

制造商:OK
模块型号:SIMCOM_SIM7600CE
软件版本:LE11B02SIM7600CE-A
模块序列号:861477030032237

SIMCOM(42B)->
+COPS: 0,0,"CHINA MOBILE CMCC",7

OK

OK 返回成功!
运营商信息:CHINA MOBILE CMCC

SIMCOM(22B)->
+CNSMOD: 0,8

OK

OK 返回成功!
网络制式:LTE

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(6B)->
OK

OK 返回成功!

SIMCOM(37B)->
+CSCA: "+8613800100569",145

OK

OK 返回成功!
短信中心号码:13800100569
短信中心号码:13800100569

SIMCOM(21B)->
+CSQ: 20,99

OK

OK 返回成功!
信号强度 <20>!

SIMCOM(41B)->
+CNUM: "","1064838905852",129


OK

OK 返回成功!
本机号码:1064838905852
本机号码:1064838905852

SIMCOM(25B)->
460040389005852

OK

OK 返回成功!
获取CIMI成功：460040389005852
--------------------- 
作者：cp1300 
来源：CSDN 
原文：https://blog.csdn.net/cp1300/article/details/79686460 
版权声明：本文为博主原创文章，转载请附上博文链接！