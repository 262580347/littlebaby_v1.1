/**********************************
说明：BMP280大气压力传感器驱动
	  注意SDO接高电平，IIC2地址为0x76
	  SDO接低电平，IIC2地址为0x77
	  若接口更改则需修改宏定义及IIC2_Init中的GPIO
	  IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3C);	//睡眠模式  用于低功耗设备
	  IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3F);	//正常模式  用于传感器
作者：关宇晟
版本：V2017.12.26
***********************************/

#include "main.h"
#include "BMP280.h"
#include <math.h>

#define __STAND_TEMP      15.0f    // 15 C
#define __STAND_PRES      1013.25f // 101.325kPa = 1013.25mbar

#define  GPIO_I2C_DIR_SEND         ((unsigned char)0x00)
#define  GPIO_I2C_DIR_RECV         ((unsigned char)0x01)

#define BMP_ACK 	1
#define BMP_NOACK	0

#define BMP280_I2C_ADDR                         0x76 // SDO 接地 0x76，SDO接VCC  0x77

#define BMP280_REG_TEMP_XLSB                    0xFC
#define BMP280_REG_TEMP_LSB                     0xFB
#define BMP280_REG_TEMP_MSB                     0xFA
#define BMP280_REG_PRESS_XLSB                   0xF9
#define BMP280_REG_PRESS_LSB                    0xF8
#define BMP280_REG_PRESS_MSB                    0xF7
#define BMP280_REG_CONFIG                       0xF5
#define BMP280_REG_CTRL_MEAS                    0xF4
#define BMP280_REG_STATUS                       0xF3
#define BMP280_REG_RESET                        0xE0
#define BMP280_REG_ID                           0xD0

#define BMP280_REG_DIG_T1                       0x88
#define BMP280_REG_DIG_T2                       0x8A
#define BMP280_REG_DIG_T3                       0x8C

#define BMP280_REG_DIG_P1                       0x8E
#define BMP280_REG_DIG_P2                       0x90
#define BMP280_REG_DIG_P3                       0x92
#define BMP280_REG_DIG_P4                       0x94
#define BMP280_REG_DIG_P5                       0x96
#define BMP280_REG_DIG_P6                       0x98
#define BMP280_REG_DIG_P7                       0x9A
#define BMP280_REG_DIG_P8                       0x9C
#define BMP280_REG_DIG_P9                       0x9E
      
#define BMP280_ID                               0x58	//0x56 0x57 0x58?BMP280
#define BMP280_RESET                            0xB6

BMP_STATUS BmpStatus;	//传感器状态

#define 	BMP_DETECT_INTERVAL		10

//#define		BMP_FORCE_MODE		0x25
#define		BMP_FORCE_MODE		0xB5

#define		BMP_WORK_MODE		0x27

//#define		BMP_SLEEP_MODE		0x24
#define		BMP_SLEEP_MODE		0xB4

uint16 dig_T1;
int16  dig_T2;
int16  dig_T3;

uint16 dig_P1;
int16  dig_P2;
int16  dig_P3;
int16  dig_P4;
int16  dig_P5;
int16  dig_P6;
int16  dig_P7;
int16  dig_P8;
int16  dig_P9;

int32  t_fine;

#define		BUF_MAX		10

static float s_fBMPTemp = 0.0;
static float s_fBMPPress = 0.0;
static float s_fBMPAltitude = 0.0;

static float s_PressBuf[BUF_MAX];

static unsigned char s_BMPGetFlag = FALSE;

float GetBMPTemp( void )
{
	return s_fBMPTemp;
}

float GetBMPPress( void )
{
	return s_fBMPPress;
}

float GetBMPAltitude( void )
{
	return s_fBMPAltitude;
}

void StartBMPSensor(void)
{
	s_BMPGetFlag = TRUE;
}

static u8  IIC2_Read_Data (unsigned char addr, unsigned char reg, unsigned char *pbuf, int len)
{
	u8 err;
	
    IIC2_Start();
    IIC2_WriteByte((addr << 1) | 0x00);
	err = IIC2_WaitACK();
    if(err)
	{
		return 1;
	}
	
    IIC2_WriteByte(reg);
    err = IIC2_WaitACK();
    if(err)
	{
		return 1;
	}	
	
    IIC2_Start();
    IIC2_WriteByte((addr << 1) | 0x01);
    err = IIC2_WaitACK();
	if(err)
	{
		return 1;
	}
	
    while (len) 
	{ 
		if (len == 1) 
		{
			*pbuf = IIC2_ReadByte(BMP_NOACK);
		} 
		else 
		{
			*pbuf = IIC2_ReadByte(BMP_ACK);
		}
        pbuf++;
        len--;
    }
    IIC2_Stop();
    
    return 0;
}

static u8  IIC2_Write_Reg (unsigned char addr, unsigned char reg, unsigned char data)
{
	u8 err = 1;
	
    IIC2_Start();
    IIC2_WriteByte((addr << 1) | 0x00);
	err = IIC2_WaitACK();   
	if(err)
		return 1;
    
    IIC2_WriteByte(reg);
	err = IIC2_WaitACK();   
	if(err)
		return 1;
	IIC2_WriteByte(data);

	err = IIC2_WaitACK();   
	if(err)
		return 1;
    IIC2_Stop();
    
    return 0;
}

static u8  IIC2_Read_Reg (unsigned char addr, unsigned char reg, /* @out */unsigned char *data)
{
	u8 err = 1;
	
    IIC2_Start();
    IIC2_WriteByte((addr << 1) | 0x00);
	err = IIC2_WaitACK();   
	if(err)
		return 1;
    IIC2_WriteByte(reg);
	err = IIC2_WaitACK();   
	if(err)
		return 1;

    IIC2_Start();
    IIC2_WriteByte((addr << 1) | 0x01);
	err = IIC2_WaitACK();   
	if(err)
		return 1;
	
    *data = IIC2_ReadByte(BMP_NOACK);
    IIC2_Stop();

    return 0;
}


int BMP280_Init (void)
{
    unsigned char uc_ID = 0, err = 0;
	char str[100];
	uint32 error_mark = 0xEEEEEEEE;
	
	BmpStatus = BMP_ERROR;
    IIC2_Init();
        
	delay_ms(20);
	
	IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_RESET,  BMP280_RESET);	//软件复位
	
	delay_ms(100);
    err = IIC2_Read_Reg (BMP280_I2C_ADDR, BMP280_REG_ID, &uc_ID);
    
	if(err)
	{
		u1_printf("\r\n [BMP280] 初始化失败\r\n");
		BmpStatus = BMP_ERROR;
		s_BMPGetFlag = FALSE;
		memcpy((uint8 *)&s_fBMPPress, (uint8 *)&error_mark, sizeof(uint32));
		return ERROR_VALUE;
	}
	
    if (uc_ID != BMP280_ID) 
	{
        sprintf(str, "\r\n [BMP280] uc_ID = 0x%02x，not BMP280 ID!Init failed!\r\n", uc_ID);
		Debug_Out(str);
		BmpStatus = BMP_ERROR;
		memcpy((uint8 *)&s_fBMPPress, (uint8 *)&error_mark, sizeof(uint32));
		s_BMPGetFlag = FALSE;
        return ERROR_VALUE;
    }
   
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T1, (unsigned char *)&dig_T1, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T2, (unsigned char *)&dig_T2, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T3, (unsigned char *)&dig_T3, 2);

    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P1, (unsigned char *)&dig_P1, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P2, (unsigned char *)&dig_P2, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P3, (unsigned char *)&dig_P3, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P4, (unsigned char *)&dig_P4, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P5, (unsigned char *)&dig_P5, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P6, (unsigned char *)&dig_P6, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P7, (unsigned char *)&dig_P7, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P8, (unsigned char *)&dig_P8, 2);
    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P9, (unsigned char *)&dig_P9, 2);
    		
	IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_RESET,  BMP280_RESET);	//软件复位
	
	IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_FORCE_MODE);
	IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CONFIG, 0x30);
	
	u1_printf("\r\n [BMP280] Init Success\r\n");
	delay_ms(50);
	
	BmpStatus = BMP_IDLE;
	
	s_BMPGetFlag = TRUE;
 //   IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3F);
//	IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_SLEEP_MODE);
    return 0;
}

static int BMP280Get_Temp_Raw (void)
{
    unsigned char temp[3] = {0};
    int     result = 0;

    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_TEMP_MSB, temp, 3);
    
    result = (temp[0] << 16) | (temp[1] << 8) | (temp[2]);
    
    result >>= 4;
    
    return result;
}

static int BMP280Get_Press_Raw (void)
{
    unsigned char press[3] = {0};
    int     result = 0;

    IIC2_Read_Data (BMP280_I2C_ADDR, BMP280_REG_PRESS_MSB, press, 3);
    
    result = (press[0] << 16) | (press[1] << 8) | (press[2]);
    
    result >>= 4;
    
    return result;
}

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of "5123" equals 51.23 DegC.
// t_fine carries fine temperature as global value

static int BMP280Get_Temp_x100 (void)
{
	
	//int32  t_fine;
    int raw_temp;   
    int adc_T;
    int32 var1, var2, T;
	
	raw_temp = BMP280Get_Temp_Raw();
	adc_T = raw_temp;
    
    var1 = ((((adc_T>>3) - ((int32)dig_T1<<1))) * ((int32)dig_T2)) >> 11;
    var2 = (((((adc_T>>4) - ((int32)dig_T1)) * ((adc_T>>4) - ((int32)dig_T1))) >> 12) * ((int32)dig_T3)) >> 14;
    
    t_fine = var1 + var2;
    
    T = (t_fine * 5 + 128) >> 8;
    
    return T;
}

static float BMP280Get_Temp (void)
{
    int tempc_x100 = BMP280Get_Temp_x100();
    return (float)(tempc_x100 / 100.0f);
}

// Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
// Output value of "24674867" represents 24674867/256 = 96386.2 Pa = 963.862 hPa
static unsigned int BMP280Get_Press_Q24_8 (void)
{
    int raw_press = BMP280Get_Press_Raw();
    
    int adc_P = raw_press;
    
    long long var1, var2, p;
    
    var1 = ((long long)t_fine) - 128000;
    var2 = var1 * var1 * (long long)dig_P6;
    var2 = var2 + ((var1*(long long)dig_P5)<<17);
    var2 = var2 + (((long long)dig_P4)<<35);
    var1 = ((var1 * var1 * (long long)dig_P3)>>8) + ((var1 * (long long)dig_P2)<<12);
    var1 = (((((long long)1)<<47)+var1))*((long long)dig_P1)>>33;
    
    if (var1 == 0) {
        return 0; // avoid exception caused by division by zero
    }
    
    p = 1048576- adc_P;
    p = (((p<<31)-var2)*3125)/var1;
    var1 = (((long long)dig_P9) * (p>>13) * (p>>13)) >> 25;
    var2 = (((long long)dig_P8) * p) >> 19;
    p = ((p + var1 + var2) >> 8) + (((long long)dig_P7)<<4);
    
    return (unsigned int)p;
}

static float BMP280Get_Press (void)
{
    float pa;
    unsigned int press_q24_8 = BMP280Get_Press_Q24_8();
    pa = (float)(press_q24_8 / 256.0f);
    
    return pa / 100.0f;
}

// h = Z2-Z1 = 18400(1+atm)LgP1/P2
static float BMP280Get_Altitude (float temperature, float pressure)
{
    return 18400.0f * (1.0f + temperature / 273.0f) * log10(__STAND_PRES / pressure);
}


unsigned char BMP_Process( unsigned short nMain10ms )
{
	char str[100];
	float fBMPPress = 0.0, fBMPTemp;
	u8 ack = 0;
	static u8 s_State = 1, s_DetCount = 0, s_DetNum = 0, err = 0;
	static u16 s_LastTime;
	uint32 error_mark = 0xEEEEEEEE;
	
	if(s_BMPGetFlag == TRUE && Sht31Status != SHT_WORKING)
	{
		switch(s_State)
		{
			case 1:
				if(BmpStatus == BMP_ERROR)
				{
					memcpy((uint8 *)&s_fBMPPress, (uint8 *)&error_mark, sizeof(uint32));
					SensorPowerReset();
					BMP280_Init();
					s_BMPGetFlag = FALSE;
					s_State++;
				}
				else
				{
					BmpStatus = BMP_WORKING;
					s_State++;
					s_LastTime = GetSystem10msCount();
	//				IIC2_Init();
					//IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3F);
					err = IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_FORCE_MODE);
				}
			
			break;
			
			case 2:
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 12)
				{				
//					IIC2_Read_Reg (BMP280_I2C_ADDR, BMP280_REG_ID, &RegistStatus);					
//					u1_printf(" BMP Status:0x%02X\r\n");
					
					fBMPTemp = BMP280Get_Temp();
		
					fBMPPress = BMP280Get_Press();
	
					BMP280Get_Altitude(fBMPTemp, fBMPPress);
					
//					IIC2_Read_Reg (BMP280_I2C_ADDR, BMP280_REG_ID, &RegistStatus);
//					u1_printf(" BMP Status:0x%02X\r\n");
					
					if(fBMPPress  > 1100 || fBMPPress < 300 || err)//错误的大气压力范围，重新初始化芯片
					{
						BmpStatus = BMP_ERROR;
						s_fBMPPress = ERROR_VALUE;
						memcpy((uint8 *)&s_fBMPPress, (uint8 *)&error_mark, sizeof(uint32));
						u1_printf("\r\n Error Press Data, Err(%d)\r\n", err);
						SetLogErrCode(LOG_CODE_SENSOR_ERR);
						SensorPowerReset();		
						I2c_GeneralCallReset();	
						delay_ms(20);
						BMP280_Init();
						s_State = 1;
						delay_ms(20);
						s_BMPGetFlag = FALSE;
						break;
					}
					else
					{
						if(s_DetCount < 3)
						{
							s_PressBuf[s_DetCount] = fBMPPress;
							s_fBMPPress = fBMPPress;
						}
						else
						{
							s_PressBuf[s_DetNum] = fBMPPress;
							s_fBMPPress = Mid_Filter(s_PressBuf, s_DetCount);
						}
						
						s_DetCount++;
						s_DetNum++;
						
						if(s_DetCount >= BUF_MAX)
						{
							s_DetCount = BUF_MAX;
						}
						
						if(s_DetNum >= BUF_MAX)
						{
							s_DetNum = 0;
						}
																			
						sprintf(str, "\r\n Pressure:%2.2fhPa Avg:%2.2fhPa\r\n", fBMPPress, s_fBMPPress);
						Debug_Out(str);
						BmpStatus = BMP_IDLE;
					}
					
									
					ack = IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_SLEEP_MODE);
					if(ack == 0)
					{
						s_State = 1;
						s_BMPGetFlag = FALSE;
						BmpStatus = BMP_IDLE;
					}
					else
					{
						s_State++;
					}
					
					s_LastTime = GetSystem10msCount();
//					IIC2_Read_Reg (BMP280_I2C_ADDR, BMP280_REG_STATUS, &RegistStatus);
//		
//					u1_printf("\r\n [BMP280]Status:0x%02X\r\n", RegistStatus);	
				}
			break;
				
			case 3:
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 4)
				{
					s_LastTime = GetSystem10msCount();
					ack = IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_SLEEP_MODE);
					if(ack == 0)
					{
						s_State = 1;
						s_BMPGetFlag = FALSE;
						BmpStatus = BMP_IDLE;
					}
					else
					{
						s_State++;
					}
				}
			break;
				
			case 4:
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 2)
				{
					s_LastTime = GetSystem10msCount();
					ack = IIC2_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, BMP_SLEEP_MODE);
					if(ack == 0)
					{
						s_State = 1;
						s_BMPGetFlag = FALSE;
					}
					else
					{
						s_State = 1;
						s_BMPGetFlag = FALSE;
					}
					BmpStatus = BMP_IDLE;
				}
			break;
			
			
		}
	}
		
	return 0;
	
}



