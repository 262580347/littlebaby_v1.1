#ifndef  _SIM_H_
#define	 _SIM_H_
#include "compileconfig.h"

#define 	MCC_CHINA		460
#define 	MCC_VIETNAM		452

typedef enum{
	CMNET = 0,		//中国移动
	UNINET,			//中国联通
	CTNET,			//中国电信
	MOBIFONE,		//越南MobiFone
	VINAPHONE,		//越南Vinaphone
	S_FONE,			//越南S-Fone
	VIETTEL,		//越南Viettel
	VIETNAMOBILE,	//越南Vietnamobile
	EVNTELECOM,		//越南EVNTelecom
	BEELINE,		//越南Beeline
	EVNTELECOM_3G,	//越南3G EVNTelecom
	MCC_UNKOWN		//未知的MCC
}MMC_TAB;

extern char APN_TAB[11][20];
	
#define 	WAIT_10 	10
#define 	WAIT_50 	50
#define 	WAIT_1000 	1000

#if( HARDWARE_VERSION == HARDWARE_VERSION_V1_0 )

#define		SIM800C_ENABLE_TYPE		GPIOA		//
#define		SIM800C_ENABLE_PIN		GPIO_Pin_11

#elif( HARDWARE_VERSION == HARDWARE_VERSION_V1_2 )
#define		SIM800C_ENABLE_TYPE		GPIOA		//
#define		SIM800C_ENABLE_PIN		GPIO_Pin_12

#endif

#define		SIM_PWR_ON()			GPIO_SetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);
#define		SIM_PWR_OFF()			GPIO_ResetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);

void SIM800CPortInit(void);

void SIM800CProcess(unsigned short nMain10ms);	

void SetGetGPSDataFlag(unsigned char isTrue);

unsigned char GetGPSDataFlag(void);

#endif
