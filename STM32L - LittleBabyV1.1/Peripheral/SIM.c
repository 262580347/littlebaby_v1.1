#include "main.h"
#include "SIM.h"



char APN_TAB[11][20] = {
//	"CMIOT",					//中国移动 "CMNET","CMIOT"
	"CMIOT",					//中国移动 "CMNET","CMIOT"
	"UNIM2M.NJM2MAPN",			//中国联通 "UNINET","UNIM2M.NJM2MAPN"
	"ctnet",					//中国电信
	"m-wap",					//越南MobiFone
	"m3-world",		//m-world 	//越南Vinaphone
	"NC",						//越南S-Fone				暂不支持
	"v-internet",				//越南Viettel
	"NC",						//越南Vietnamobile		暂不支持
	"NC",						//越南EVNTelecom			暂不支持
	"NC",						//越南Beeline			暂不支持
	"NC"						//越南3G EVNTelecom		暂不支持
};

static unsigned char s_GetGPSDataFlag = FALSE;



void SIM800CPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = SIM800C_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM800C_ENABLE_TYPE, &GPIO_InitStructure);	
			
}

void SIM800CProcess(unsigned short nMain10ms)
{
//	u8 i;
	u16 nTemp = 0;
	char str[512];
	static u16 s_LastTime = 0, s_OverCount = 0, s_ATAckCount = 0;
	static unsigned int s_RTCSumSecond = 0, s_AliveRTCCount = 0xfffe0000;
	static u8 s_APNFlag = 0, s_APNTag, s_State = 1, s_Step = 1, RunOnce = FALSE, s_ErrCount = 0, s_RetryCount = 0, s_Reboot = 0, s_LocationFailCount = 0;
	u16 MCC = 0;
	u8  MNC = 0;
	static u8 SinalStrength = 0;
	char * p1 = NULL, *p_str = NULL, buf[10];
	static float s_Longitude = 0.0;	//经度
	static float s_Latitude = 0.0;	//纬度
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	static u8 s_LastRTCDate = 32;
	static u16 s_WaitTime = 10;
	static u8 s_StartCalTimeFlag = TRUE;	//开始计算上报时间标志
	static u16 s_StartTime = 0, s_EndTime = 0, s_LinkTime = 0;
	static u8 s_LinkTimeRunFlag = FALSE;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
		
		RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
		s_LastRTCDate = RTC_DateStruct.RTC_Date;
	}
		
//	if(GetGPSTaskRunFlag())		//L76X  GPS定位
//	{
//		GPS_Process(nMain10ms);
//		return ;
//	}
	
	if(s_LinkTimeRunFlag)
	{		
		if( CalculateTime(GetSystem100msCount(), s_LinkTime) >= 1800 )	//100s超时
		{
			s_LinkTimeRunFlag = FALSE;
			u1_printf(" 180s连接超时 休眠\r\n");
			s_LinkTime = GetSystem100msCount();
			s_RetryCount = 0;
			s_State = 53;
			s_Reboot++;
			s_Step = 1;
			s_ErrCount = 0;
		}
	}
	
	switch(s_State)
	{	
		case 1:   //模块断电
			s_RetryCount++;
			SetTCModuleReadyFlag(FALSE);
			if(s_RetryCount >= 2)
			{
				s_RTCSumSecond = GetRTCSecond();	
				u1_printf(" 超时未连上GPRS\r\n");
				SetLogErrCode(LOG_CODE_RETRY);
				s_Reboot++;
				if(s_Reboot >= 6)
				{
					s_Reboot = 0;
					u1_printf(" SIM长时间链接不上,设备重启\r\n");
					delay_ms(100);
					SetLogErrCode(LOG_CODE_RESET);
					StoreOperationalData();
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					Sys_Soft_Reset();	
				}
				s_RetryCount = 0;
				s_State = 53;
				s_Step = 1;
				break;
			}
			SIM800CPortInit();
			SIM_PWR_OFF();
			SetTCProtocolRunFlag(FALSE);
			USART2_Config(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_State++;
			u1_printf("\r\n[SIM]模块断电，延时1s.\r\n");
			s_LinkTimeRunFlag = TRUE;
			s_LinkTime = GetSystem100msCount();
		break;
		 
		case 2:	//断电后延时5S上电，模块上电自动开机
			
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 100 )
			{
				u1_printf("\r\n[SIM800C]模块上电，延时15s.\r\n");
				SIM_PWR_ON();
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
			
			if(s_StartCalTimeFlag)
			{
				s_StartCalTimeFlag = FALSE;
				s_StartTime = GetSystem10msCount();
			}
			
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1500 )
			{
				Debug_Out("\r\n[SIM800C]模块上电完成，检测模块波特率.\r\n");
				u2_printf("AT\r");
				delay_ms(50);
				u2_printf("AT\r");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
			
		case 4:  	//检测模块的波特率
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 50 )
			{
				Clear_Uart2Buff();
				u2_printf("AT+CIURC=0\r");	//发送"AT\r"，查询模块是否连接，返回"OK"
				Debug_Out("[SIM800C]AT+CIURC=0\r\n");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
		
		case 5:	//延时100MS,检测响应数据
			if(g_Uart2RxFlag == TRUE)
			{
				g_Uart2RxFlag = FALSE;
				
				if(strstr((char *)g_USART2_RX_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_State++;	
					s_WaitTime = 10;
					u1_printf("\r\n  Close Call Ready\r\n");
				}			
				Clear_Uart2Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 30 )
			{				
					Debug_Out("[SIM800C]OK无响应\r\n");
					s_State--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 10 )
					{
						SetLogErrCode(LOG_CODE_NOMODULE);
						s_ErrCount = 0;
						s_State = 1;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 6:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= s_WaitTime) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				Debug_Out(str);		

				switch(s_Step)
				{
					case 1:		u2_printf("ATE0\r");		Debug_Out("[SIM]->ATE0\r\n");					break;
					case 2:		u2_printf("AT+CPIN?\r");	Debug_Out("[SIM]->查询模块SIM状态\r\n");			break;
					case 3:		u2_printf("AT+CCID\r");		Debug_Out("[SIM]->查询ICCID\r\n");				break;
					case 4:		u2_printf("AT+CIMI\r");		Debug_Out("[SIM]->查询IMSI\r\n");				break;
					case 5:		u2_printf("AT+GSV\r");		Debug_Out("[SIM]->查询模块版本号\r\n");			break;
					case 6:		u2_printf("AT+CSQ\r");		Debug_Out("[SIM]->查询模块信号质量\r\n");			break;
					case 7:		u2_printf("AT+COPS?\r");	Debug_Out("[SIM]->查询网络运营商\r\n");			break;	
					case 8:		u2_printf("AT+CGREG?\r");	Debug_Out("[SIM]->查询模块GPRS网络注册状态\r\n");	break;			//查询ps 数据域状态 ，网络 注册状态		
					case 9:		u2_printf("AT+CGATT?\r");	Debug_Out("[SIM]->查询模块GPRS附着状态\r\n");		break;			
					case 10:	u2_printf("AT+CGATT=1\r");	Debug_Out("[SIM]->设置模块附着GPRS状态\r\n");		break;
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
			}	
					
		break;
		
		case 7:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					
					Debug_OutReceive_ASC((char *)g_USART2_RX_BUF, g_USART2_RX_CNT);
												
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();							
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 2:
						//	u1_printf("检测SIM卡： ");
							if((strstr((const char *)g_USART2_RX_BUF,"+CPIN: READY")) != NULL )
							{					
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();		
								u1_printf("\r\n  SIM Card OK\r\n");
							}
							else
							{
								
								s_State--;	
								s_ErrCount++;
								if(s_ErrCount >= 5)
								{
									SetLogErrCode(LOG_CODE_NO_CARD);
									u1_printf(" No Card!\r\n");
									s_ErrCount = 0;
									s_State = 1;
									s_Step = 1;
								}					
							}						
						break;	
							
						case 3:				//ICCID查询
							//中国移动:898600；898602；898604；898607 
							//中国联通:898601、898606、898609
							//中国电信:898603、898611。
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();							
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 4:			//IMSI查询
							//中国移动:46000、46002、46004、46007、46008
							//中国联通:46001、46006、46009
							//中国电信:46003、46005、46011。
								//越南MCC:452
								//越南MNC   	运营商				APN:
								// 	  01 	MobiFone			m-wap
								// 	  02 	Vinaphone			m3-world / m-world
								// 	  03 	S-Fone 
								// 	  04 	Viettel Mobile		v-internet
								// 	  05 	Vietnamobile
								// 	  06 	EVNTelecom
								// 	  07 	Beeline VN
								// 	  08 	3G EVNTelecom
								if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )			
								{
									MCC = (g_USART2_RX_BUF[2] - '0')*100 + (g_USART2_RX_BUF[3] - '0')*10 +(g_USART2_RX_BUF[4] - '0');
									MNC = (g_USART2_RX_BUF[5] - '0')*10 + (g_USART2_RX_BUF[6] - '0');
									
									s_APNFlag = 0;	
									switch(MCC)
									{
										case	MCC_CHINA:
											switch(MNC)
											{
												//移动卡
												case 0:
												case 2:
												case 4:
												case 7:
												case 8:
													s_APNFlag = 1;	
													s_APNTag = CMNET;
													break;
												//联通卡
												case 1:
												case 6:
												case 9:
													s_APNFlag = 1;	
													s_APNTag = UNINET;
													break;	
												//电信卡
												case 3:
												case 5:
												case 11:
													s_APNFlag = 1;	
													s_APNTag = CTNET;
													break;										
												default:
													s_APNFlag = 0;	
													s_APNTag = MCC_UNKOWN;
													break;
											}
											break;
											
										case	MCC_VIETNAM:
											switch(MNC)
											{
												//移动卡
												case 1:
													s_APNFlag = 1;	
													s_APNTag = MOBIFONE;
													break;
												case 2:
													s_APNFlag = 1;	
													s_APNTag = VINAPHONE;
													break;
												case 4:
													s_APNFlag = 1;	
													s_APNTag = VIETTEL;
													break;							
												default:
													s_APNFlag = 0;	
													s_APNTag = MCC_UNKOWN;
													break;
											}
											break;
										default:
											s_APNFlag = 0;	
											s_APNTag = MCC_UNKOWN;
											break;
									}						

									if(s_APNFlag)
									{
										sprintf(str, "IMSI: APN SET TO %s\r\n", (char *)APN_TAB[s_APNTag]);
										Debug_Out(str);	
										s_Step++;	
										s_State--;		
										s_ErrCount = 0;		
									}
									else
									{
										Debug_Out("IMSI: GET IMSI FAIL\r\n");
										s_State--;		
										s_ErrCount ++;	
									}
									s_LastTime = GetSystem10msCount();	
								}
								else
								{
									s_State--;		
									s_ErrCount ++;
								}
						break;
							
						case 5:
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
							//	u1_printf("序列号：%s\r\n", g_USART2_RX_BUF);
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();							
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 6:	
						//	u1_printf("检测信号质量： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CSQ:")) != NULL )
							{							
								//u1_printf("%s\r\n", g_USART2_RX_BUF);							
								strncpy(buf,p1+6,2);
								nTemp = atoi( buf);
								SetCSQValue(nTemp);
								SinalStrength = nTemp;
								if( nTemp < 6 )								
								{
									s_State--;	
									s_ErrCount++;
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
									Debug_Out(" 信号差\r\n");
								}
								else
								{
									s_State--;	
									s_Step++;	
									s_ErrCount = 0;
									ClearLogErrCode(LOG_CODE_WEAKSIGNAL);
									u1_printf("\r\n  信号正常\r\n");
								}
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}		
						break;		
							
						case 7:
							//u1_printf(" 运营商名称： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+COPS:")) != NULL )
							{				
								if((strstr((const char *)g_USART2_RX_BUF,"CHINA MOBILE")) != NULL )  
								{
									Debug_Out("COPS: CHINA MOBILE\r\n");

									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"UNICOM")) != NULL )  
								{
									Debug_Out("COPS: UNICOM\r\n");
									
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if(s_APNFlag)  	//电信运营商信息不一定正确
								{
									Debug_Out("COPS: UNRECOGNIZABLED\r\n");
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else
								{
									Debug_Out("COPS: UNRECOGNIZABLED NETWORK\r\n");
									s_State--;		
									s_ErrCount ++;
								}
									
								s_LastTime = GetSystem10msCount();		
							//	u1_printf("%s\r\n", g_USART2_RX_BUF);
							}
							else
							{
								Debug_Out("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}
						break;										
							
						case 8:
							//u1_printf(" 模块注册网络： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGREG:")) != NULL )
							{						
								//u1_printf("%s\r\n", g_USART2_RX_BUF);
								
								nTemp = atoi( (const char *) (p1+10));
								switch( nTemp )								
								{
									case 0:
									Debug_Out("[SIM800C]模块注册--未注册\r\n");
									s_State--;
									s_ErrCount++;
									s_WaitTime = WAIT_50;
									break;
									
									case 1:
									u1_printf("\r\n[SIM800C]模块注册网络--成功\r\n");
									s_State--;
									s_Step++;
									s_Step++;		//调过GSM查询
									s_ErrCount = 0;
									s_WaitTime = WAIT_10;
									break;
									
									case 2:
									sprintf(str, "[SIM800C]模块注册--正在注册，查询次数=%d\r\n",s_ErrCount);
									Debug_Out(str);
									s_State--;
									s_ErrCount++;
									s_WaitTime = WAIT_50;

									break;
									
									case 3:
									sprintf(str, "\r\n [SIM800C]模块注册--注册被拒, 次数=%d\r\n",s_ErrCount);
									Debug_Out(str);
									s_State--;
									s_ErrCount++;
									s_WaitTime = WAIT_50;
									break;
									
									default:
									Debug_Out("[SIM800C]模块注册--注册状态未识别\r\n");
									s_State--;
									s_ErrCount++;
									s_WaitTime = WAIT_50;
									break;	
								}
								
								if(s_ErrCount >= 60)
								{		
									u1_printf("\r\n  GPRS注册网络失败，查询GSM\r\n");
									s_ErrCount = 0;								
									s_Step++;
								}
								
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_State--;	
								s_ErrCount++;
								s_WaitTime = WAIT_50;
							}	
								
						break;
														
						case 9:	
							//u1_printf("GPRS附着状态： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGATT:")) != NULL )
							{					
								//u1_printf("%s\r\n", g_USART2_RX_BUF);
								
								nTemp = atoi( (const char *) (p1+8));
								if( nTemp == 0 )								
								{
									s_State--;	
									s_ErrCount++;
									s_WaitTime = WAIT_50;
									if(s_ErrCount >= 20)
									{									
										s_Step++;
										s_ErrCount = 0;
										s_WaitTime = WAIT_10;
									}
								}
								else
								{
									ClearLogErrCode(LOG_CODE_UNATTACH);
									if(s_GetGPSDataFlag)
									{
										s_State++;	
									}
									else
									{
										s_State = 10;
									}
									s_ATAckCount = 0;
									s_Step = 1;	
									s_ErrCount = 0;
									s_WaitTime = WAIT_10;
								}
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}		
						break;		
							
						case 10:
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
								s_Step--;	
								s_State--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();	
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
							
						break;
							
					}
					
					if(s_ErrCount >= 200 )
					{
						s_ErrCount = 0;
						s_State = 1;
						s_Step = 1;
						s_WaitTime = WAIT_10;
					}
					
					Clear_Uart2Buff();
					s_LastTime = GetSystem10msCount();
				}				
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1000) 
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
					Debug_Out(str);	
		
					s_ATAckCount++;
					
					if(s_ATAckCount >= 5)
					{
						s_ATAckCount = 0;
						SetLogErrCode(LOG_CODE_TIMEOUT);
						u1_printf(" AT无响应\r\n");
						s_State = 1;
						s_Step = 1;
					}
					else
					{
						s_State--;
						Debug_Out(" AT指令超时\r\n");
					}
					s_LastTime = GetSystem10msCount();
					s_ErrCount++;
				}	
			}		
		break;

		case 8:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 15) 
			{
				switch(s_Step)
				{
					case 1:	u2_printf("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"\r");		Debug_Out("[SIM]->设置网络参数\r\n");		break;
					case 2:	u2_printf("AT+SAPBR=3,1,\"APN\",\"CMNET\"\r");			Debug_Out("[SIM]->设置APN\r\n");			break;
					case 3:	u2_printf("AT+SAPBR=1,1\r");			Debug_Out("[SIM]->激活移动场景\r\n");						break;
					case 4:	u2_printf("AT+SAPBR=2,1\r");			Debug_Out("[SIM]->获取IP\r\n");								break;
					case 5:	u2_printf("AT+CLBSCFG=0,3\r"); 			Debug_Out("[SIM]->查询地址信息\r\n");						break;
					case 6:	u2_printf("AT+CLBS=1,1\r");				Debug_Out("[SIM]->获取定位\r\n");							break;
					case 7:	u2_printf("AT+SAPBR=0,1\r");			Debug_Out("[SIM]->关闭移动场景\r\n\r\n");					break;	
					
					default:						
						s_State = 9;	// 跳至 10
						s_Step = 1;
					break;
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		
		break;
			
		case 9:
			if(g_Uart2RxFlag == TRUE)
			{
				g_Uart2RxFlag = FALSE;
				
				Debug_OutReceive_ASC((char *)g_USART2_RX_BUF, g_USART2_RX_CNT);
											
				switch(s_Step)
				{
					case 1:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
													
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 2:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
													
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 3:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL)
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
												
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 4:
						if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
					
					case 5:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL)
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
												
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 6:
						if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							memcpy(str, g_USART2_RX_BUF, g_USART2_RX_CNT);
							p_str = strtok(str, ",");
							p_str = strtok(NULL, ",");  
							s_Longitude = atof(p_str);
							SetLongitude(s_Longitude);
							
							
							p_str = strtok(NULL, ",");  
							s_Latitude = atof(p_str);
							SetLatitude(s_Latitude);
							
							s_LocationFailCount = 0;
							sprintf(str, "\r\n 经度:%f  纬度:%f \r\n", s_Longitude, s_Latitude);
							Debug_Out(str);
							SetGetGPSDataFlag(TRUE);
						}
						else
						{
							s_State--;	
							s_ErrCount++;

							
						}
						
					break;
						
					case 7:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{					
							s_Step = 1;	
							s_State++;		
							s_ATAckCount = 0;
							s_ErrCount = 0;
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}						
					break;
					
					default:

					break;
						
				}
				
				if(s_ErrCount >= 20 )
				{
					s_ErrCount = 0;
					s_State = 10;
					s_Step = 1;
				}
				s_LastTime = GetSystem10msCount();	
				
				Clear_Uart2Buff();

			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1200) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				Debug_Out(str);
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 2)
				{
					s_ATAckCount = 0;
					Debug_Out(" AT无响应\r\n");
					s_State++;
					s_Step = 1;
					SetLogErrCode(LOG_CODE_TIMEOUT);
				}
				else
				{
					s_State--;
					Debug_Out(" AT指令超时\r\n");
					s_LocationFailCount++;
					if(s_LocationFailCount >= 3)
					{
						s_LocationFailCount = 0;
						SetGetGPSDataFlag(FALSE);
					}
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}	
		break;		
			
		case 10:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 15) 
			{
				switch(s_Step)
				{
						case 1:
							u2_printf("AT+CIPCLOSE=1\r");
							Debug_Out("[SIM]->关闭当前链接\r\n");
						break;
						case 2:
							u2_printf("AT+CIPSHUT\r");
							Debug_Out("[SIM]->关闭所有链接\r\n");
						break;
						case 3:
							u2_printf("AT+CIPMODE=1\r");
							Debug_Out("[SIM]->设置为透传模式\r\n");
						break;
						case 4:
							//u2_printf("AT+CSTT?\r");
							if(s_APNFlag)
							{
								u2_printf("AT+CSTT=\"%s\"\r",(char *)APN_TAB[s_APNTag]);
								sprintf(str, "[SIM]->建立APN:%s\r\n",(char *)APN_TAB[s_APNTag]);
							}	
							else 
							{								
								u2_printf("AT+CSTT?\r");
								sprintf(str, "[SIM]->未能识别的运营商\r\n");	
							}
							Debug_Out(str);
						break;
						case 5:
							u2_printf("AT+CIICR\r");
							Debug_Out("[SIM]->建立无线链路GPRS\r\n");
						break;
						case 6:
							u2_printf("AT+CIFSR\r");
							Debug_Out("[SIM]->获得本地IP地址\r\n\r\n");
						break;
						
						case 7:
						break;
						
						default:
							s_State = 0;	// =1
							s_Step = 1;
						break;
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
				s_WaitTime = WAIT_10;
			}
		
		break;
	
		case 11:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) < s_WaitTime) 
			{
				break;
			}
			else
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;				
					
					Debug_OutReceive_ASC((char *)g_USART2_RX_BUF, g_USART2_RX_CNT);
					
					if((strstr((const char *)g_USART2_RX_BUF,"SMS Ready")) != NULL )
					{	
						s_ErrCount ++;
						u1_printf("\r\n  SMS非法指令\r\n");		
					}	
					else
					{						
						switch(s_Step)
						{
							case 1:
								if((strstr((const char *)g_USART2_RX_BUF,"+CIPCLOSE")) != NULL )
								{
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
															
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
								{
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
									
								}
								else
								{
									s_State--;	
									s_ErrCount++;
								}
							break;
								
							case 2:
								if((strstr((const char *)g_USART2_RX_BUF,"SHUT OK")) != NULL )
								{
								//	Debug_Out("关闭移动场景\r\n");
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
															
								}
								else
								{
									s_State--;	
									s_ErrCount++;
								}
							break;
								
							case 3:
								if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
								{
								//	Debug_Out("设置为透传模式\r\n");
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
														
								}
								else
								{
									s_State--;	
									s_ErrCount++;
								}
							break;
								
							case 4:
								if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
								{
								//	Debug_Out("APN OK\r\n");
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
															
								}
								else
								{
									s_State--;	
									s_ErrCount++;
								}
							break;
							
							case 5:
								if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
								{
								//	Debug_Out("激活移动场景 OK\r\n");
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
														
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"+PDP: DEACT")) != NULL )
								{
									s_State--;	
									s_Step = 1;
									s_ErrCount++;
								}
								else
								{
									s_State--;	
									s_ErrCount++;
									
									if(s_ErrCount >= 10)
									{
										u1_printf(" 跳过激活移动场景\r\n");
										s_Step++;	
										s_State = 10;		
										s_ErrCount = 0;
									}
								}
								
							break;
								
							case 6:
								//Debug_Out("模块本地IP：");
								if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
								{					
									s_Step = 1;	
									s_State++;		
									s_ATAckCount = 0;
									s_ErrCount = 0;
								
									//sprintf(str, "%s\r\n", g_USART2_RX_BUF));
									//Debug_Out(str);
								}
								else
								{
									Debug_Out("    Error\r\n");
									s_State--;	
									s_ErrCount++;
									
									if(s_ErrCount >= 10)
									{
										u1_printf(" 跳过获取本地IP\r\n");
										s_Step++;	
										s_State = 12;		
										s_ErrCount = 0;
									}
								}						
							break;
							
							case 7:	
								sprintf(str, "模块收到数据包含IP头=%s\r\n", g_USART2_RX_BUF);
								Debug_Out(str);
								s_State++;
								s_Step = 1;
							break;
								
						}
					}
					
					if(s_ErrCount >= 50 )
					{
						s_ErrCount = 0;
						s_State = 1;
						s_Step = 1;
					}
					s_LastTime = GetSystem10msCount();	
					
					Clear_Uart2Buff();

				}				
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 800) 
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
					Debug_Out(str);		
		
					s_ATAckCount++;
					
					if(s_ATAckCount >= 5)
					{
						s_ATAckCount = 0;
						u1_printf(" AT无响应\r\n");
						SetLogErrCode(LOG_CODE_TIMEOUT);
						s_State = 1;
						s_Step = 1;
					}
					else
					{
						s_State--;
						Debug_Out(" AT指令超时\r\n");
					}
					s_LastTime = GetSystem10msCount();
					s_ErrCount++;
				}
			}	
		break;		
		
		case 12:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 35) 
			{
				Debug_Out("[SIM800C]建立TCP链接\r\n");
				sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d\r"
				,p_sys->Gprs_ServerIP[0]
				,p_sys->Gprs_ServerIP[1]
				,p_sys->Gprs_ServerIP[2]
				,p_sys->Gprs_ServerIP[3]
				,p_sys->Gprs_Port	);

				u2_printf(str);	
				Debug_Out(str);	//串口输出AT命令
				Debug_Out("\n");
				s_LastTime = GetSystem10msCount();
				s_State++;		
			}
		//	s_ErrCount = 0;		
			s_WaitTime = WAIT_10;
		break;
		
		case 13:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) < s_WaitTime) 
			{
				break;
			}
			else
			{
				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					
					Debug_OutReceive_ASC((char *)g_USART2_RX_BUF, g_USART2_RX_CNT);	
					
					if((strstr((const char *)g_USART2_RX_BUF,"SMS Ready")) != NULL )
					{	
						s_ErrCount ++;
						u1_printf("\r\n  SMS非法指令\r\n");		
					}	
					else
					{
						if((strstr((const char *)g_USART2_RX_BUF,"+ALREADY CONNECT")) != NULL )
						{
							s_Step = 1;	
							s_State++;		
							s_ErrCount = 0;
							SetTCModuleReadyFlag(TRUE);			//连接成功======================== 	
							u1_printf("\r\n  GPRS链接已建立\r\n");				
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT OK")) != NULL )
						{
							s_Step = 1;	
							s_State++;		
							s_ErrCount = 0;
							SetTCModuleReadyFlag(TRUE);			//连接成功========================
							u1_printf("\r\n  GPRS链接成功\r\n");				
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT FAIL")) != NULL )
						{		
							s_ErrCount++;
							s_State = 10;
							s_Step = 1;	
							Debug_Out("GPRS链接失败\r\n");				
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT")) != NULL )
						{
							s_ATAckCount = 0;
							s_Step = 1;	
							s_State++;		
							s_ErrCount = 0;
							SetTCModuleReadyFlag(TRUE);		//连接成功========================
							u1_printf("\r\n  GPRS链接成功\r\n");				
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{		
							Debug_Out("\r\n  等待Connect...\r\n");	
							s_LastTime = GetSystem10msCount();				
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
						{		
							s_ErrCount++;
							s_State--;		
						}
						else
						{
							s_State--;	
							s_ErrCount++;
							Debug_Out("\r\n  未识别指令\r\n");
							//连接成功========================
							if(s_ErrCount >= 10)
							{
								s_Step = 1;	
								s_State = 14;		
								s_ErrCount = 0;
								SetTCModuleReadyFlag(TRUE);			
								u1_printf("\r\n  尝试发送数据\r\n");		
							}
						}
					
					}
						if(s_ErrCount >= 20 )
						{
							s_ErrCount = 0;
							s_State = 1;
							s_Step = 1;
						}
						s_LastTime = GetSystem10msCount();	
						
						Clear_Uart2Buff();
					
					}									
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1500) 
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
		
					s_Step = 1;	
					s_State = 14;		
					s_ErrCount = 0;
					SetTCModuleReadyFlag(TRUE);	
					u1_printf(" \r\n AT无响应,尝试发送数据\r\n");
					SetLogErrCode(LOG_CODE_TIMEOUT);
					s_LastTime = GetSystem10msCount();
				}	

			}		
		break;
		
		case 14:
			s_OverCount = 0;
			s_State = 50;
			SetDataPackFlag(FALSE);
			SetServerConfigCmdFlag(FALSE);
			SetTCProtocolRunFlag(TRUE);
			s_LastTime = GetSystem10msCount();
			u1_printf("\r\n SinalStrength:%d\r\n", SinalStrength);
		break;
		
		case 50:
			if(GetTCModuleReadyFlag())		//SIM就绪
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	
				{
					s_AliveRTCCount = GetRTCSecond();
					Mod_Send_Alive(SIM_COM);
					delay_ms(200);
					break;
				}
				
				s_EndTime = GetSystem10msCount();
				SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
				u1_printf("\r\n 连接耗时:%.2fs\r\n\r\n", CalculateTime(s_EndTime, s_StartTime)/100.0);
				Mod_Report_LowPower_Data(SIM_COM);	//发送数据包
				s_State++;
				s_LastTime = GetSystem10msCount();

			}
			else
			{
				s_State = 1;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 51:
			if(GetDataPackFlag())//非长连接设备取消心跳包  2019.2.12
			{
				s_State++;
				s_LastTime = GetSystem10msCount();
				SetDataPackFlag(FALSE);
				
				s_OverCount = 0;
				SetLogErrCode(LOG_CODE_SUCCESS);
				WriteCommunicationSuccessCount();
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1000 )
			{
				s_State--;
				s_LastTime = GetSystem10msCount();
				u1_printf("\r\n  超时未收到回复包  重发\r\n");
				Clear_Uart2Buff();
				s_OverCount++;
				if(s_OverCount >= 2)
				{
					SetLogErrCode(LOG_CODE_NOSERVERACK);
					s_OverCount = 0;
					s_State = 1;
					s_Step = 1;
					s_ErrCount = 0;
				}
			}		
		break;
					
		case 52:
			if(GetServerConfigCmdFlag() == FALSE)
			{
				if(CalculateTime(GetSystem10msCount(),s_LastTime) >= 100)		//延时1s等待GPRS下行数据
				{
					u1_printf("\r\n  等待服务器下行数据超时，休眠\r\n");
					s_State++;
				}
			}
			else if(GetServerConfigCmdFlag())		//收到关闭Socket指令，可以休眠
			{
				u1_printf("\r\n  收到应答包,休眠\r\n");
				s_State++;
				SetServerConfigCmdFlag(FALSE);
			}
		break;
		
		case 53:
			TXB0108_GPIO_Reset();
			SIM_PWR_OFF();
			s_RTCSumSecond = GetRTCSecond();
			s_State++;
			s_LastTime = GetSystem10msCount();
			s_LinkTimeRunFlag = FALSE;
			StoreOperationalData();
			WriteCommunicationCount();
		break;
					
		case 54:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();	
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
				SetTCProtocolRunFlag(FALSE);	
				SetLogErrCode(LOG_CODE_CLEAR);					
				u1_printf("\r\n [%02d:%02d:%02d][SIM]重新上电\r\n", RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				s_RetryCount = 0;
				s_StartCalTimeFlag = TRUE;
//				StartSensorMeasure();			//清传感器标志，开启传感器检测
				s_State++;
			}		
			else			//未到上报时间，继续休眠
			{
				if((Sht31Status != SHT_WORKING) && (MaxStatus != MAX_WORKING) && (BmpStatus != BMP_WORKING) && (BatStatus != BAT_WORKING))
				{
					s_LastTime = GetSystem10msCount();
					if(GetTCModuleReadyFlag())
					{
						SetStopModeTime(TRUE, STOP_TIME);
					}
					else
					{
						SetStopModeTime(TRUE, ERR_STOP_TIME);
					}
				}					
			}
		break;
			
		case  55:
			s_State = 1;
			s_Step = 1;
			s_ErrCount = 0;
			s_LastTime = GetSystem10msCount();
			SetTCModuleReadyFlag(FALSE);	

			RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
			
			if(s_LastRTCDate != RTC_DateStruct.RTC_Date)	//日期更新，获取经纬度
			{
				s_LastRTCDate = RTC_DateStruct.RTC_Date;
				SetGPSTaskRunFlag(TRUE);
				SetGetGPSDataFlag(FALSE);
			}

		break;		
			
		default:
			u1_printf(" 意外错误\r\n");
			s_State = 1;
			s_Step = 1;
		break;
	}
	
}



