#include "main.h"
#include "L76X.h"
#include <math.h>

//#define COM_DATA_SIZE	256

#define		GET_GPS_COUNT		10		//获取10次经纬度数据
//卫星信息
__packed typedef struct  
{										    
 	u8 Num;		//卫星编号
	u8 Eledeg;	//卫星仰角
	u16 Azideg;	//卫星方位角
	u8 SN;		//信噪比		   
}NMEA_SATELLITE_MSG; 
//北斗 NMEA-0183协议重要参数结构体定义 
//卫星信息
__packed typedef struct  
{	
 	u8 BD_Num;		//卫星编号
	u8 BD_Eledeg;	//卫星仰角
	u16 BD_Azideg;	//卫星方位角
	u8 BD_SN;		//信噪比		   
}NMEA_BD_SATELLITE_MSG; 

//UTC时间信息
__packed typedef struct  
{										    
 	u16 Year;	//年份
	u8 Month;	//月份
	u8 date;	//日期
	u8 Hour; 	//小时
	u8 Min; 	//分钟
	u8 Sec; 	//秒钟
}NMEA_UTC_TIME;   	
//NMEA 0183 协议解析后数据存放结构体
__packed typedef struct  
{										    
 	u8 Satellite_Num;					//可见GPS卫星数
	u8 BD_Satellite_Num;					//可见GPS卫星数
	NMEA_SATELLITE_MSG 		Sl_Msg[12];		//最多12颗GPS卫星
	NMEA_BD_SATELLITE_MSG 	BD_Sl_Msg[12];		//暂且算最多12颗北斗卫星
	NMEA_UTC_TIME 			UTC_Time;			//UTC时间
	u32 Latitude;				//纬度 分扩大100000倍,实际要除以100000
	u8 N_S_Hemi;					//北纬/南纬,N:北纬;S:南纬				  
	u32 Longitude;			    //经度 分扩大100000倍,实际要除以100000
	u8 E_W_Hemi;					//东经/西经,E:东经;W:西经
	u8 GPS_Status;					//GPS状态:0,未定位;1,非差分定位;2,差分定位;6,正在估算.				  
 	u8 Position_Num;				//用于定位的GPS卫星数,0~12.
 	u8 Position_Satellite[12];		//用于定位的卫星编号
	u8 FixMode;					//定位类型:1,没有定位;2,2D定位;3,3D定位
	u16 Pdop;					//位置精度因子 0~500,对应实际值0~50.0
	u16 Hdop;					//水平精度因子 0~500,对应实际值0~50.0
	u16 Vdop;					//垂直精度因子 0~500,对应实际值0~50.0 

	int Altitude;			 	//海拔高度,放大了10倍,实际除以10.单位:0.1m	 
	u16 Speed;					//地面速率,放大了1000倍,实际除以10.单位:0.001公里/小时	 
}NMEA_MSG;

static NMEA_MSG s_GPS_Msg;		//GPS信息

static float s_LatitudeBuf[GET_GPS_COUNT], s_LongitudeBuf[GET_GPS_COUNT];

static float s_Latitude = 0, s_Longitude = 0;

static u8 s_GetGPSDataFlag = FALSE;	//是否获取到GPS数据

unsigned char GetGPSDataFlag(void)
{
	return s_GetGPSDataFlag;
}

void SetGetGPSDataFlag(unsigned char isTrue)
{
	s_GetGPSDataFlag = isTrue;
}

float GetLongitude(void)
{
	return s_Longitude;
}

void SetLongitude(float temp)
{
	s_Longitude = temp;
}

float GetLatitude(void)
{
	return s_Latitude;
}

void SetLatitude(float temp)
{
	s_Latitude = temp;
}

GNRMC GPS;


static unsigned char s_GetGPSTaskRunFlag = FALSE;	//执行GPS定位进程标志

unsigned char GetGPSTaskRunFlag(void)
{
	return s_GetGPSTaskRunFlag;
}

void SetGPSTaskRunFlag(unsigned char isTrue)
{
	s_GetGPSTaskRunFlag = isTrue;
}

void ClrGPSState()
{
	GPS.Status = 0;
	GPS.Lon = 0;
	GPS.Lat = 0;
}

unsigned char GetGPSState(void)
{
	return GPS.Status;
}

float GetGPSLongitude(void)
{
	return GPS.Lon;
}

float GetGPSLatitude(void)
{
	return GPS.Lat;
}
//从buf里面得到第cx个逗号所在的位置
//返回值:0~0XFE,代表逗号所在位置的偏移.
//       0XFF,代表不存在第cx个逗号							  
u8 NMEA_Comma_Pos(u8 *Buff,u8 cx)
{	 		    
	u8 *p=Buff;
	while(cx)
	{		 
		if(*Buff=='*'||*Buff<' '||*Buff>'z')return 0XFF;//遇到'*'或者非法字符,则不存在第cx个逗号
		if(*Buff==',')cx--;
		Buff++;
	}
	return Buff-p;	 
}
//m^n函数
//返回值:m^n次方.
u32 NMEA_Pow(u8 m,u8 n)
{
	u32 result=1;	 
	while(n--)result*=m;    
	return result;
}
//str转换为数字,以','或者'*'结束
//Buff:数字存储区
//dx:小数点位数,返回给调用函数
//返回值:转换后的数值
int NMEA_Str2num(u8 *Buff,u8*dx)
{
	u8 *p=Buff;
	u32 ires=0,fres=0;
	u8 ilen=0,flen=0,i;
	u8 mask=0;
	int res;
	while(1) //得到整数和小数的长度
	{
		if(*p=='-'){mask|=0X02;p++;}//是负数
		if(*p==','||(*p=='*'))break;//遇到结束了
		if(*p=='.'){mask|=0X01;p++;}//遇到小数点了
		else if(*p>'9'||(*p<'0'))	//有非法字符
		{	
			ilen=0;
			flen=0;
			break;
		}	
		if(mask&0X01)flen++;
		else ilen++;
		p++;
	}
	if(mask&0X02)Buff++;	//去掉负号
	for(i=0;i<ilen;i++)	//得到整数部分数据
	{  
		ires+=NMEA_Pow(10,ilen-1-i)*(Buff[i]-'0');
	}
	if(flen>5)flen=5;	//最多取5位小数
	*dx=flen;	 		//小数点位数
	for(i=0;i<flen;i++)	//得到小数部分数据
	{  
		fres+=NMEA_Pow(10,flen-1-i)*(Buff[ilen+1+i]-'0');
	} 
	res=ires*NMEA_Pow(10,flen)+fres;
	if(mask&0X02)res=-res;		   
	return res;
}	  	
//分析GPGSV信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_GPGSV_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p,*p1,dx;
	u8 Length,i,j,slx=0;
	u8 Posx;   	 
	p=Buff;
	p1=(u8*)strstr((const char *)p,"$GPGSV");
	Length=p1[7]-'0';								//得到GPGSV的条数
	Posx=NMEA_Comma_Pos(p1,3); 					//得到可见卫星总数
	if(Posx!=0XFF)GPS_Msg->Satellite_Num=NMEA_Str2num(p1+Posx,&dx);
	for(i=0;i<Length;i++)
	{	 
		p1=(u8*)strstr((const char *)p,"$GPGSV");  
		for(j=0;j<4;j++)
		{	  
			Posx=NMEA_Comma_Pos(p1,4+j*4);
			if(Posx!=0XFF)GPS_Msg->Sl_Msg[slx].Num=NMEA_Str2num(p1+Posx,&dx);	//得到卫星编号
			else break; 
			Posx=NMEA_Comma_Pos(p1,5+j*4);
			if(Posx!=0XFF)GPS_Msg->Sl_Msg[slx].Eledeg=NMEA_Str2num(p1+Posx,&dx);//得到卫星仰角 
			else break;
			Posx=NMEA_Comma_Pos(p1,6+j*4);
			if(Posx!=0XFF)GPS_Msg->Sl_Msg[slx].Azideg=NMEA_Str2num(p1+Posx,&dx);//得到卫星方位角
			else break; 
			Posx=NMEA_Comma_Pos(p1,7+j*4);
			if(Posx!=0XFF)GPS_Msg->Sl_Msg[slx].SN=NMEA_Str2num(p1+Posx,&dx);	//得到卫星信噪比
			else break;
			slx++;	   
		}   
 		p=p1+1;//切换到下一个GPGSV信息
	}   
}
//分析GNGGA信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_GNGGA_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p1,dx;			 
	u8 Posx;    
	p1=(u8*)strstr((const char *)Buff,"$GNGGA");
	Posx=NMEA_Comma_Pos(p1,6);								//得到GPS状态
	if(Posx!=0XFF)GPS_Msg->GPS_Status=NMEA_Str2num(p1+Posx,&dx);	
	Posx=NMEA_Comma_Pos(p1,7);								//得到用于定位的卫星数
	if(Posx!=0XFF)GPS_Msg->Position_Num=NMEA_Str2num(p1+Posx,&dx); 
	Posx=NMEA_Comma_Pos(p1,9);								//得到海拔高度
	if(Posx!=0XFF)GPS_Msg->Altitude=NMEA_Str2num(p1+Posx,&dx);  
}
//分析GNGSA信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_GNGSA_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p1,dx;			 
	u8 Posx; 
	u8 i;   
	p1=(u8*)strstr((const char *)Buff,"$GNGSA");
	Posx=NMEA_Comma_Pos(p1,2);								//得到定位类型
	if(Posx!=0XFF)GPS_Msg->FixMode=NMEA_Str2num(p1+Posx,&dx);	
	for(i=0;i<12;i++)										//得到定位卫星编号
	{
		Posx=NMEA_Comma_Pos(p1,3+i);					 
		if(Posx!=0XFF)GPS_Msg->Position_Satellite[i]=NMEA_Str2num(p1+Posx,&dx);
		else break; 
	}				  
	Posx=NMEA_Comma_Pos(p1,15);								//得到PDOP位置精度因子
	if(Posx!=0XFF)GPS_Msg->Pdop=NMEA_Str2num(p1+Posx,&dx);  
	Posx=NMEA_Comma_Pos(p1,16);								//得到HDOP位置精度因子
	if(Posx!=0XFF)GPS_Msg->Hdop=NMEA_Str2num(p1+Posx,&dx);  
	Posx=NMEA_Comma_Pos(p1,17);								//得到VDOP位置精度因子
	if(Posx!=0XFF)GPS_Msg->Vdop=NMEA_Str2num(p1+Posx,&dx);  
}
//分析GNRMC信息
//GPS_Msg:nmea信息结构体
//Buff:接收到的GPS数据缓冲区首地址
void NMEA_GNRMC_Analysis(NMEA_MSG *GPS_Msg,u8 *Buff)
{
	u8 *p1,dx;			 
	u8 Posx;     
	u32 Temp;	   
	float rs;  
	p1=(u8*)strstr((const char *)Buff,"$GNRMC");//"$GNRMC",经常有&和GNRMC分开的情况,故只判断GPRMC.
	Posx=NMEA_Comma_Pos(p1,1);								//得到UTC时间
	if(Posx!=0XFF)
	{
		Temp=NMEA_Str2num(p1+Posx,&dx)/NMEA_Pow(10,dx);	 	//得到UTC时间,去掉ms
		GPS_Msg->UTC_Time.Hour=Temp/10000;
		GPS_Msg->UTC_Time.Min=(Temp/100)%100;
		GPS_Msg->UTC_Time.Sec=Temp%100;	 	 
	}	
	Posx=NMEA_Comma_Pos(p1,3);								//得到纬度
	if(Posx!=0XFF)
	{
		Temp=NMEA_Str2num(p1+Posx,&dx);		 	 
		GPS_Msg->Latitude=Temp/NMEA_Pow(10,dx+2);	//得到°
		rs=Temp%NMEA_Pow(10,dx+2);				//得到'		 
		GPS_Msg->Latitude=GPS_Msg->Latitude*NMEA_Pow(10,5)+(rs*NMEA_Pow(10,5-dx))/60;//转换为° 
	}
	Posx=NMEA_Comma_Pos(p1,4);								//南纬还是北纬 
	if(Posx!=0XFF)GPS_Msg->N_S_Hemi=*(p1+Posx);					 
 	Posx=NMEA_Comma_Pos(p1,5);								//得到经度
	if(Posx!=0XFF)
	{												  
		Temp=NMEA_Str2num(p1+Posx,&dx);		 	 
		GPS_Msg->Longitude=Temp/NMEA_Pow(10,dx+2);	//得到°
		rs=Temp%NMEA_Pow(10,dx+2);				//得到'		 
		GPS_Msg->Longitude=GPS_Msg->Longitude*NMEA_Pow(10,5)+(rs*NMEA_Pow(10,5-dx))/60;//转换为° 
	}
	Posx=NMEA_Comma_Pos(p1,6);								//东经还是西经
	if(Posx!=0XFF)GPS_Msg->E_W_Hemi=*(p1+Posx);		 
	Posx=NMEA_Comma_Pos(p1,9);								//得到UTC日期
	if(Posx!=0XFF)
	{
		Temp=NMEA_Str2num(p1+Posx,&dx);		 				//得到UTC日期
		GPS_Msg->UTC_Time.date=Temp/10000;
		GPS_Msg->UTC_Time.Month=(Temp/100)%100;
		GPS_Msg->UTC_Time.Year=2000+Temp%100;	 	 
	} 
}

void L76X_Send_Command(char *data)
{
	char str[100];
    char Check = data[1];
    u8 i = 0;
    //printf(" 1i = %d Check =%x \n", i, Check);
    for(i=2; data[i] != '\0'; i++){
        Check ^= data[i];       //Calculate the check value
    }
    //printf(" i = %d Check =%x \n", i, Check);

    
//	sprintf(str, "%s*%s\r\n", data, Check_char);
	sprintf(str, "%s\r\n", data);
    Uart_Send_Str(GPS_COM, str);
    Debug_Out(str);
//    Uart_Send_Str(GPS_COM, data);
//    Uart_Send_Str(GPS_COM, "*");
//    Uart_Send_Str(GPS_COM, Check_char);
//	Uart_Send_Str(GPS_COM, "\r\n");
}

GNRMC L76X_Gat_GNRMC(char * buff_t, u16 lenth)
{
    u16 add = 0, x = 0, z = 0, i = 0;
    u32 Time = 0, latitude = 0, longitude = 0;
//	char * p1;

    GPS.Status = 0;

	GPS.Time_H = 0;
    GPS.Time_M = 0;
    GPS.Time_S = 0;
	
    add = 0;
//	if((p1 = strstr(buff_t, "$GPRMC")) != NULL)
    while(add < lenth)
	{
        if(buff_t[add] == '$' && buff_t[add+1] == 'G' && (buff_t[add+2] == 'N' || buff_t[add+2] == 'P')\
            && buff_t[add+3] == 'R' && buff_t[add+4] == 'M' && buff_t[add+5] == 'C')
		{
            x = 0;
            for(z = 0; x < 12; z++)
			{
                if(buff_t[add+z]=='\0')
				{
                    break;
                }
                if(buff_t[add+z]==',')
				{
                    x++;
                    switch(x)
					{
						case 1:	//第一个参数是时间参数 	hhmmss.ss
							Time = 0;
							for(i = 0; buff_t[add+z+i+1] != '.'; i++)
							{
								if(buff_t[add+z+i+1]=='\0'){
									break;
								}   
								if(buff_t[add+z+i+1] == ',')
									break;
								Time = (buff_t[add+z+i+1]-'0') + Time*10;
							}
							
							GPS.Time_H = Time/10000+8;
							GPS.Time_M = Time/100%100;
							GPS.Time_S = Time%100;
							if(GPS.Time_H >= 24)
								GPS.Time_H = GPS.Time_H - 24;
						break;
						
						case 2:		//定位有效标志
										 //A 表示已经获得有效定位
										 //V 表示目前定位无效
							if(buff_t[add+z+1] == 'A')
							{
								 GPS.Status = 1;
							}
							else
							{
								 GPS.Status = 0;
							}
						break;
							
						case 3:		//纬度信息  ddmm.mmmm
							latitude = 0;
							for(i = 0; buff_t[add+z+i+1] != ','; i++)
							{
								if(buff_t[add+z+i+1] == '\0')
								{
									break;
								} 
								if(buff_t[add+z+i+1] == '.')
								{
									continue;
								}
								latitude = (buff_t[add+z+i+1]-'0') + latitude*10;
							}
							GPS.Lat = latitude/1000000.0;
						break;
							
						case 4:		//N-北纬  S-南纬
							GPS.Lat_area = buff_t[add+z+1];
						break;
						
						case 5:		//经度信息  dddmm.mmmm
							longitude = 0;
							for(i = 0; buff_t[add+z+i+1] != ','; i++)
							{
								if(buff_t[add+z+i+1] == '\0')
								{
									break;
								} 
								if(buff_t[add+z+i+1] == '.')
									continue;
								longitude = (buff_t[add+z+i+1]-'0') + longitude*10;
							}
							GPS.Lon = longitude/1000000.0;
						break;
							
						case 6:		//E-东经  W-西经
							GPS.Lon_area = buff_t[add+z+1];
                    }
                }
            }
            add = 0;
            break;
        }
        if(buff_t[add+5] == '\0')
		{
            add = 0;
			break;
        }
        add++;
        if(add > lenth)
		{
            add = 0;
            break;
        }
    }
    return GPS;
}

void L76XPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = L76X_POWER_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(L76X_POWER_TYPE, &GPIO_InitStructure);	
			
}

unsigned int GPS_Process(unsigned short nMain10ms)
{
	u16 i, s_errflg1 = 0, s_errflg2 = 0;
	u16 nTemp = 0;
	char str[512];
	static u16 s_LastTime = 0;//, s_OverCount = 0, s_ATAckCount = 0;
//	static unsigned int s_RTCSumSecond = 0, s_AliveRTCCount = 0xfffe0000;
	static u8 s_State = 1, s_Step = 1, RunOnce = FALSE, s_ErrCount = 0;
	static u16  s_ReceiveCount = 0;//, s_LocationFailCount = 0;
	char * p1 = NULL;//, *p_str = NULL, buf[10];
	static u8 s_DetCount = 0, s_DetNum;
	static uint16 s_StartCount = 0;//开始检测计数
		
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_State = 1;
		s_LastTime = GetSystem10msCount();
	}
	
	if(s_GetGPSTaskRunFlag == FALSE)
	{
		s_State = 1;
		return 1;
	}
	
	switch(s_State)
	{	
		case 1:   //模块断电
			L76XPortInit();
			GPSPowerOff();
			USART2_Config(9600);		//L76默认波特率
			s_LastTime = GetSystem10msCount();
			s_State++;
			u1_printf("\r\n[L76X]GPS模块断电，延时1s.\r\n");
		break;
		 
		case 2:	//断电后延时1S上电
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 100 )
			{
				u1_printf("\r\n[L76X]模块上电，延时1s.\r\n");
				L76X_PWR_ON();
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
			
		case 3: 
			if( CalculateTime(GetSystem10msCount(),s_LastTime) < 50 )
			{

				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;			
					
					//strncpy(str, (const char *)g_USART2_RX_BUF, g_USART2_RX_CNT);
					
					for(i=0; i<g_USART2_RX_CNT; i++)
					{
						sprintf(&str[i], "%c", g_USART2_RX_BUF[i]);
					}
					str[i] = '\0';
					Debug_Out(str);
					
					Clear_Uart2Buff();
				}
			}
			else
			{
				Debug_Out("\r\n[L76X]上电完成，检测GPS模块.\r\n");		
				s_LastTime = GetSystem10msCount();
				s_State++;
				s_Step = 1;
			}
		break;
		
		case 4: 	//上电完成后，检测GPS模块信息
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{	
				switch(s_Step)
				{
					case 1:		Debug_Out("[L76X]-> 获取模块信息\r\n");	L76X_Send_Command("$PDTINFO");		break;
					case 2:		Debug_Out("[L76X]-> 检测天线状态\r\n");	L76X_Send_Command("$ANTSTAT1");		break;
					case 3:		Debug_Out("[L76X]-> 关闭GLL\r\n");	L76X_Send_Command("$CFGMSG,0,1,0");	break;
					case 4:		Debug_Out("[L76X]-> 开启GSA\r\n");	L76X_Send_Command("$CFGMSG,0,2,2");	break;
					case 5:		Debug_Out("[L76X]-> 开启GSV\r\n");	L76X_Send_Command("$CFGMSG,0,3,2");	break;
					case 6:		Debug_Out("[L76X]-> 关闭VTG\r\n");	L76X_Send_Command("$CFGMSG,0,5,0");	break;
					case 7:		Debug_Out("[L76X]-> 关闭ZDA\r\n");	L76X_Send_Command("$CFGMSG,0,6,0");	break;
					case 8:		Debug_Out("[L76X]-> 关闭GST\r\n");	L76X_Send_Command("$CFGMSG,0,7,0");	break;
				}
				s_State++;
			}	
					
		break;
		
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{

				if(g_Uart2RxFlag == TRUE)
				{
					g_Uart2RxFlag = FALSE;
					
					Debug_OutReceive_ASC((char *)g_USART2_RX_BUF, g_USART2_RX_CNT);		
												
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)g_USART2_RX_BUF,"$PDTINFO")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;						
							}
							else
							{
								s_ErrCount++;
								if(s_ErrCount >= 10)
								{					
									u1_printf("\r\n  GPS模块信息读取失败，关闭模块\r\n");
									SetLogErrCode(LOG_CODE_NOGPS);
									s_ErrCount = 0;
									s_State = 1;
									s_Step = 1;
									GPSPowerOff();
									GPS.Status = 0;
									SetGetGPSDataFlag(TRUE);
									s_GetGPSTaskRunFlag = FALSE;
									SetLogErrCode(LOG_CODE_GPS_SUCCESS);
								}
								else
								{										
									s_State--;
								}
							}
							
						break;
							
						case 2:
							s_errflg1 = 0;
							s_errflg2 = 0;
							if((p1 = (strstr((const char *)g_USART2_RX_BUF,"$ANTSTAT1"))) != NULL )
							{				
								nTemp = atoi( (const char *) (p1+10));	//天线状态
								switch(nTemp)								
								{
									case 0:
										Debug_Out("  初始化");
										s_errflg1 = 1;
									break;
									case 1:
										Debug_Out("  未知");
										s_errflg1 = 1;
									break;
									case 2:
										Debug_Out("  正常");
										s_errflg1 = 0;
									break;
									case 3:
										Debug_Out("  短路");	
										s_errflg1 = 1;
									break;
									case 4:
										Debug_Out("  开路");	
										s_errflg1 = 1;
									break;
									default:
										s_errflg1 = 1;
									break;
								}	
								Debug_Out(", ");	
								nTemp = atoi( (const char *) (p1+12));	//天线供电状态
								switch(nTemp)								
								{
									case 0:
										Debug_Out("断路\r\n");
										s_errflg2 = 1;
									break;
									case 1:
										Debug_Out("正常\r\n");
										s_errflg2 = 0;
									break;
									case 2:
										Debug_Out("未知\r\n");
										s_errflg2 = 1;
									break;
									default:
										s_errflg2 = 1;
									break;
								}	
								if(s_errflg1 || s_errflg2)
								{
									s_ErrCount ++;
									if(s_ErrCount >= 5)
									{					
										s_ErrCount = 0;
										u1_printf("\r\n  GPS天线异常，关闭模块\r\n");
										s_State = 1;
										s_Step = 1;
										GPSPowerOff();
										GPS.Status = 0;
										SetGetGPSDataFlag(TRUE);
										s_GetGPSTaskRunFlag = FALSE;
										SetLogErrCode(LOG_CODE_GPS_SUCCESS);
									}
									else
									{										
										s_State--;
									}
								}	
								else
								{
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;	
								}
							}
							else
							{
								//Debug_Out("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}						
						break;	
							
						case 3:
						case 4:
						case 5:
						case 6:
						case 7:
						case 8:
							if(strstr((const char *)g_USART2_RX_BUF,"OK") != NULL )
							{		
								if(s_Step == 8)		//设置成功， 执行下一条命令
								{
									s_Step = 1;
									s_State ++;
								}
								else
								{
									s_Step++;	
									s_State--;		
								}
								s_ErrCount = 0;	
							}
							else
							{
								s_State--;		
								s_ErrCount ++;	
								if(s_ErrCount >= 5)		//设置失败， 执行下一条命令
								{
									s_ErrCount = 0;
									if(s_Step == 8)
									{
										s_State ++;
										s_Step = 1;	
									}
									else
									{
										s_State --;
										s_Step ++;	
									}
								}
							}
						break;			
					}
					
					if(s_ErrCount >= 20 )
					{
						u1_printf("\r\n  GPS模块响应异常，关闭模块\r\n");
						s_ErrCount = 0;
						s_State = 1;
						s_Step = 1;
						GPSPowerOff();
						GPS.Status = 0;
						SetGetGPSDataFlag(TRUE);
						s_GetGPSTaskRunFlag = FALSE;
						SetLogErrCode(LOG_CODE_GPS_SUCCESS);
						Clear_Uart2Buff();
						return 1;
					}
					Clear_Uart2Buff();
				}
				s_LastTime = GetSystem10msCount();
			}		
		break;

			
		case 6:				//捕获GPS定位信息输出			
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				s_LastTime = GetSystem10msCount();

				if(g_Uart2RxFlag == TRUE)	//收到一帧数据包
				{			
					
					u1_printf(" Satellite Num:%d,Position Num:%d,GPS Status:%d,Altitude:%.1fm,P:%.2f,H:%.2f,V:%.2f,Location:%d,%d\r\n", \
					s_GPS_Msg.Satellite_Num, s_GPS_Msg.Position_Num, s_GPS_Msg.GPS_Status, (float)s_GPS_Msg.Altitude/100.0, (float)s_GPS_Msg.Pdop/100.0, (float)s_GPS_Msg.Hdop/100.0, (float)s_GPS_Msg.Vdop/100.0, s_GPS_Msg.Latitude, s_GPS_Msg.Longitude);						

							
					NMEA_GPGSV_Analysis(&s_GPS_Msg,g_USART2_RX_BUF);	//GPGSV解析
					NMEA_GNGGA_Analysis(&s_GPS_Msg,g_USART2_RX_BUF);	//GNGGA解析 
					NMEA_GNGSA_Analysis(&s_GPS_Msg,g_USART2_RX_BUF);	//GNGSA解析
					NMEA_GNRMC_Analysis(&s_GPS_Msg,g_USART2_RX_BUF);	//GNRMC解析
					
					if(s_GPS_Msg.Longitude >= 7340000 && s_GPS_Msg.Longitude <= 13523000)
					{
						if(s_GPS_Msg.Latitude >= 352000 && s_GPS_Msg.Latitude <= 5333000)
						{
							
							s_StartCount++;
							
							if(s_StartCount > 5 && s_GPS_Msg.Hdop < 250 )	//前10次数据丢弃 精度因子控制在2.5以内
							{
								if(s_DetCount < 3)
								{
									s_LatitudeBuf[s_DetCount] = s_GPS_Msg.Latitude;
									s_LongitudeBuf[s_DetCount] = s_GPS_Msg.Longitude;
																
									s_Latitude = s_GPS_Msg.Latitude/100000.0;
									s_Longitude = s_GPS_Msg.Longitude/100000.0;					
								}
								else
								{
									s_LatitudeBuf[s_DetNum] = s_GPS_Msg.Latitude;
									s_LongitudeBuf[s_DetNum] = s_GPS_Msg.Longitude;

									s_Latitude = Mid_Filter(s_LatitudeBuf, s_DetCount)/100000.0;;
									s_Longitude = Mid_Filter(s_LongitudeBuf, s_DetCount)/100000.0;;
								}
								
								s_DetCount++;
								s_DetNum++;
								
								if(s_DetCount >= GET_GPS_COUNT)
								{
									s_DetCount = GET_GPS_COUNT;
								}
								
								if(s_DetNum >= GET_GPS_COUNT)
								{
									s_DetNum = 0;
									s_StartCount =0;
									s_DetCount = 0;
									
									GPSPowerOff();	
									SetGetGPSDataFlag(FALSE);
									s_GetGPSTaskRunFlag = FALSE;
									u1_printf(" Get Location Data,Location:%.8f,%.8f\r\n", s_Latitude,  s_Longitude);
									
									SetLogErrCode(LOG_CODE_GPS_SUCCESS);
									s_GetGPSDataFlag = TRUE;
								}
							}							
						}
					}
			
					Clear_Uart2Buff();
				}
				
				if(s_ReceiveCount ++ >= 650) 	//65秒后仍未定位成功，超时退出
				{
					s_ReceiveCount = 0;
					s_DetNum = 0;
					s_StartCount =0;
					s_DetCount = 0;
					GPSPowerOff();
					GPS.Status = 0;
					SetGetGPSDataFlag(TRUE);
					s_GetGPSTaskRunFlag = FALSE;
					Debug_Out("[L76X] GPS定位失败\r\n");
					SetLogErrCode(LOG_CODE_GPS_FAIL);
					s_State = 1;
					s_Step = 1;
					return 1;
				}
			}
		
		break;
			
		default:
			s_ReceiveCount = 0;
			s_DetNum = 0;
			s_StartCount =0;
			s_DetCount = 0;
			GPSPowerOff();
			GPS.Status = 0;
			SetGetGPSDataFlag(TRUE);
			s_GetGPSTaskRunFlag = FALSE;
			Debug_Out("[L76X] GPS定位失败\r\n");
			SetLogErrCode(LOG_CODE_GPS_FAIL);
			s_State = 1;
			s_Step = 1;
			return 1;

	}	
	
	return 0;
}

