#include "main.h"
#include "SIM7600CE.h"

extern char APN_TAB[11][20];

void SIM7600CEPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB, ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = SIM7600CE_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM7600CE_ENABLE_TYPE, &GPIO_InitStructure);	

//	GPIO_InitStructure.GPIO_Pin = SIM7600CE_WAKEUP_PIN;	
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
//	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
//	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
//	GPIO_Init(SIM7600CE_WAKEUP_TYPE, &GPIO_InitStructure);	
//	
//	GPIO_SetBits(SIM7600CE_WAKEUP_TYPE, SIM7600CE_WAKEUP_PIN);
	
	GPIO_InitStructure.GPIO_Pin = SIM7600CE_RST_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_RST_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = SIM7600CE_RF_DISABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_RF_DISABLE_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = SIM7600CE_DTR_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_DTR_TYPE, &GPIO_InitStructure);	
	
	GPIO_ResetBits(SIM7600CE_DTR_TYPE, SIM7600CE_DTR_PIN);
			
}

void SIM7600CEProcess(unsigned short nMain10ms)
{
	u16 nTemp = 0;
	char str[512];
	static u16 s_LastTime = 0, s_OverCount = 0, s_ATAckCount = 0;
	static unsigned int s_RTCSumSecond = 0, s_AliveRTCCount = 0xfffe0000;
	static u8 s_APNFlag = 0, s_APNTag = 0, s_State = 1, s_Step = 1, RunOnce = FALSE, s_ErrCount = 0, s_RetryCount = 0, s_Reboot = 0, s_LocationFailCount = 0;
	static u8 s_PowerOnFlag = FALSE;
	u16 MCC = 0;
	u8  MNC = 0;
	char * p1 = NULL, *p_str = NULL, buf[10];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	static u8 s_LastRTCDate = 32;
	static u8 s_StartCalTimeFlag = TRUE;	//开始计算上报时间标志
	static u16 s_StartTime = 0, s_EndTime = 0, s_LinkTime = 0;
	static u8 s_LinkTimeRunFlag = FALSE;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
		
		RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
		s_LastRTCDate = RTC_DateStruct.RTC_Date;		
	}		
		
	if(s_LinkTimeRunFlag)
	{		
		if( CalculateTime(GetSystem100msCount(), s_LinkTime) >= 1800 )	//180s超时
		{
			s_LinkTimeRunFlag = FALSE;
			u1_printf(" 180s连接超时 休眠\r\n");
			s_LinkTime = GetSystem100msCount();
			s_RetryCount = 0;
			s_State = 53;
			s_Reboot++;
			s_Step = 1;
			s_ErrCount = 0;
		}
	}
	
	switch(s_State)
	{	
		case 1:   //模块断电
			s_RetryCount++;
			SetTCModuleReadyFlag(FALSE);
			if(s_RetryCount >= 2)
			{
				s_RTCSumSecond = GetRTCSecond();	
				u1_printf("超时未连上GPRS\r\n");
				s_Reboot++;
				if(s_Reboot >= 6)
				{
					s_Reboot = 0;
					u1_printf(" SIM长时间链接不上,设备重启\r\n");
					delay_ms(100);
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					Sys_Soft_Reset();	
				}
				s_RetryCount = 0;
				s_State = 53;
				break;
			}
			SIM7600CEPortInit();
			SIM7600CE_PWR_OFF();
			SIM7600CE_RST_ON();
			SIM7600CE_RF_DISABLE_ON();
			SetTCProtocolRunFlag(FALSE);
			USART2_Config(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_State++;
			u1_printf("\r\n[SIM7600CE]模块断电，延时1s.\r\n");
			s_LinkTimeRunFlag = TRUE;
			s_LinkTime = GetSystem100msCount();
			s_PowerOnFlag = TRUE;
		break;
		 
		case 2:	//断电后延时3S上电，模块上电自动开机				
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1500 )
			{
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 100 )
			{
				if(s_PowerOnFlag)
				{
					if(s_StartCalTimeFlag)
					{
						s_StartCalTimeFlag = FALSE;
						s_StartTime = GetSystem10msCount();
					}
					s_PowerOnFlag = FALSE;
					u1_printf("\r\n[SIM7600CE]模块上电，延时15s.\r\n");
					SIM7600CE_PWR_ON();	
				}
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 50 )
			{
				Debug_Out("\r\n[SIM7600CE]模块上电完成，检测模块.\r\n");
				u2_printf("AT\r");
				delay_ms(50);
				u2_printf("AT\r");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
		
		case 4:	//延时100MS,检测响应数据
			if(g_Uart2RxFlag == TRUE)
			{
				g_Uart2RxFlag = FALSE;
				
				if(strstr((char *)g_USART2_RX_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_State++;	
				}			
				Clear_Uart2Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 20 )
			{				
					Debug_Out("[SIM7600CE]AT无响应\r\n");
					s_State--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 5 )
					{
						SetLogErrCode(LOG_CODE_NOMODULE);
						s_ErrCount = 0;
						s_State = 1;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				Debug_Out(str);		

				switch(s_Step)
				{
					case 1:	u2_printf("ATE0\r");			Debug_Out("[SIM7600CE]->ATE0\r\n");		s_LastTime = GetSystem10msCount();	break;
					case 2:	u2_printf("AT+CPIN?\r");		Debug_Out("[SIM7600CE]->查询模块SIM状态\r\n");	s_LastTime = GetSystem10msCount();	break;
					case 3:	u2_printf("AT+CIMI\r");			Debug_Out("[SIM7600CE]->查询IMSI\r\n");		s_LastTime = GetSystem10msCount();	break;
					case 4:				//根据CIMI信息设置APN
						u2_printf("AT+CGDCONT=1,\"IP\",\"%s\"\r", (char *)APN_TAB[s_APNTag]);
						sprintf(str, "[SIM7600CE]->建立APN:%s\r\n", (char *)APN_TAB[s_APNTag]);
						Debug_Out(str);			
						s_LastTime = GetSystem10msCount();	
					break;
					case 5:	u2_printf("AT+CSQ\r");		Debug_Out("[SIM7600CE]->查询模块信号质量\r\n");		s_LastTime = GetSystem10msCount();	break;	
					case 6:	u2_printf("AT+COPS?\r");	Debug_Out("[SIM7600CE]->查询网络运营商\r\n");			s_LastTime = GetSystem10msCount();	break;	
					case 7:	u2_printf("AT+CPSI?\r");	Debug_Out("[SIM7600CE]->查询驻网详细信息\r\n");		s_LastTime = GetSystem10msCount();	break;	
					case 8:	u2_printf("AT+CGATT?\r");	Debug_Out("[SIM7600CE]->查询模块附着GPRS状态\r\n");	s_LastTime = GetSystem10msCount();	break;	
				}
				s_State++;
			}	
					
		break;
		
		case 6:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
			{
				if(g_Uart2RxFlag == TRUE)
				{				
					Debug_OutReceive_ASC((char *)g_USART2_RX_BUF, g_USART2_RX_CNT);
												
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();							
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 2:
						//	u1_printf("检测SIM卡： ");
							if((strstr((const char *)g_USART2_RX_BUF,"+CPIN: READY")) != NULL )
							{					
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();		
								u1_printf("\r\n  SIM卡正常\r\n");
							}
							else
							{
								s_State--;	
								s_ErrCount++;
								if(s_ErrCount >= 10)
								{
									SetLogErrCode(LOG_CODE_NO_CARD);
									u1_printf(" No Card!\r\n");
									s_ErrCount = 0;
									s_State = 1;
									s_Step = 1;
								}
							}						
						break;	
							
						case 3:			//CIMI查询
							//中国移动:46000、46002、46004、46007、46008
							//中国联通:46001、46006、46009
							//中国电信:46003、46005、46011。
								//越南MCC:452
								//越南MNC   	运营商				APN:
								// 	  01 	MobiFone			m-wap
								// 	  02 	Vinaphone			m3-world / m-world
								// 	  03 	S-Fone 
								// 	  04 	Viettel Mobile		v-internet
								// 	  05 	Vietnamobile
								// 	  06 	EVNTelecom
								// 	  07 	Beeline VN
								// 	  08 	3G EVNTelecom
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )			//===============此处判断可能容易出错  
							{
								MCC = (g_USART2_RX_BUF[2] - '0')*100 + (g_USART2_RX_BUF[3] - '0')*10 +(g_USART2_RX_BUF[4] - '0');
								MNC = (g_USART2_RX_BUF[5] - '0')*10 + (g_USART2_RX_BUF[6] - '0');
								
									sprintf(str, "MCC=%d, MNC=%d\r\n",MCC,MNC);
									Debug_Out(str);	
								
								s_APNFlag = 0;	
								switch(MCC)
								{
									case	MCC_CHINA:
										Debug_Out("MCC_CHINA\r\n");	
										switch(MNC)
										{
											//移动卡
											case 0:
											case 2:
											case 4:
											case 7:
											case 8:
												s_APNFlag = 1;	
												s_APNTag = CMNET;
												break;
											//联通卡
											case 1:
											case 6:
											case 9:
												s_APNFlag = 1;	
												s_APNTag = UNINET;
												break;	
											//电信卡
											case 3:
											case 5:
											case 11:
												s_APNFlag = 1;	
												s_APNTag = CTNET;
												break;										
											default:
												s_APNFlag = 0;	
												s_APNTag = MCC_UNKOWN;
												break;
										}
										break;
										
									case	MCC_VIETNAM:
										switch(MNC)
										{
											//移动卡
											case 1:
												s_APNFlag = 1;	
												s_APNTag = MOBIFONE;
												break;
											case 2:
												s_APNFlag = 1;	
												s_APNTag = VINAPHONE;
												break;
											case 4:
												s_APNFlag = 1;	
												s_APNTag = VIETTEL;
												break;							
											default:
												s_APNFlag = 0;	
												s_APNTag = MCC_UNKOWN;
												break;
										}
										break;
									default:
										s_APNFlag = 0;	
										s_APNTag = MCC_UNKOWN;
										break;
								}						

								if(s_APNFlag)
								{
									sprintf(str, "IMSI: APN SET TO %s\r\n", (char *)APN_TAB[s_APNTag]);
									Debug_Out(str);	
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;		
								}
								else
								{
									Debug_Out("IMSI: GET IMSI FAIL\r\n");
									s_State--;		
									s_ErrCount ++;	
								}
								s_LastTime = GetSystem10msCount();	
									
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 4:
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
							//	Debug_Out("APN OK\r\n");
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
														
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 5:				//	u1_printf("检测信号质量： ");					
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CSQ:")) != NULL )
							{							
								//u1_printf("%s\r\n", g_USART2_RX_BUF);							
								strncpy(buf,p1+6,2);
								nTemp = atoi( buf);
								SetCSQValue(nTemp);
								if( nTemp == 99 )								
								{
									s_State--;	
									s_ErrCount++;
									Debug_Out(" 无信号\r\n");
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								}
								else if( nTemp < 8 )								
								{
									s_State--;	
									s_ErrCount++;
									Debug_Out(" 信号差\r\n");
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								}
								else
								{
									s_State--;	
									s_Step++;	
									s_ErrCount = 0;
									u1_printf("\r\n  信号正常\r\n");
									ClearLogErrCode(LOG_CODE_WEAKSIGNAL);
									
								}
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}		
						break;		
							
						case 6:
							//u1_printf(" 运营商名称： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+COPS:")) != NULL )
							{		
								if((strstr((const char *)g_USART2_RX_BUF,"CHINA MOBILE")) != NULL )  
								{
									Debug_Out("COPS: CHINA MOBILE\r\n");
									SetLogErrCode(LOG_CODE_MOBILE);
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"UNICOM")) != NULL )  
								{
									Debug_Out("COPS: UNICOM\r\n");
									SetLogErrCode(LOG_CODE_UNICOM);
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"CHINA TELECOM")) != NULL )  	//电信运营商信息不一定正确
								{
									Debug_Out("COPS: CTNET\r\n");
									
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"Vinaphone")) != NULL )  	//越南vinaphone
								{
									Debug_Out("COPS: m3-world\r\n");
									
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if(s_APNFlag)  	//电信运营商信息不一定正确
								{
									Debug_Out("COPS: UNRECOGNIZABLED\r\n");
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else
								{
									Debug_Out("COPS: UNRECOGNIZABLED NETWORK\r\n");
									s_State--;		
									s_ErrCount ++;
								}
									
								s_LastTime = GetSystem10msCount();		
							//	u1_printf("%s\r\n", g_USART2_RX_BUF);
							}
							else
							{
								Debug_Out("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}
						break;	
											
						case 7:
							//u1_printf(" 模块注册网络： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CPSI:")) != NULL )
							{						
								//u1_printf("%s\r\n", g_USART2_RX_BUF);
								if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
								{
									u1_printf("\r\n[SIM7600CE]模块注册网络--成功\r\n");
									s_State--;
									s_Step++;
									s_ErrCount = 0;
								}
								else
								{
									s_State--;
									s_ErrCount++;
									sprintf(str, "\r\n [SIM7600CE]模块注册获取失败, 次数=%d\r\n",s_ErrCount);
									Debug_Out(str);
								}	
								
								if(s_ErrCount >= 20)
								{		
									u1_printf("\r\n  GPRS注册网络失败，重启模块\r\n");
									s_ErrCount = 0;
									s_State = 1;
									s_Step = 1;
								}
								
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}	
								
						break;						
													
						case 8:
							u1_printf("GPRS附着状态： ");
							if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGATT:")) != NULL )
							{					
								//u1_printf("%s\r\n", g_USART2_RX_BUF);
								
								nTemp = atoi( (const char *) (p1+8));
								if( nTemp == 0 )								
								{
									Debug_Out(" 未附着\r\n");
									s_State--;	
									s_ErrCount++;
									if(s_ErrCount >= 50)
									{					
										u1_printf("\r\n  网络附着失败，重启模块\r\n");
										s_ErrCount = 0;
										s_State = 1;
										s_Step = 1;
										SetLogErrCode(LOG_CODE_UNATTACH);
									}
								}
								else
								{
									u1_printf(" 附着成功\r\n");
									if(GetGPSState())		//GPS已定位成功，不需进行基站定位
									{
										Debug_Out("GPS已定位\r\n");
										s_State = 9;
									}
									else if(GetGPSDataFlag())	// && ((s_APN = APN_CMNET) || (s_APN = APN_UNINET)) )	//移动和联通卡可进行定位
									{
										Debug_Out("LBS定位\r\n");
										s_State++;	
									}
									else
									{
										Debug_Out("不定位\r\n");;
										s_State = 9;	
									}
									s_Step = 1;	
									s_ATAckCount = 0;
									s_ErrCount = 0;
								}
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}							
						break;
					}
					
					if(s_ErrCount > 100 )
					{
						s_ErrCount = 0;
						s_State = 1;
						s_Step = 1;
					}
					Clear_Uart2Buff();
				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 300) 
				{
					s_ATAckCount++;
					if(s_ATAckCount >= 5)
					{
						s_ATAckCount = 0;
						s_ErrCount = 0;
						u1_printf(" AT无响应\r\n");
						SetLogErrCode(LOG_CODE_TIMEOUT);
						s_State = 1;
						s_Step = 1;
					}
					else
					{
						s_State--;
						Debug_Out(" AT指令超时\r\n");
					}
					
					s_LastTime = GetSystem10msCount();
				}
			}
				
		
		break;

		case 7:			//电信LBS定位指令不同
			if((s_APNTag == CMNET) || (s_APNTag == UNINET))		//移动联通卡LBS定位
			{
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
				{
					switch(s_Step)
					{
						case 1:	u2_printf("AT+CNETSTART\r");	Debug_Out("[SIM7600CE]->开启LBS定位业务\r\n");	break;
						case 2:	u2_printf("AT+CNETIPADDR?\r");	Debug_Out("[SIM7600CE]->获取定位服务器IP\r\n");	break;
						case 3:	u2_printf("AT+CLBS=1\r");		Debug_Out("[SIM7600CE]->获取基站定位信息\r\n");	break;
						case 4:	u2_printf("AT+CNETSTOP\r");		Debug_Out("[SIM7600CE]->关闭LBS\r\n");			break;
						default:
							s_Step = 1;	
							s_State = 8;			//跳过LBS定位
							s_ATAckCount = 0;
							s_ErrCount = 0;
						break;
					}
					s_State++;
					s_LastTime = GetSystem10msCount();	
				}
			}
			else 		//电信卡定位：	
			{
				s_State = 9;	//跳过LBS定位
				s_LastTime = GetSystem10msCount();	
			}
		break;
			
		case 8:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10)
			{
				if(g_Uart2RxFlag == TRUE)
				{				
					Debug_OutReceive_ASC((char *)g_USART2_RX_BUF, g_USART2_RX_CNT);	
												
					switch(s_Step)
					{	
						case 1:						
							if((strstr((const char *)g_USART2_RX_BUF,"+CNETSTART: 0")) != NULL )
							{									
								s_Step++;			
								s_State--;	
								s_ATAckCount = 0;
								s_ErrCount = 0;
							}				
							else if((strstr((const char *)g_USART2_RX_BUF,"+CNETSTART: 1")) != NULL )
							{					
								Debug_Out("[SIM7600CE]正在启动。。\r\n");						
								s_State--;	
								s_ErrCount ++;
							}	
							else	
							{
								Debug_Out("[SIM7600CE]收到未识别的指令\r\n");	
								s_State--;	
								s_ErrCount ++;
							}
						break;					
							
						case 2:
							if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
							{					
								s_Step ++;	
								s_State--;		
								s_ATAckCount = 0;
								s_ErrCount = 0;
							}
							else
							{
								Debug_Out("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}						
						break;	
							
						case 3:
							if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ATAckCount = 0;
								s_ErrCount = 0;
								memcpy(str, g_USART2_RX_BUF, g_USART2_RX_CNT);
								p_str = strtok(str, ",");
								p_str = strtok(NULL, ",");  
								SetLatitude(atof(p_str));
								p_str = strtok(NULL, ",");  
								SetLongitude(atof(p_str));
								s_LocationFailCount = 0;
								sprintf(str, "\r\n 经度:%f  纬度:%f \r\n", GetLongitude(), GetLatitude());
								SetGetGPSDataFlag(TRUE);
								Debug_Out(str);
							}
							else
							{
								s_State--;	
								s_ErrCount++;								
							}
						break;
							
						case 4:
							if((strstr((const char *)g_USART2_RX_BUF,"+CNETSTOP: 0")) != NULL )
							{					
								s_Step = 1;	
								s_State++;		
								s_ATAckCount = 0;
								s_ErrCount = 0;
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}						
						break;
							
						default:

						break;
							
					}		
					if(s_ErrCount > 5 )
					{
						Debug_Out("[SIM7600CE] LBS定位失败, 关闭LBS\r\n");
						u2_printf("AT+CNETSTOP\r");
						SetLongitude(0);
						SetLatitude(0);
						s_LocationFailCount++;
						if(s_LocationFailCount >= 3)
						{
							s_LocationFailCount = 0;
							SetGetGPSDataFlag(FALSE);
						}
						s_ErrCount = 0;
						s_ATAckCount = 0;
						s_State = 9;
						s_Step = 1;
					}
					
					Clear_Uart2Buff();
				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 300) 
				{		
					s_ATAckCount++;			
					if(s_ATAckCount >= 5 )
					{
						Debug_Out("[SIM7600CE] LBS定位超时, 关闭LBS\r\n");
						u2_printf("AT+CNETSTOP\r");
						SetLongitude(0);
						SetLatitude(0);
						s_LocationFailCount++;
						if(s_LocationFailCount >= 3)
						{
							s_LocationFailCount = 0;
							SetGetGPSDataFlag(FALSE);
						}
						s_ErrCount = 0;
						s_ATAckCount = 0;
						s_State ++;
						s_Step = 1;
					}
					else
					{				
						Debug_Out("[SIM7600CE] 响应超时\r\n");		
						s_State--;	
					}
										
				}				
				s_LastTime = GetSystem10msCount();		
			}
		break;		
			
		case 9:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				switch(s_Step)
				{
						case 1:	u2_printf("AT+CIPMODE=1\r");	Debug_Out("[SIM7600CE]->设置为透传模式\r\n");		break;
						case 2:	u2_printf("AT+NETOPEN\r");		Debug_Out("[SIM7600CE]->激活PDP\r\n");			break;
						case 3:	u2_printf("AT+IPADDR\r");		Debug_Out("[SIM7600CE]->获得本地IP地址\r\n");		break;		
					
						default:
							s_State = 0;
							s_Step = 1;
						break;
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		
		break;
	
		case 10:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				if(g_Uart2RxFlag == TRUE)
				{				
					Debug_OutReceive_ASC((char *)g_USART2_RX_BUF, g_USART2_RX_CNT);	
												
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
							{
							//	Debug_Out("设置为透传模式\r\n");
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
													
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 2:
							if((strstr((const char *)g_USART2_RX_BUF,"+NETOPEN: 0")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
														
							}
							else if((strstr((const char *)g_USART2_RX_BUF,"already opened")) != NULL )
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
								
							}
							else
							{
								s_State--;	
								s_ErrCount++;
							}
						break;
							
						case 3:
							//Debug_Out("模块本地IP：");
							if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
							{					
								s_Step = 1;	
								s_State++;		
								s_ATAckCount = 0;
								s_ErrCount = 0;
							
								//sprintf(str, "%s\r\n", g_USART2_RX_BUF));
								//Debug_Out(str);
							}
							else
							{
								Debug_Out("    Error\r\n");
								s_State--;	
								s_ErrCount++;
							}						
						break;
						
						default:
							
						break;
							
					}
					
					if(s_ErrCount > 50 )
					{
						s_ErrCount = 0;
						s_State = 1;
						s_Step = 1;
					}
					s_LastTime = GetSystem10msCount();	
					
					Clear_Uart2Buff();
				}
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 800) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				Debug_Out(str);		
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 5)
				{
					s_ATAckCount = 0;
					u1_printf(" AT无响应\r\n");
					SetLogErrCode(LOG_CODE_TIMEOUT);
					s_State = 1;
					s_Step = 1;
				}
				else
				{
					s_State--;
					Debug_Out(" AT指令超时\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}	
		break;		
		
		case 11:
			Debug_Out("[SIM7600CE]建立TCP链接\r\n");
			sprintf(str,"AT+CIPOPEN=0,\"TCP\",\"%d.%d.%d.%d\",%d\r"
			,p_sys->Gprs_ServerIP[0]
			,p_sys->Gprs_ServerIP[1]
			,p_sys->Gprs_ServerIP[2]
			,p_sys->Gprs_ServerIP[3]
			,p_sys->Gprs_Port	);

			u2_printf(str);	
			Debug_Out(str);	//串口输出AT命令
			Debug_Out("\n");
			s_LastTime = GetSystem10msCount();
			s_State++;		
		//	s_ErrCount = 0;		
		break;
		
		case 12:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				if(g_Uart2RxFlag == TRUE)
				{			
					Debug_OutReceive_ASC((char *)g_USART2_RX_BUF, g_USART2_RX_CNT);	
													
					if((p1 = (strstr((const char *)g_USART2_RX_BUF,"CONNECT FAIL"))) != NULL )
					{					
						s_ErrCount++;
						s_State = 9;
						s_Step = 1;	
						Debug_Out("GPRS链接失败\r\n");	
					}							
					else if((p1 = (strstr((const char *)g_USART2_RX_BUF,"CONNECT 115200"))) != NULL )
					{	
						s_Step = 1;	
						s_State++;		
						s_ErrCount = 0;
						SetTCModuleReadyFlag(TRUE);		//连接成功========================
						u1_printf("\r\n  GPRS链接已建立\r\n");							
					}								
					else if((p1 = (strstr((const char *)g_USART2_RX_BUF,"+CIPOPEN:"))) != NULL )
					{
						nTemp = atoi( (const char *) (p1+12));
						if(nTemp == 0)
						{						
							s_Step = 1;	
							s_State++;		
							s_ErrCount = 0;
							SetTCModuleReadyFlag(TRUE);			//连接成功========================
							u1_printf("\r\n  GPRS链接已建立\r\n");				
						}
						else
						{
							s_ErrCount++;
							s_State--;
							Debug_Out("GPRS链接失败\r\n");
						}						
					}
					else
					{
						s_State--;	
						s_ErrCount++;
						Debug_Out("\r\n  未识别指令\r\n");
					}
						
					
					if(s_ErrCount > 5 )
					{
						s_ErrCount = 0;
						s_State = 1;
						s_Step = 1;
					}
					s_LastTime = GetSystem10msCount();	
					
					Clear_Uart2Buff();

				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1000) 
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
		
					s_ATAckCount++;
					
					if(s_ATAckCount >= 5)
					{
						s_ATAckCount = 0;
						u1_printf(" AT无响应\r\n");
						SetLogErrCode(LOG_CODE_TIMEOUT);
						s_State = 1;
						s_Step = 1;
						s_LastTime = GetSystem10msCount();
						s_ErrCount++;
					}
					else
					{
						s_State--;
						Debug_Out(" AT指令超时， 重连\r\n");
					}
				}	
			}				
		break;
		
		case 13:
			s_OverCount = 0;
			s_State = 50;
			SetDataPackFlag(FALSE);
			SetServerConfigCmdFlag(FALSE);
			s_LastTime = GetSystem10msCount();
			SetTCProtocolRunFlag(TRUE);
		break;
		
		case 50:
			if(GetTCModuleReadyFlag())		//SIM就绪
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	
				{
					s_AliveRTCCount = GetRTCSecond();
					Mod_Send_Alive(SIM_COM);
					delay_ms(100);
					break;
				}
				s_EndTime = GetSystem10msCount();
				SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
				u1_printf("\r\n 连接耗时:%.2fs\r\n\r\n", CalculateTime(s_EndTime, s_StartTime)/100.0);
				Mod_Report_LowPower_Data(SIM_COM);	//发送数据包
				s_State++;
				s_LastTime = GetSystem10msCount();

			}
			else
			{
				s_State = 1;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 51:
			if(GetDataPackFlag())//非长连接设备取消心跳包  2019.2.12
			{
				s_State++;
				s_LastTime = GetSystem10msCount();
				SetDataPackFlag(FALSE);
				
				s_OverCount = 0;
				SetLogErrCode(LOG_CODE_SUCCESS);
				WriteCommunicationSuccessCount();
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1000 )
			{
				s_State--;
				s_LastTime = GetSystem10msCount();
				u1_printf("\r\n  Timeout did not receive a reply packet reissued\r\n");
				Clear_Uart2Buff();
				s_OverCount++;
				if(s_OverCount >= 2)
				{
					SetLogErrCode(LOG_CODE_NOSERVERACK);
					s_OverCount = 0;
					s_State = 1;
					s_Step = 1;
					s_ErrCount = 0;
				}
			}		
		break;
					
		case 52:
			if(GetServerConfigCmdFlag() == FALSE)
			{
				if(CalculateTime(GetSystem10msCount(),s_LastTime) >= 100)		//延时1s等待GPRS下行数据
				{
					u1_printf("\r\n Wait for server downlink data timeout,Sleep\r\n");
					u2_printf("AT+NETCLOSE\r");		//	执行PDP去激活，TCP链接也一起关闭			
					s_State++;					
				}
			}
			else 		//收到关闭Socket指令，可以休眠
			{
				u1_printf("\r\n Receive reply packet,Sleep\r\n");
				SetServerConfigCmdFlag(FALSE);
				s_State++;						
			}
		break;
			
		case 53:
			TXB0108_GPIO_Reset();
			SIM7600CE_PWR_OFF();
			s_LinkTimeRunFlag = FALSE;
			s_State = 55;
			s_Step = 1;
			s_RTCSumSecond = GetRTCSecond();			
			s_LastTime = GetSystem10msCount();	
			StoreOperationalData();	
			WriteCommunicationCount();
		break;
			
		case 55:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();	
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
				SetLogErrCode(LOG_CODE_CLEAR);	
				SetTCProtocolRunFlag(FALSE);	
				u1_printf("\r\n [%02d:%02d:%02d][SIM7600CE]重新上电\r\n", RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				s_RetryCount = 0;
				s_StartCalTimeFlag = TRUE;
//				StartSensorMeasure();			//清传感器标志，开启传感器检测
				s_State++;
			}		
			else			//未到上报时间，继续休眠
			{
				if((Sht31Status != SHT_WORKING) && (MaxStatus != MAX_WORKING) && (BmpStatus != BMP_WORKING) &&(BatStatus != BAT_WORKING))
				{
					s_LastTime = GetSystem10msCount();
					
					if(GetTCModuleReadyFlag())
					{
						SetStopModeTime(TRUE, STOP_TIME);
					}
					else
					{
						SetStopModeTime(TRUE, ERR_STOP_TIME);
					}
				}					
			}
		break;
			
		case  56:

			s_State = 1;
			s_Step = 1;
			s_ErrCount = 0;
			s_LastTime = GetSystem10msCount();
			SetTCModuleReadyFlag(FALSE);

			RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
			
			if(s_LastRTCDate != RTC_DateStruct.RTC_Date)	//日期更新，获取经纬度
			{
				s_LastRTCDate = RTC_DateStruct.RTC_Date;
				SetGPSTaskRunFlag(TRUE);
				SetGetGPSDataFlag(FALSE);
			}

		break;	

		default:
			u1_printf(" 意外错误\r\n");
			s_State = 1;
			s_Step = 1;
		break;			
	}
	
}
