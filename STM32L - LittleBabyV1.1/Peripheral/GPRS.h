#ifndef  _GPRS_H_
#define	 _GPRS_H_


void GPRS_Process(unsigned short nMain10ms);

//void OnGPRSComm(unsigned char CommData);

//unsigned char GetGPRSReadyFlag( void );

#define		GPRS_UART_TYPE		GPIOA
#define		GPRS_TX_PIN			GPIO_Pin_2
#define		GPRS_RX_PIN			GPIO_Pin_3

#if( HARDWARE_VERSION == HARDWARE_VERSION_V1_0 )
#define		GPRS_PWR_PIN		GPIO_Pin_5
#define		GPRS_PWR_TYPE		GPIOA

#elif( HARDWARE_VERSION == HARDWARE_VERSION_V1_2 )
#define		GPRS_PWR_PIN		GPIO_Pin_12
#define		GPRS_PWR_TYPE		GPIOA
#endif



#define		GPRS_PWR_ON()		GPIO_SetBits(GPRS_PWR_TYPE, GPRS_PWR_PIN);
#define		GPRS_PWR_OFF()		GPIO_ResetBits(GPRS_PWR_TYPE, GPRS_PWR_PIN);



#endif
