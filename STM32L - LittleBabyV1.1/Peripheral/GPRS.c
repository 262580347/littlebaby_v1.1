#include "main.h"
#include "GPRS.h"

//#define COM_DATA_SIZE	256

//static void GPRSObser(unsigned short nMain10ms);

//static u8 m_CommBuff[COM_DATA_SIZE];

//static u16 m_CirWPinter = 0;

//static u16 m_CirRPinter = 0;

//static u8 s_HeartPackFlag = FALSE;

//static u8 s_DataPackFlag = FALSE;

//static u8 s_GPRSReadFlag = FALSE;

//unsigned char GetGPRSReadyFlag( void )
//{
//	return s_GPRSReadFlag;
//}

//void SetGPRSReadyFlag( unsigned char ucFlag )
//{
//	s_GPRSReadFlag = ucFlag;
//}

//static void Clear_COMBuff(void)
//{
//	u16 i;
//	
//	for(i=0; i<COM_DATA_SIZE; i++)
//	{
//		m_CommBuff[i] = 0;		
//	}
//		m_CirWPinter = 0;

//		m_CirRPinter = 0;	
//}

//void OnGPRSComm(u8 CommData)
//{
//	m_CommBuff[m_CirWPinter++] = CommData;
//	
//	if(m_CirWPinter >= COM_DATA_SIZE)
//	{
//		m_CirWPinter = 0;
//	}
//	if(m_CirWPinter == m_CirRPinter)
//	{
//		m_CirRPinter++;
//		if(m_CirRPinter	>= COM_DATA_SIZE)
//		{
//		    m_CirRPinter = 0; 
//		}
//	}	
//}

void GPRS_Port_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = GPRS_PWR_PIN;   //GPRS  PWR
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(LORA_PWR_TYPE, &GPIO_InitStructure);	
}

void GPRS_Process(unsigned short nMain10ms)
{
	//char str[256];
	static u8 RunOnce = FALSE,  s_State = 1;
	static u16 s_LastTime = 0, s_OverCount = 0, s_RetryCount = 0, s_Reboot = 0;
//	unsigned char SleepTime = 0;
	static unsigned int s_RTCSumSecond = 0, s_AliveRTCCount = 0xfffe0000;

	SYSTEMCONFIG *p_sys;
	
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
		
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	}	
	

//	GPRSObser(GetSystem10msCount());
	
		
	
	switch(s_State)
	{
		case 1:   //模块断电
			s_RetryCount++;
			SetTCModuleReadyFlag(FALSE);
			if(s_RetryCount >= 2)
			{
				s_RTCSumSecond = GetRTCSecond();	
				u1_printf("超时未连上GPRS\r\n");
				s_Reboot++;
				SetLogErrCode(LOG_CODE_RETRY);
				if(s_Reboot >= 6)
				{
					s_Reboot = 0;
					u1_printf(" GPRS长时间链接不上,设备重启\r\n");
					delay_ms(100);
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					SetLogErrCode(LOG_CODE_RESET);
					StoreOperationalData();
					Sys_Soft_Reset();	
				}
				s_RetryCount = 0;
				s_State = 52;
				break;
			}
			GPRS_Port_Init();
			GPRS_PWR_OFF();
			SetTCProtocolRunFlag(FALSE);
			USART2_Config(GPRS_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_State++;
			u1_printf("\r\n[GPRS]模块断电，延时1s.\r\n");
			delay_ms(1000);
			u1_printf("\r\n[GPRS]模块上电，延时10s.\r\n");
			GPRS_PWR_ON();		
		break;
		 
		case 2:	//上电后延时10S，模块自动建立TCP连接并进入透传模式				
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1000 )
			{
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
				Debug_Out("\r\n[GPRS]模块上电完成.\r\n");
				s_OverCount = 0;
				s_State = 50;
				SetTCModuleReadyFlag(TRUE);	
				SetDataPackFlag(FALSE);
				SetServerConfigCmdFlag(FALSE);
				s_LastTime = GetSystem10msCount();
				SetTCProtocolRunFlag(TRUE);
		break;		
		
		case 50:
			if(GetTCModuleReadyFlag())		//模块就绪
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	
				{
					s_AliveRTCCount = GetRTCSecond();
					Mod_Send_Alive(GPRS_COM);
					delay_ms(100);
					break;
				}
				
				if((Sht31Status != SHT_WORKING) && (MaxStatus != MAX_WORKING) && (BmpStatus != BMP_WORKING) &&(BatStatus != BAT_WORKING))
				{
					Mod_Report_LowPower_Data(GPRS_COM);	//发送数据包
					s_State++;
					s_LastTime = GetSystem10msCount();
				}
			}
			else
			{
				s_State = 1;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 51:
			if(GetDataPackFlag())//非长连接设备取消心跳包  2019.2.12
			{
				s_State++;
				s_LastTime = GetSystem10msCount();
				SetDataPackFlag(FALSE);
				s_OverCount = 0;
				SetLogErrCode(LOG_CODE_SUCCESS);
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1000 )
			{
				s_State--;
				s_LastTime = GetSystem10msCount();
				u1_printf("\r\n  超时未收到回复包  重发\r\n");
				Clear_Uart2Buff();
				s_OverCount++;
				if(s_OverCount >= 2)
				{
					SetLogErrCode(LOG_CODE_NOSERVERACK);
					s_OverCount = 0;
					s_State = 1;
				}
			}		
		break;
					
		case 52:
			if(GetServerConfigCmdFlag() == FALSE)
			{
				if(CalculateTime(GetSystem10msCount(),s_LastTime) >= 100)		//延时1s等待GPRS下行数据
				{
					u1_printf("\r\n  等待服务器下行数据超时, 等待下次连接\r\n");
					s_State++;
					s_LastTime = GetSystem10msCount();	
					s_RTCSumSecond = GetRTCSecond();	
					StoreOperationalData();
				}
			}
			else 		//收到关闭Socket指令，可以休眠
			{
				u1_printf("\r\n  收到应答包, 等待下次连接\r\n");
				s_State++;
				s_LastTime = GetSystem10msCount();	
				s_RTCSumSecond = GetRTCSecond();
				StoreOperationalData();
			}
		break;
			
		case 53:		
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();	
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
				SetLogErrCode(LOG_CODE_CLEAR);	
				SetServerConfigCmdFlag(FALSE);
//				SetTCProtocolRunFlag(FALSE);	
//				SetTCModuleReadyFlag(FALSE);
//				u1_printf("\r\n [%02d:%02d:%02d][GPRS]重新上电\r\n", RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				u1_printf("\r\n [%02d:%02d:%02d][GPRS]数据上报\r\n", RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				s_RetryCount = 0;
//				StartSensorMeasure();			//清传感器标志，开启传感器检测
//				s_State++;
				s_State = 50;
			}		
//			else			//未到上报时间，继续休眠
//			{
//				if((Sht31Status != SHT_WORKING) && (MaxStatus != MAX_WORKING) && (BmpStatus != BMP_WORKING) &&(BatStatus != BAT_WORKING))
//				{
//					s_LastTime = GetSystem10msCount();
//					
//					if(GetTCModuleReadyFlag())
//					{
//						SetStopModeTime(TRUE, STOP_TIME);
//					}
//					else
//					{
//						SetStopModeTime(TRUE, ERR_STOP_TIME);
//					}
//				}					
//			}
		break;
			
		case  54:
			if((Sht31Status != SHT_WORKING) && (MaxStatus != MAX_WORKING) && (BmpStatus != BMP_WORKING) &&(BatStatus != BAT_WORKING))	//GPRS重新上电初始化
			{
				s_State = 1;
				s_LastTime = GetSystem10msCount();
			}
		break;		
	}
}

//void OnRecGPRSData(CLOUD_HDR *pMsg, u8 *data, u8 lenth)
//{
//	CLOUD_HDR *hdr;
//	u8 i, sendbuf[100], sendlenth;
//	RTC_TimeTypeDef RTC_TimeStructure;	
//	RTC_DateTypeDef RTC_DateStruct;
//	
//	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
//	//解析协议包
//	u1_printf("\r\n [%02d:%02d:%02d][GPRS] <- 协议号：%02d 设备号：%04d 方向：%X 包序号：%04X 包长度：%X 命令字：%02X 数据:", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->protocol, pMsg->device_id, pMsg->dir, pMsg->seq_no, pMsg->payload_len, pMsg->cmd);
//	for(i=0; i<lenth; i++)
//		u1_printf("%02X ", data[i]);
//	u1_printf("\r\n");

//	if(pMsg->cmd == CMD_HEATBEAT)
//	{
//		u1_printf("收到服务器心跳包回复,同步服务器时间\r\n");
//			
//		RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
//				
//		RTC_TimeStructure.RTC_Hours   = data[2];
//		RTC_TimeStructure.RTC_Minutes = data[1];
//		RTC_TimeStructure.RTC_Seconds = data[0];

//		RTC_DateStruct.RTC_Date  	= data[3];
//		RTC_DateStruct.RTC_Month  	= data[4];
//		RTC_DateStruct.RTC_Year  	= data[5];	
//		RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
//		
//		if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure)  != ERROR)
//		{
//			u1_printf("\r\n 同步远程时间成功\r\n");
//			RTC_TimeShow();
//		}
//		else
//		{
//			u1_printf("\r\n 同步远程时间失败\r\n");
//		}
//		
//		s_HeartPackFlag = TRUE;
//		
//		SetGPRSReadyFlag(TRUE);
//	}
//	
//	if(pMsg->cmd == CMD_REPORT_D)
//	{
//		u1_printf("收到服务器上报数据回复,上报数据成功\r\n");
//		
//		s_DataPackFlag = TRUE;
//	}
//	
//	if(pMsg->cmd == CMD_GET_CONFIG)//获取设备配置
//	{
//		System_get_config(GPRS_COM, pMsg, data, lenth);
//	}
//	
//	if(pMsg->cmd == CMD_SET_CONFIG) //设置设备配置
//	{
//		memcpy(sendbuf, pMsg, sizeof(CLOUD_HDR));
//		sendlenth = sizeof(CLOUD_HDR);
//		
//		hdr = (CLOUD_HDR *)sendbuf;
//		hdr->protocol = swap_word(hdr->protocol);	
//		hdr->device_id = swap_dword(hdr->device_id);
//		hdr->seq_no = swap_word(hdr->seq_no);	
//		hdr->dir = 1;
//		hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
//		memcpy(&sendbuf[sizeof(CLOUD_HDR)], data, lenth);
//		System_set_config(GPRS_COM, sendbuf, lenth + sendlenth);
//		u1_printf("更新设备参数，等待设备重启\r\n");
//		while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);	
//		Sys_Soft_Reset();
//	}
//}
//	
//static void GPRSObser(unsigned short nMain10ms)
//{
//	static u8 FrameID = 0, special_flag = FALSE, s_HeadFlag = FALSE;
//	static u16 data_lenth = 0, s_LastTime = 0, i = 0;
//	static CLOUD_HDR m_RxFrame;
//	static u8 RxData, databuff[100];
//	u8 checksum;
//	
//	if(s_HeadFlag == TRUE)
//	{
//		if(CalculateTime(nMain10ms, s_LastTime) >= 100)
//		{
//			s_HeadFlag = FALSE;
//			u1_printf("\r\n 接收超时\r\n");
//			Clear_Uart2Buff();
//			memset(databuff, 0, 100);
//			data_lenth = 0;
//			i = 0;
//			FrameID = 0;
//			return ;
//		}
//	}
//		
//	if(m_CirRPinter != m_CirWPinter)
//	{
//		RxData = m_CommBuff[m_CirRPinter++];
//		if(m_CirRPinter >= COM_DATA_SIZE)
//		{
//			m_CirRPinter = 0;
//		}
//		if(RxData == 0x7d && special_flag == FALSE)
//		{
//			special_flag = TRUE;
//			return;
//		}
//		if(special_flag == TRUE)
//		{
//			special_flag = FALSE;
//			if(RxData == 0x5d)
//			{
//				RxData = 0x7d;
//			}
//			else if(RxData == 0x5e)
//			{
//				RxData = 0x7e;
//			}
//			else if(RxData == 0x51)
//			{
//				RxData = 0x21;
//			}
//		}



//		switch(FrameID)
//		{
//			case 0:
//				if(RxData == 0X7e)
//				{
//					s_HeadFlag = TRUE;
//					s_LastTime = nMain10ms;
//					FrameID++;
//				}
//				break;
//			case 1:
//				m_RxFrame.protocol = RxData;
//				FrameID++;
//			break;
//			case 2:
//				m_RxFrame.protocol = (m_RxFrame.protocol << 8) | RxData;
//				FrameID++;
//			break;
//			case 3:
//				m_RxFrame.device_id = RxData;
//				FrameID++;
//			break;
//			case 4:
//				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
//				FrameID++;
//			break;
//			case 5:
//				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
//				FrameID++;
//			break;
//			case 6:
//				m_RxFrame.device_id = (m_RxFrame.device_id << 8) | RxData;
//				FrameID++;
//			
//			break;
//			case 7:
//				m_RxFrame.dir = RxData;
//				FrameID++;
//			break;
//			case 8:
//				m_RxFrame.seq_no = RxData;
//				FrameID++;
//			break;
//			case 9:
//				m_RxFrame.seq_no = (m_RxFrame.seq_no << 8) | RxData;
//				FrameID++;
//			break;
//			case 10:
//				m_RxFrame.payload_len = RxData;
//				FrameID++;
//			break;
//			case 11:
//				m_RxFrame.payload_len = (m_RxFrame.payload_len << 8) | RxData;
//				data_lenth = m_RxFrame.payload_len;
//				FrameID++;
//			break;
//			case 12:
//				m_RxFrame.cmd = RxData;				
//				i = 0;
//			    memset(databuff, 0, 100);
//				FrameID++;
//				if(data_lenth == 0)
//				{
//					FrameID = 14;
//					break;
//				}	
//				else if(data_lenth >= 100)
//				{
//					u1_printf("\r\n too long %d\r\n", data_lenth );
//					Clear_Uart2Buff();
//					memset(databuff, 0, 100);
//					data_lenth = 0;
//					i = 0;
//					FrameID = 0;
//				}
//			break;
//			case 13:				
//				databuff[i++] = RxData;
//				if(i >= data_lenth)
//				{
//					FrameID++;
//				}
//			break;		
//			case 14:
//				checksum = RxData;
//				if(CheckHdrSum(&m_RxFrame, databuff, data_lenth) != checksum)
//				{
//					u1_printf("[GPRS]Check error...");
//					u1_printf("Rec:%02X, CheckSum:%02X\r\n", checksum, CheckHdrSum(&m_RxFrame, databuff, data_lenth));
//					FrameID = 0;	
//					s_HeadFlag = FALSE;					
//					Clear_Uart3Buff();	
//					Clear_COMBuff();
//					s_HeadFlag = FALSE;		
//					memset(databuff, 0, 100);
//					data_lenth = 0;
//					i = 0;
//				}else
//				FrameID++;
//			break;
//			case 15:
//				if(RxData == 0x21)	
//				{								
//					OnRecGPRSData(&m_RxFrame, databuff, data_lenth);
//				}
//				else
//				{				
//					u1_printf("end error!\r\n");
//				}
//				s_HeadFlag = FALSE;		
//				Clear_Uart2Buff();
//				memset(databuff, 0, 100);
//				data_lenth = 0;
//				i = 0;
//			    FrameID = 0;				
//			break;
//			default:
//				s_HeadFlag = FALSE;		
//				Clear_Uart2Buff();
//				memset(databuff, 0, 100);
//				data_lenth = 0;
//				i = 0;
//			    FrameID = 0;	 
//			break;	
//		}
//	}	
//}

