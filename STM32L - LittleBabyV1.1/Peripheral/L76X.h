#ifndef  _L76X_H_
#define	 _L76X_H_

#include <math.h>
#include <stdlib.h>

#define		L76X_POWER_TYPE		GPIOA		//电源开关	0：电源关   1：电源开
#define		L76X_POWER_PIN		GPIO_Pin_8

#define		L76X_PWR_ON()			GPIO_SetBits(L76X_POWER_TYPE, L76X_POWER_PIN);
#define		GPSPowerOff()			GPIO_ResetBits(L76X_POWER_TYPE, L76X_POWER_PIN);

#define		GPS_BAND_RATE		115200

#define BUFFSIZE 800

//Startup mode
#define HOT_START       "$PMTK101"
#define WARM_START      "$PMTK102"
#define COLD_START      "$PMTK103"
#define FULL_COLD_START "$PMTK104"

//Standby mode -- Exit requires high level trigger
#define SET_PERPETUAL_STANDBY_MODE      "$PMTK161"

#define SET_PERIODIC_MODE               "$PMTK225"
#define SET_NORMAL_MODE                 "$PMTK225,0"
#define SET_PERIODIC_BACKUP_MODE        "$PMTK225,1,1000,2000"
#define SET_PERIODIC_STANDBY_MODE       "$PMTK225,2,1000,2000"
#define SET_PERPETUAL_BACKUP_MODE       "$PMTK225,4"
#define SET_ALWAYSLOCATE_STANDBY_MODE   "$PMTK225,8"
#define SET_ALWAYSLOCATE_BACKUP_MODE    "$PMTK225,9"

//Set the message interval,100ms~10000ms
#define SET_POS_FIX         "$PMTK220"
#define SET_POS_FIX_100MS   "$PMTK220,100"
#define SET_POS_FIX_200MS   "$PMTK220,200"
#define SET_POS_FIX_400MS   "$PMTK220,400"
#define SET_POS_FIX_800MS   "$PMTK220,800"
#define SET_POS_FIX_1S      "$PMTK220,1000"
#define SET_POS_FIX_2S      "$PMTK220,2000"
#define SET_POS_FIX_4S      "$PMTK220,4000"
#define SET_POS_FIX_8S      "$PMTK220,8000"
#define SET_POS_FIX_10S     "$PMTK220,10000"

//Switching time output
#define SET_SYNC_PPS_NMEA_OFF   "$PMTK255,0"
#define SET_SYNC_PPS_NMEA_ON    "$PMTK255,1"

//Baud rate
#define SET_NMEA_BAUDRATE           "$PMTK251"
#define SET_NMEA_BAUDRATE_115200    "$CFGPRT,1,0,115200,3,3"
//#define SET_NMEA_BAUDRATE_115200    "$PMTK251,115200"
#define SET_NMEA_BAUDRATE_57600     "$PMTK251,57600"
#define SET_NMEA_BAUDRATE_38400     "$PMTK251,38400"
#define SET_NMEA_BAUDRATE_19200     "$PMTK251,19200"
#define SET_NMEA_BAUDRATE_14400     "$PMTK251,14400"
#define SET_NMEA_BAUDRATE_9600      "$PMTK251,9600"
#define SET_NMEA_BAUDRATE_4800      "$PMTK251,4800"

//To restore the system default setting
#define SET_REDUCTION               "$PMTK314,-1"

//Set NMEA sentence output frequencies 
#define SET_NMEA_OUTPUT "$PMTK314,1,1,1,1,,1,1,1,0,0,0,0,0,0,0,0,0,1,0"

typedef struct {
	double Lon;     //GPS Latitude and longitude
	double Lat;
    u8 Lon_area;
    u8 Lat_area;
    u8 Time_H;   //Time
    u8 Time_M;
    u8 Time_S;
    u8 Status;   //1:Successful positioning 0：Positioning failed
}GNRMC;

typedef struct {
    double Lon;
    double Lat;
}Coordinates;



void L76XPortInit(void);

unsigned int GPS_Process(unsigned short nMain10ms);

//void SIM_Send_Alive(void);

//unsigned int SIM_Report_Data(void);

//void OnSIMComm(unsigned char CommData);

void SetL76XReadyFlag( unsigned char ucFlag );

unsigned char GetL76XReadyFlag( void );

void ClrGPSState( void );

unsigned char GetGPSState(void);

float GetGPSLongitude(void);

float GetGPSLatitude(void);

unsigned char GetGPSTaskRunFlag(void);

void SetGPSTaskRunFlag(unsigned char isTrue);

unsigned char GetGPSDataFlag(void);

void SetGetGPSDataFlag(unsigned char isTrue);

unsigned char GetLocationSuccessFlag(void);

void SetLocationSuccessFlag(unsigned char isTrue);



float GetLongitude(void);

void SetLongitude(float temp);

float GetLatitude(void);

void SetLatitude(float temp);

#endif
