#ifndef	_MAX44009_H_
#define	_MAX44009_H_

#define		DEFAULT_MAX_RATIO		1.70

typedef enum
{
	MAX_WORKING 	= 0,//???
	MAX_ERROR  		= 1,//?????
	MAX_IDLE 	 	= 2,//?????
	MAX_OTHER    	= 3,//????
}MAX_STATUS;

extern MAX_STATUS MaxStatus;

float Get_Ill_Value(void);

unsigned char MAX44009_Init(void);

void MAX_Process(unsigned short nMain10ms);

void StartMAXSensor(void);
#endif

