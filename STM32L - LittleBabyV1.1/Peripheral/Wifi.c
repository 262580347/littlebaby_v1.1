#include "Wifi.h"
#include "main.h"

#define 	COM_DATA_SIZE	256

#define		WIFI_POWER_PIN		GPIO_Pin_12
#define		WIFI_POWER_TYPE		GPIOA

#define		WIFI_POWER_ON()		GPIO_SetBits(WIFI_POWER_TYPE, WIFI_POWER_PIN)
#define		WIFI_POWER_OFF()	GPIO_ResetBits(WIFI_POWER_TYPE, WIFI_POWER_PIN)

static void WiFiUartSendStr(char *str)
{
#if( HARDWARE_VERSION == HARDWARE_VERSION_V1_0 )

	u3_printf(str);

#elif( HARDWARE_VERSION == HARDWARE_VERSION_V1_2 )
	
	u2_printf(str);

#endif

}

static void WiFi_Port_Reset(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);		
}

typedef __packed struct
{
	unsigned char 			magic; 	//0x55
	unsigned short 			length;	//存储内容长度	
	unsigned char 			chksum;	//校验和
	unsigned char 			data[100];		//设备ID
}WIFIPASSWORD;

static char s_WlanName[64];		//WLAN用户名

static char s_WlanPassword[64];	//WLAN密码



static void WiFi_Port_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = WIFI_POWER_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(WIFI_POWER_TYPE, &GPIO_InitStructure);	
	
	WIFI_POWER_OFF();
	u1_printf("WIFI_POWER_OFF\r\n");
	delay_ms(1000);
	delay_ms(1000);
	
	WIFI_POWER_ON();
	u1_printf("WIFI_POWER_ON\r\n");
	delay_ms(500);
}

static void ReadWlanName(void)
{
	u8 Name_Length, i;
	
	Name_Length = Check_Area_Valid(WIFI_USER_ADDR);
	if (Name_Length)
	{
		EEPROM_ReadBytes(WIFI_USER_ADDR, (u8 *)s_WlanName, sizeof(SYS_TAG) +  Name_Length);
		u1_printf("\r\n Name:");
		for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + Name_Length; i++)
		{
			u1_printf("%c", s_WlanName[i]);
		}
		u1_printf("\r\n");
	}
}

static void ReadWlanPassword(void)
{
	u8 Password_Length, i;
	
	Password_Length = Check_Area_Valid(WIFI_PASSWORD_ADDR);
	if (Password_Length)
	{
		EEPROM_ReadBytes(WIFI_PASSWORD_ADDR, (u8 *)s_WlanPassword, sizeof(SYS_TAG) +  Password_Length);
		u1_printf("\r\n Password:");
		for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + Password_Length; i++)
		{
			u1_printf("%c", s_WlanPassword[i]);
		}
		u1_printf("\r\n");
	}
}

static void TaskForWiFiCommunication(void)
{
	static u8 s_First = FALSE, WiFiStatus = 3, WiFiStep = 1, s_ErrCount = 0, s_RetryCount = 0, s_ErrAckCount = 0;
	static u8 s_CWLAPFirst = FALSE, s_WaitTime = 1, s_OverTimeCount = 0;
	static unsigned int s_RTCSumSecond = 0, s_RTCReStart = 0, s_AliveRTCCount;
	char strWlan[128], str[40];
	static u8 s_StartCalTimeFlag = TRUE;	//开始计算上报时间标志
	static u16 s_StartTime = 0, s_EndTime = 0;
	static u8 s_LastRTCDate = 32;
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	
	p_sys = GetSystemConfig();
	
	if(!s_First)
	{
		if(strncmp((char *)g_USART2_RX_BUF, "compiled", g_USART2_RX_CNT) == 0)
		{
			u1_printf("%s\r\n", g_USART2_RX_BUF);
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Clear_Uart2Buff();
		}		
		else 
		{		
			
		}
		
		RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
		s_LastRTCDate = RTC_DateStruct.RTC_Date;
		s_First = TRUE;
		g_TCRecStatus.LinkStatus = NotConnected;
		s_RTCReStart = GetRTCSecond();
		s_RTCSumSecond = GetRTCSecond();
	}
	
	if(g_TCRecStatus.LinkStatus != TCPConnected)
	{
		if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCReStart) > 600)
		{
			if(GetLockinTimeFlag())
			{
				SetLockinTimeFlag(FALSE);
				s_RTCReStart = GetRTCSecond();
			}
			else
			{		
				u1_printf("\r\n 10min Unconnect,Reboot\r\n");
				SetLogErrCode(LOG_CODE_RESET);
				StoreOperationalData();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();
			}
		}
	}
	else
	{
		s_RTCReStart = GetRTCSecond();
	}
	
	switch(WiFiStatus)
	{	
		case 0:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) > 10)
			{
				s_RTCSumSecond = GetRTCSecond();
				u1_printf(" Check ESP Connect...\r\n");
			}
		break;
		
		case 1:
			s_RetryCount++;
			if(s_RetryCount >= 3)
			{
				s_RetryCount = 0;
				WiFiStatus = 11;
				WiFiStep = 1;
				s_RTCSumSecond = GetRTCSecond();
				break;
			}
			SetTCProtocolRunFlag(FALSE);
			SetTCModuleReadyFlag(FALSE);
			WiFi_Port_Reset();
			WIFI_POWER_OFF();
			
			WiFiStatus++;
			
			s_RTCSumSecond = GetRTCSecond();
		break;
				
			
		case 2:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) > 1)
			{
				if(s_StartCalTimeFlag)
				{
					s_StartCalTimeFlag = FALSE;
					s_StartTime = GetSystem10msCount();
				}
				USART2_Config(WIFI_BAND_RATE);
				WiFi_Port_Init();
				WiFiStatus++;
				s_RTCSumSecond = GetRTCSecond();
			}
		break;
		
		case 3:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) > 1)
			{
				WiFiStatus++;
				WiFiStep = 1;		
				s_RTCSumSecond = GetRTCSecond();
			}
		break;
		
		case 4:

				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

				switch(WiFiStep)
				{
					case 1:
						s_WaitTime = 1;
						WiFiUartSendStr("AT\r\n");
					break;
					case 2:
						if(!s_CWLAPFirst)
						{
							s_WaitTime = 2;
							s_CWLAPFirst = TRUE;
							WiFiUartSendStr("AT+GMR\r\n");
							break;
						}
						else
						{
							s_WaitTime = 1;
							WiFiStep = 3;
						}

					case 3:
						s_WaitTime = 1;
						WiFiUartSendStr("AT+CWMODE=1\r\n");
					break;
					
					case 4:
						WiFiUartSendStr("AT+CWAUTOCONN=0\r\n");
					break;
					case 5:
						s_WaitTime = 8;
						if(Check_Area_Valid(WIFI_USER_ADDR) && Check_Area_Valid(WIFI_PASSWORD_ADDR))
						{
							sprintf(strWlan, "AT+CWJAP=\"%s\",\"%s\"\r\n", &s_WlanName[4], &s_WlanPassword[4]);
							WiFiUartSendStr(strWlan);
						}
						else
						{
							WiFiUartSendStr("AT+CWJAP=\"ESP8266WIFI\",\"12344321\"\r\n");
						}
					break;
						
					case 6:		//尝试连接默认热点
						s_WaitTime = 8;
						WiFiUartSendStr("AT+CWJAP=\"ESP8266WIFI\",\"12344321\"\r\n");
					break;
					
					case 7:
						s_WaitTime = 1;
						WiFiUartSendStr("AT+CIPMUX=0\r\n");//设置单链接

					break;
					
					case 8:
						WiFiUartSendStr("AT+CIPMODE=1\r\n");

					break;
					
					case 9:
						u1_printf("[WIFI]Establishing TCP links\r\n");
						sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d\r\n"
						,p_sys->Gprs_ServerIP[0]
						,p_sys->Gprs_ServerIP[1]
						,p_sys->Gprs_ServerIP[2]
						,p_sys->Gprs_ServerIP[3]
						,p_sys->Gprs_Port	);
					
//						u1_printf("%s", str);
						WiFiUartSendStr(str);
						s_WaitTime = 8;
					break;
					
					case 10:
						s_WaitTime = 2;
						WiFiUartSendStr("AT+CIPSEND\r\n");

					break;		
					
					case 20:
						WiFiUartSendStr("AT+CWQAP\r\n");

					break;
				}
				WiFiStatus++;
				s_RTCSumSecond = GetRTCSecond();
		break;
		
		case 5:
			
			if(s_ErrAckCount >= 5)
			{
				u1_printf(" Err Over\r\n");
				s_ErrAckCount = 0;
				WiFiStatus = 1;
				WiFiStep = 1;
				g_TCRecStatus.LinkStatus = NotConnected;
			}
			
			if(g_Uart2RxFlag)	//收到一帧数据包
			{			
				if(g_USART2_RX_CNT >= USART3_REC_LEN)
				{
					g_USART2_RX_BUF[USART3_REC_LEN-1] = 0;
					g_USART2_RX_CNT = USART3_REC_LEN;							
				}
				
					u1_printf("%s\r\n", g_USART2_RX_BUF);
				
				switch(WiFiStep)
				{
					case 1:
					case 3:
					case 4:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;							
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
						{
							s_ErrAckCount++;
						}
					break;
					
					case 2:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;	
						}
						
					break;
					
					case 5:	//连接存储的地址
						if((strstr((const char *)g_USART2_RX_BUF,"WIFI CONNECTED")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"WIFI GOT IP")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							g_TCRecStatus.LinkStatus = GotIP;
							WiFiStep = 7;
							WiFiStatus--;	
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"WIFI DISCONNECT")) != NULL )
						{
							g_TCRecStatus.LinkStatus = NotConnected;
							u1_printf("\r\n WIFI Password Error\r\n");
							StoreOperationalData();
							WiFiStep = 6;
							WiFiStatus--;	
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"FAIL")) != NULL )
						{
							u1_printf(" Retry\r\n");
							WiFiStatus--;	
							s_ErrAckCount++;
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )//收到ERROR代表用户或密码错误或不存在该热点，做其他处理
						{
							u1_printf(" No AP\r\n");
							WiFiStep = 6;
							WiFiStatus--;	
						}
						
					break;
					
					case 6:		//连接默认热点
						if((strstr((const char *)g_USART2_RX_BUF,"WIFI CONNECTED")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"WIFI GOT IP")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							g_TCRecStatus.LinkStatus = GotIP;
							WiFiStep = 7;
							WiFiStatus--;	
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"WIFI DISCONNECT")) != NULL )
						{
							g_TCRecStatus.LinkStatus = NotConnected;
							u1_printf("\r\n WIFI Password Error\r\n");
							WiFiStep = 5;
							s_ErrAckCount++;
							WiFiStatus--;	
							delay_ms(100);
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"FAIL")) != NULL )
						{
							u1_printf(" Retry\r\n");
							WiFiStep = 5;
							WiFiStatus--;	
							s_ErrAckCount++;
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )//收到ERROR代表用户或密码错误或不存在该热点，做其他处理
						{
							u1_printf(" No AP\r\n");
							WiFiStep = 5;
							WiFiStep = 1;
							WiFiStatus = 1;	
						}
						
					break;
						
					case 7:
					case 8:	
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;					
						}
					break;
						
					case 9:
						if((strstr((const char *)g_USART2_RX_BUF,"CONNECT")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;							
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )						
						{	
							WiFiStatus--;		
							delay_ms(500);
							s_ErrAckCount++;
						}
							
					break;
						
					case 10:
						if((strstr((const char *)g_USART2_RX_BUF,">")) != NULL )
						{
							WiFiStep = 1;	
							WiFiStatus++;								
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )						
						{	
							WiFiStatus--;		
							delay_ms(500);
							s_ErrAckCount++;
						}
					break;	
					
					
						
				}
				
				if(s_ErrAckCount >= 10)
				{
					WiFiStatus = 1;
					WiFiStep = 1;
					s_ErrAckCount = 0;
					SetLogErrCode(LOG_CODE_TIMEOUT);
				}
					
				Clear_Uart2Buff();

			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= s_WaitTime)
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				WiFiStatus--;
				u1_printf(" AT No Ack\r\n");
				g_TCRecStatus.LinkStatus = NotConnected;
				s_ErrCount++;
				s_RTCSumSecond = GetRTCSecond();
				if(s_ErrCount >= 3)
				{
					s_ErrCount = 0;
					WiFiStatus = 1;
					WiFiStep = 1;
				}
			}
			
			
		break;
		
		case 6:
			u1_printf("\r\n Success to TCP Connect\r\n");
			SetTCProtocolRunFlag(TRUE);
			SetTCModuleReadyFlag(TRUE);
			WiFiStatus++;
			g_TCRecStatus.LinkStatus = TCPConnected;
			g_TCRecStatus.LastLinkTime = GetRTCSecond();
		break;
		
		case 7:
			if(g_TCRecStatus.ResetFlag)
			{
				g_TCRecStatus.ResetFlag = FALSE;
				u1_printf(" \r\n WIFI Retry Connect\r\n");
				SetTCProtocolRunFlag(FALSE);
				SetTCModuleReadyFlag(FALSE);
				g_TCRecStatus.LinkStatus = NotConnected;
				s_ErrCount = 0;
				WiFiStatus = 1;
				WiFiStep = 1;
				s_RTCSumSecond = GetRTCSecond();
			}
			else if(GetTCModuleReadyFlag())		//SIM就绪
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	
				{
					s_AliveRTCCount = GetRTCSecond();
					Mod_Send_Alive(WIFI_COM);
					delay_ms(100);
					break;
				}
				
				s_EndTime = GetSystem10msCount();
				SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
				u1_printf("\r\n 连接耗时:%.2fs\r\n\r\n", CalculateTime(s_EndTime, s_StartTime)/100.0);
				Mod_Report_LowPower_Data(WIFI_COM);	//发送数据包
				WiFiStatus++;
				s_RTCSumSecond = GetRTCSecond();
			}

		break;
			
		case 8:
			if(g_TCRecStatus.DataAckFlag)
			{
				g_TCRecStatus.DataAckFlag = FALSE;
				WiFiStatus++;
				s_RTCSumSecond = GetRTCSecond();
				SetLogErrCode(LOG_CODE_SUCCESS);
				WriteCommunicationSuccessCount();
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= 10)
			{
				WiFiStatus--;
				s_OverTimeCount++;
				if(s_OverTimeCount >= 3)
				{
					s_OverTimeCount = 0;
					WiFiStatus = 10;
					SetLogErrCode(LOG_CODE_NOSERVERACK);
				}
				s_RTCSumSecond = GetRTCSecond();
			}
		break;
			
		case 9:
			if(GetServerConfigCmdFlag() == FALSE)
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= 1)	//延时1s等待GPRS下行数据
				{
					u1_printf("\r\n Wait for server downlink data timeout,Sleep\r\n");
					WiFiStatus++;					
				}
			}
			else 		//收到关闭Socket指令，可以休眠
			{
				u1_printf("\r\n Receive reply packet,Sleep\r\n");
				SetServerConfigCmdFlag(FALSE);
				WiFiStatus++;						
			}
		break;
		
		case 10:
			TXB0108_GPIO_Reset();
			WIFI_POWER_OFF();
			WiFiStatus++;
			s_RTCSumSecond = GetRTCSecond();
			
			StoreOperationalData();	
			WriteCommunicationCount();			
		break;
		
		case 11:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();	
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
				SetLogErrCode(LOG_CODE_CLEAR);	
				SetTCProtocolRunFlag(FALSE);	
				u1_printf("\r\n [%02d:%02d:%02d][WIFI]重新上电\r\n", RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				s_RetryCount = 0;
				s_OverTimeCount = 0;
				s_StartCalTimeFlag = TRUE;
				g_TCRecStatus.ResetFlag = TRUE;
				WiFiStatus = 7;
				
				RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
			
				if(s_LastRTCDate != RTC_DateStruct.RTC_Date)	//日期更新，获取经纬度
				{
					s_LastRTCDate = RTC_DateStruct.RTC_Date;
					SetGPSTaskRunFlag(TRUE);
					SetGetGPSDataFlag(FALSE);
				}
			}		
			else			//未到上报时间，继续休眠
			{
				if((Sht31Status != SHT_WORKING) && (MaxStatus != MAX_WORKING) && (BmpStatus != BMP_WORKING) &&(BatStatus != BAT_WORKING))
				{					
					if(GetTCModuleReadyFlag())
					{
						SetStopModeTime(TRUE, STOP_TIME);
					}
					else
					{
						SetStopModeTime(TRUE, ERR_STOP_TIME);
					}
				}					
			}
		break;
			
		default:
			u1_printf(" 意外错误\r\n");
			WiFiStatus = 1;
		break;
		
	}
	

}

static void WiFi_Init(void)
{			
	ReadWlanName();
	
	ReadWlanPassword();			
	
	USART2_Config(WIFI_BAND_RATE);
	
	WiFi_Port_Init();
	
	SetTCProtocolRunFlag(FALSE);
	SetTCModuleReadyFlag(FALSE);
		
}

void WiFiProcess(void)
{
	static u8 s_First = FALSE;
	
	if(s_First == FALSE)
	{
		s_First = TRUE;
		
		USART2_Config(WIFI_BAND_RATE);
		
		WiFi_Init();
	}
	
	TaskForWiFiCommunication();
}
