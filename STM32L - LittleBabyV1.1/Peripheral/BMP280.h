#ifndef  _BMP_280_H_
#define  _BMP_280_H_

#include "mytype.h"

#define BMP_SENSOR_STATUS_NORMAL	0
#define BMP_SENSOR_STATUS_ERR		1


UINT8 BMP_Process( UINT16 nMain100ms );

float GetBMPTemp( void );
float GetBMPPress( void );
float GetBMPAltitude( void );

unsigned char BMP_Process( unsigned short nMain100ms );

int BMP280_Init (void);

typedef enum
{
	BMP_WORKING 	= 0,//???
	BMP_ERROR  		= 1,//?????
	BMP_IDLE 	 	= 2,//?????
	BMP_OTHER    	= 3,//????
}BMP_STATUS;

extern BMP_STATUS BmpStatus;

void StartBMPSensor(void);
#endif



