#ifndef  _LOW_POWER_H_
#define  _LOW_POWER_H_

#define		MEASUREMENT_INTERVAL  1 

#define		STOP_TIME			10

#define		ERR_STOP_TIME		2

void EnterStopMode(unsigned char Second);
void EnterStopMode_SIM(unsigned char Second);	

void EXTI5_Config(void);
void EXTI5_Reset(void);
void EXTI17_Reset(void);

void StartSensorMeasure(void);

void LowPower_Process(void);
	
void StopModeProcess(void);

void SetStopModeTime(unsigned char isTrue, unsigned char StopTime);

#endif
