/**********************************
说明：慧云配置工具串口通信程序
	  
作者：关宇晟
版本：V2017.4.3
***********************************/
#include "Comm_Debug.h"
#include "main.h"

#define		MAX_BUFF	256
#define		COM_DATA_SIZE	256

#define		DEBUG_FLG_ON		1
#define		DEBUG_FLG_OFF		0
#define		DEBUG_FLG_DEFAULT	DEBUG_FLG_OFF		//默认关闭调试输出

DEBUG_CFG	s_Debug_config;

static u16 PackMsg(u8* srcbuf,u16 srclen,u8* destbuf,u16 destsize)
{
	u16 i,j;

	j = 0;
	i = 0;
	destbuf[j++] = BOF_VAL;
	for (i=0;i<srclen;i++)
	{
		switch (srcbuf[i])
		{
		case 0x7d:
			destbuf[j++]=0x7d;
			destbuf[j++]=0x5d;
			break;		
		case 0x7e:
			destbuf[j++]=0x7d;		
			destbuf[j++]=0x5e;
			break;
		case 0x21:
			destbuf[j++]=0x7d;		
			destbuf[j++]=0x51;
			break;
		default:
			destbuf[j++]=srcbuf[i];
			break;
		}
		if ( j > destsize- 2) break;
	}

	destbuf[j++] = EOF_VAL;

	return j;
}

static void Debug_Send_Packet(u8 *packet, u8 len)
{
	u8 send_buf[MAX_BUFF];
	u8 send_len;
	
	send_len = PackMsg(packet, len, send_buf, MAX_BUFF);			//数据包转义处理					
	USART1_DMA_Send(send_buf, send_len);	
}

void Debug_Config_Init(void)		//烧录
{	
//			s_Debug_config.Magic 		= 0x55;
//			s_Debug_config.Length 		= 0X01;
//			s_Debug_config.DebugFlg 	= DEBUG_FLG_OFF;
//			s_Debug_config.Chksum = 	Calc_Checksum((unsigned char *)&s_Debug_config.DebugFlg, 1);
//			EEPROM_WriteBytes(DEBUG_FLG_ADDR, (unsigned char *)&s_Debug_config, sizeof(DEBUG_CFG));
	
	if (Check_Area_Valid(DEBUG_FLG_ADDR))
	{
		EEPROM_ReadBytes(DEBUG_FLG_ADDR, (unsigned char *)&s_Debug_config, sizeof(DEBUG_CFG));
		
		if(s_Debug_config.DebugFlg == DEBUG_FLG_ON)
		{
			u1_printf("\r\n  DEBUG ON\r\n");
		}
		else if(s_Debug_config.DebugFlg == DEBUG_FLG_OFF)
		{
			u1_printf("\r\n  DEBUG OFF\r\n");
		}
		else
		{
			s_Debug_config.Magic 		= VAILD;
			s_Debug_config.Length 		= 0X01;
			s_Debug_config.DebugFlg 	= DEBUG_FLG_DEFAULT;
			s_Debug_config.Chksum = 	Calc_Checksum((unsigned char *)&s_Debug_config.DebugFlg, 1);
			EEPROM_WriteBytes(DEBUG_FLG_ADDR, (unsigned char *)&s_Debug_config, sizeof(DEBUG_CFG));
					
			if(s_Debug_config.DebugFlg == DEBUG_FLG_ON)
			{
				u1_printf("\r\n  GET DEBUG CONFIG ERROR, INIT:DEBUG ON\r\n");
			}
			else if(s_Debug_config.DebugFlg == DEBUG_FLG_OFF)
			{
				u1_printf("\r\n  GET DEBUG CONFIG ERROR, INIT:DEBUG OFF\r\n");
			}
		}	
	}
	else
	{
		s_Debug_config.Magic 		= VAILD;
		s_Debug_config.Length 		= 0X01;
		s_Debug_config.DebugFlg 	= DEBUG_FLG_DEFAULT;
		s_Debug_config.Chksum = 	Calc_Checksum((unsigned char *)&s_Debug_config.DebugFlg, 1);
		EEPROM_WriteBytes(DEBUG_FLG_ADDR, (unsigned char *)&s_Debug_config, sizeof(DEBUG_CFG));
			
		u1_printf("\r\n  INIT:DEBUG OFF\r\n");
	}
}

unsigned char GetDebugFlag(void)
{	
	return s_Debug_config.DebugFlg;
}

void SetDebugFlag(unsigned char data)
{		
	unsigned char temp;
	if(data == CMD_SET_DEBUG_ON)
	{
		u1_printf("\r\n  SET DEBUG ON\r\n");
		temp = DEBUG_FLG_ON;
		if(s_Debug_config.DebugFlg != temp)
		{
			s_Debug_config.DebugFlg = temp;;
			s_Debug_config.Chksum = 	Calc_Checksum((unsigned char *)&s_Debug_config.DebugFlg, 1);
			EEPROM_WriteBytes(DEBUG_FLG_ADDR, (unsigned char *)&s_Debug_config, sizeof(DEBUG_CFG));
		}
	}
	else if(data == CMD_SET_DEBUG_OFF)
	{
		u1_printf("\r\n  SET DEBUG OFF\r\n");
		temp = DEBUG_FLG_OFF;
		if(s_Debug_config.DebugFlg != temp)
		{
			s_Debug_config.DebugFlg = temp;;
			s_Debug_config.Chksum = 	Calc_Checksum((unsigned char *)&s_Debug_config.DebugFlg, 1);
			EEPROM_WriteBytes(DEBUG_FLG_ADDR, (unsigned char *)&s_Debug_config, sizeof(DEBUG_CFG));
		}
	}
	else
	{
		u1_printf("  SET DEBUG CONFIG ERROR, NO CHANGE\r\n");
	}	
}

//void Debug_Output_Config(unsigned char * data, u8 lenth)
//{		
//	u8 send_buf[64];
//	u32 send_len;
//	
//	CLOUD_HDR *hdr;
//	
//	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(u16));
//	hdr = (CLOUD_HDR *)send_buf;
//	hdr->dir = 1;
//	send_len = sizeof(CLOUD_HDR)+sizeof(u16);
//		
//	//配置调试信息输出标志
//	SetDebugFlag(hdr->cmd);
//	
//	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
//	send_len++;
//	
//	Debug_Send_Packet(send_buf, send_len);	
//}

void Debug_Out(char *pstr)
{
	if(GetDebugFlag())
	{
		u1_printf(pstr);
	}
}

void Debug_OutReceive_ASC(char * p, unsigned short len)
{
	char str[500];
	unsigned short i = 0;
	for(i = 0; (p[i] != '\0') && (i < len); i++)
	{
		sprintf(&str[i], "%c", p[i]);
	}
	Debug_Out(str);
	Debug_Out("\r\n");
}

void Debug_OutReceive_Hex(char * p, unsigned short len)
{
	char str[500];
	unsigned short i = 0;
	for(i = 0; (p[i] != '\0') && (i < len); i++)
	{
		sprintf(&str[i*2], "%02X ", p[i]);
	}
	Debug_Out(str);
	Debug_Out("\r\n");
}
static void Debug_Get_Time(u8 *data, u8 lenth)
{
	u8 send_buf[64];
	u32 send_len;
	
	CLOUD_HDR *hdr;
	u8 *p_data;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(6);
	p_data = &send_buf[sizeof(CLOUD_HDR)];
	
	*(p_data++) = RTC_TimeStructure.RTC_Seconds;
	*(p_data++) = RTC_TimeStructure.RTC_Minutes; 
	*(p_data++) = RTC_TimeStructure.RTC_Hours;

	*(p_data++) = RTC_DateStruct.RTC_Date;
	*(p_data++) = RTC_DateStruct.RTC_Month;
	*(p_data++) = (u8)(RTC_DateStruct.RTC_Year);
	
	send_len += 6;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;

	Debug_Send_Packet(send_buf, send_len);
	
}	

static void Debug_Updata_Time(u8 *data, u8 lenth)
{
	u8 *p_data;
	u8 send_buf[64];
	u32 send_len;
	CLOUD_HDR *hdr;
	
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
		
	p_data = (u8 *)&data[sizeof(CLOUD_HDR)];
	
	RTC_TimeStructure.RTC_Seconds	 = *(p_data++);
	RTC_TimeStructure.RTC_Minutes 	 = *(p_data++); 
	RTC_TimeStructure.RTC_Hours	 	 = *(p_data++);
	RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure);

	RTC_DateStruct.RTC_Date  	= *(p_data++);
	RTC_DateStruct.RTC_Month  	= *(p_data++);
	RTC_DateStruct.RTC_Year  	= *(p_data++);	
	RTC_DateStruct.RTC_WeekDay  = 1;
	RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	SetLockinTimeFlag(TRUE);
	Debug_Send_Packet(send_buf, send_len);
}

static void Debug_Read_SystemInfo(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEM_INFO 	*sys_info;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEM_INFO));
	sys_info = Get_SystemInfo();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_info, sizeof(SYSTEM_INFO));
	send_len += sizeof(SYSTEM_INFO);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}


static void Debug_Read_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG));
	sys_cfg = GetSystemConfig();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_cfg, sizeof(SYSTEMCONFIG));
	send_len += sizeof(SYSTEMCONFIG);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}

static unsigned char Debug_Set_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[64], ReturnVal = 0;
	u32 send_len;
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	
	sys_cfg = (SYSTEMCONFIG *)&data[sizeof(CLOUD_HDR)];
	ReturnVal = Set_System_Config(sys_cfg);
	if(ReturnVal == FALSE)
	{
		return FALSE;
	}
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	

	return TRUE;
}

static u32 Debug_Input_Count(u8 *data, u32 len)
{
	CLOUD_HDR *hdr;
	u32 count;
	u8 send_buf[64];
	u32 send_len;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(u32));
	
	count = 0;
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &count, sizeof(u32));
	send_len += sizeof(u32);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	return 1;
}

//读取一个单元输入参量配置信息 --0x11
static u32 Debug_Get_Input(u8 *data, u32 len)
{
	u8 send_buf[128];
	u32 send_len;
	
	CLOUD_HDR *hdr;
	MAP_IN_ITEM *item;
	u16 input_ch;
	
	input_ch = *(uint16 *)&data[sizeof(CLOUD_HDR)];		
	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(u16));
	send_len = sizeof(CLOUD_HDR)+sizeof(u16);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	
	item = get_map_in_item(input_ch);
	if (item)
	{
		hdr->payload_len = swap_word(sizeof(u16)+sizeof(MAP_IN_ITEM)+item->spec_len);
		memcpy(&send_buf[send_len], item, sizeof(MAP_IN_ITEM)+item->spec_len);
		send_len += sizeof(MAP_IN_ITEM)+item->spec_len;
	}
	else
	{
		hdr->payload_len = swap_word(sizeof(u16));
	}
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);
	
	return 1;
}

//设置一个单元输入参量配置信息 --0x12
static u32 Debug_Set_Input(u8 *data, u32 len)
{
//	u8 send_buf[128];
//	u32 send_len;
//	
//	CLOUD_HDR *hdr;
//	MAP_IN_ITEM *item;
//	u16 input_ch;
//	u8 ack;
//	
//	input_ch = *(uint16 *)&data[sizeof(CLOUD_HDR)];		
//	item = (MAP_IN_ITEM *)&data[sizeof(CLOUD_HDR)+sizeof(u16)];	//配置数据
//	
//	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(u16));
//	send_len = sizeof(CLOUD_HDR)+sizeof(u16);
//	hdr = (CLOUD_HDR *)send_buf;
//	hdr->dir = 1;
//	hdr->payload_len = swap_word(sizeof(u16)+1);
//	
//	ack = set_map_in_item(input_ch, item);	   				//设置结果
//	hwl_input_init(input_ch, FALSE);
//	
//	send_buf[send_len] = ack;
//	send_len++;
//	
//	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
//	send_len++;
//	
//	Debug_Send_Packet(send_buf, send_len);
//	
	return 1;
	
}

//删除一个输入配置 --0xF7 ===========================================
static u32 Debug_Delete_Input(u8 *data, u32 len)
{
	u8 send_buf[64];
	u32 send_len;
	
	CLOUD_HDR *hdr;
	u16 channel;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR)+sizeof(u16));
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	send_len = sizeof(CLOUD_HDR)+sizeof(u16);
	
	channel = *(u16 *)&data[sizeof(CLOUD_HDR)];
	
	//删除一个输入配置信息
	set_map_in_item(channel, NULL);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	Debug_Send_Packet(send_buf, send_len);
	
	return 1;
}

static u32 Debug_Output_Count(u8 *data, u32 len)
{
	CLOUD_HDR *hdr;
	u32 count;
	u8 send_buf[64];
	u32 send_len;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(u32));
	
	count = 0;
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &count, sizeof(u32));
	send_len += sizeof(u32);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	return 1;
}

static void Debug_Enter_Test_Mode(u8 *data, u32 len)
{
	CLOUD_HDR *hdr;
	u8 send_buf[64];
	u32 send_len;
	
	if(g_ComTestFlag == FALSE)
	{
		g_ComTestFlag = TRUE;
		
		SIM7600CEPortInit();
		SIM7600CE_RST_ON();
		SIM7600CE_RF_DISABLE_ON();
		SetTCProtocolRunFlag(FALSE);
		USART2_Config(SIM_BAND_RATE);
		delay_ms(200);
		SIM7600CE_PWR_ON();
	}
	else
	{
		g_ComTestFlag = FALSE;
	}
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(u32));
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);			
}

static void WLAN_Read_Name(u8 *data, u32 len)
{
	u8 send_buf[80], Name_Length, User_Password[64];
	u8 send_len;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	
	Name_Length = Check_Area_Valid(WIFI_USER_ADDR);
	if (Name_Length)
	{
		EEPROM_ReadBytes(WIFI_USER_ADDR, (u8 *)User_Password, sizeof(SYS_TAG) +  Name_Length);
//		for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + Name_Length; i++)
//		{
//			u1_printf("%c", User_Password[i]);
//		}
//		u1_printf("\r\n");
	}
		
	hdr->payload_len = swap_word(Name_Length);
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &User_Password[sizeof(SYS_TAG)], Name_Length);
	send_len += Name_Length;
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);		
}

static void WLAN_Read_Password(u8 *data, u32 len)
{
	u8 send_buf[80], Password_Length, User_Password[64];
	u8 send_len;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	
	Password_Length = Check_Area_Valid(WIFI_PASSWORD_ADDR);
	if (Password_Length)
	{
		EEPROM_ReadBytes(WIFI_PASSWORD_ADDR, (u8 *)User_Password, sizeof(SYS_TAG) +  Password_Length);
//		for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + Password_Length; i++)
//		{
//			u1_printf("%c", User_Password[i]);
//		}
//		u1_printf("\r\n");
	}
		
	hdr->payload_len = swap_word(Password_Length);
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &User_Password[sizeof(SYS_TAG)], Password_Length);
	send_len += Password_Length;
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);		
}

static void WLAN_Set_Name(u8 *data, u32 len)
{
	SYS_TAG tag;
	u8 send_buf[80], NameBuf[80], send_len = 0, NameLength = 0;
	
	CLOUD_HDR *hdr;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	NameLength = hdr->payload_len;
	hdr->payload_len = swap_word(hdr->payload_len);
		
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &data[sizeof(CLOUD_HDR)], NameLength);
	send_len += NameLength;
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	tag.magic = VAILD;
	tag.length = NameLength;
	tag.chksum = Calc_Checksum((unsigned char *)&data[sizeof(CLOUD_HDR)], NameLength);
	
	memcpy(NameBuf, &tag, sizeof(SYS_TAG));
	memcpy(&NameBuf[sizeof(SYS_TAG)], &data[sizeof(CLOUD_HDR)], NameLength);
	
	EEPROM_EraseWords(WIFI_USER_BLOCK);
	EEPROM_WriteBytes(WIFI_USER_ADDR, (unsigned char *)NameBuf, sizeof(SYS_TAG) + NameLength);

//	for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + tag.length; i++)
//	{
//		u1_printf("%c", NameBuf[i]);
//	}
//	u1_printf("\r\n");	
}

static void WLAN_Set_Password(u8 *data, u32 len)
{
	SYS_TAG tag;
	u8 send_buf[80], PasswordBuf[80], send_len = 0, PasswordLength = 0;
	
	CLOUD_HDR *hdr;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	PasswordLength = hdr->payload_len;
	hdr->payload_len = swap_word(hdr->payload_len);
		
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &data[sizeof(CLOUD_HDR)], PasswordLength);
	send_len += PasswordLength;
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	tag.magic = VAILD;
	tag.length = PasswordLength;
	tag.chksum = Calc_Checksum((unsigned char *)&data[sizeof(CLOUD_HDR)], PasswordLength);
	
	memcpy(PasswordBuf, &tag, sizeof(SYS_TAG));
	memcpy(&PasswordBuf[sizeof(SYS_TAG)], &data[sizeof(CLOUD_HDR)], PasswordLength);
	
	EEPROM_EraseWords(WIFI_PASSWORD_BLOCK);
	EEPROM_WriteBytes(WIFI_PASSWORD_ADDR, (unsigned char *)PasswordBuf, sizeof(SYS_TAG) + PasswordLength);

//	for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + tag.length; i++)
//	{
//		u1_printf("%c", PasswordBuf[i]);
//	}
//	u1_printf("\r\n");
}

void OnDebug(u8 *data, u8 lenth)
{
	u8 ReturnVal = 0;
	CLOUD_HDR *hdr;

	hdr = (CLOUD_HDR *)data;
	switch(hdr->cmd)
	{
		case CMD_GET_DATETIME:	//获取设备时间	--0xF1
			Debug_Get_Time(data, lenth);
			break;
		case CMD_SET_DATETIME:	 //设置设备时间 --0xF2
			Debug_Updata_Time(data, lenth);
			break;
		
		case CMD_RD_SYS_INFO:	//读取系统信息 --0xF3
			Debug_Read_SystemInfo(data, lenth);
			break;
	
		case CMD_RD_SYS_CFG:	//读取系统配置 --0xF5
			Debug_Read_SystemConfig(data, lenth);
			break;
		case CMD_WR_SYS_CFG:	//设置系统配置 --0xF6
			ReturnVal = Debug_Set_SystemConfig(data, lenth);
			if(ReturnVal == FALSE)
			{
				return;
			}
			delay_ms(300);
			u1_printf("\r\n 等待重启...\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
			break;
//		
//		//单机配置输入通道================================================================
		case CMD_INPUT_COUNT:	   //获取设备额定单元输入参量个数 --0x10
			Debug_Input_Count(data, lenth);
		break;
		case CMD_GET_INPUT:		  //读取一个单元输入参量配置信息 --0x11
			Debug_Get_Input(data, lenth);
		break;
		case CMD_SET_INPUT:		  //设置一个单元输入参量配置信息 --0x12
			Debug_Set_Input(data, lenth);
		break;
		case CMD_DEL_INPUT:	   //删除一个输入配置 --0xF7
			Debug_Delete_Input(data, lenth);
		break;
		//单机配置输出通道=================================================================
		case CMD_OUTPUT_COUNT:	 //获取设备额定单元输出通道个数	--0x13
			Debug_Output_Count(data, lenth);
			break;
		case CMD_GET_OUTPUT:	 //读取一个单元输出通道关联管脚配置信息--0x14

			break;
		case CMD_SET_OUTPUT:	//设置一个单元输出通道关联管脚配置信息--0x15

			break;
		case CMD_DEL_OUTPUT:   //删除一个输出配置  --0xF8

			break;	
		//WLAN 功能区
		case CMD_READ_WLAN_NAME://读取WLAN名称
			WLAN_Read_Name(data, lenth);
		break;
		
		case CMD_READ_WLAN_PASSWORD://读取WLAN密码
			WLAN_Read_Password(data, lenth);
		break;
		
		case CMD_SET_WLAN_NAME://读取WLAN名称
			WLAN_Set_Name(data, lenth);
		break;
		
		case CMD_SET_WLAN_PASSWORD://读取WLAN密码
			WLAN_Set_Password(data, lenth);
		break;
		
		//DEBUG 功能区
		case CMD_SET_DEBUG_OFF:		  //关闭调试信息输出 --0xAC
		case CMD_SET_DEBUG_ON:		  //打开调试信息输出 --0xAD
			SetDebugFlag(hdr->cmd);
		break;
		
		case CMD_TEST_MODE:
			Debug_Enter_Test_Mode(data, lenth);		//进入低功耗设备配置模式	关闭通信，不进入停止模式
			break;
		
		case CMD_RESET:
			u1_printf("\r\n 设备重启指令...\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
			break;
	}
}


