/**********************************
说明:日志存储

日志使用EEPROM	10~64  区块
每区块有16*4字节
每4字节作为最小存储单位

格式为 | 日（8bits）、时（8bits）、分（8bits）、秒（8bits） |  | 温度（16bits）、 湿度（16bits） | | 光照（16bits）、压力（16bits） |	| 电压（16bits）、错误码1（8bits）、错误码2（8bits）、错误码3（8bits）、错误码4（8bits） |
作者:关宇晟
版本:V2019.07.05
***********************************/

#include "main.h"
#include "LogStore.h"

#define		COM_LOG_NUM		7
#define		DATA_MAX		16
#define		LOGBLOCKMAX		16

#define		LOG_ROOM_MAX	216	//216个日志区
#define		STORE_ERR_CODE	0XEEEE
typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//存储内容长度	
	unsigned char 			Chksum;	//校验和
	unsigned short			PointerAddr;
}LOGSTORE_CFG;

LOGSTORE_CFG	LogStoteConfig;
LOGSTORE_CFG	*pthis = NULL;

typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//存储内容长度	
	unsigned char 			Chksum;	//校验和
	unsigned int			CommunicationCount;	//总上报次数
	unsigned int			SuccessCount;	//成功次数
}COMLOG_CFG;

COMLOG_CFG		ComLogConfig;
COMLOG_CFG		*p_ComLog = NULL;

u8 s_ComLogBuf[sizeof(COMLOG_CFG)] ;	//断线日志Buff
u8 s_LogData[sizeof(LOGSTORE_CFG)] ;	//存储日志Buff
u16 s_LogErrCode = 0;

static unsigned short s_LinkNetTime = 0;	//连接时长
static unsigned short s_CSQValue = 0;			//信号强度

void SetLinkNetTime(unsigned short Time)
{
	s_LinkNetTime = Time;
}

unsigned short GetLinkNetTime(void)
{
	return s_LinkNetTime;
}

void SetCSQValue(unsigned short data)
{
	s_CSQValue = data;
}

unsigned short GetCSQValue(void)
{
	return s_CSQValue;
}
void SetLogErrCode(unsigned short ErrCode)
{
	if(ErrCode == 0xFF)
	{
		s_LogErrCode = 0;
	}
	else if(ErrCode < 16)
	{
		s_LogErrCode |= (1<<ErrCode);
	}
}

void ClearLogErrCode(unsigned short ErrCode)
{
	s_LogErrCode &= ~(1<<ErrCode);
}

void LogStorePointerInit(void)	//日志存储区初始化
{
	if (Check_Area_Valid(LOG_STORE_POINTER_ADDR))
	{
		EEPROM_ReadBytes(LOG_STORE_POINTER_ADDR, s_LogData, sizeof(LOGSTORE_CFG));
		
		pthis = (LOGSTORE_CFG *)s_LogData;
		
		LogStoteConfig = *pthis;
	}
	else	//第一次上电初始化
	{
		LogStoteConfig.Magic 		= VAILD;
		LogStoteConfig.Length 		= 0X02;
		LogStoteConfig.PointerAddr 	= 0;
		LogStoteConfig.Chksum = Calc_Checksum((unsigned char *)&LogStoteConfig.PointerAddr, 2);
		EEPROM_WriteBytes(LOG_STORE_POINTER_ADDR, (unsigned char *)&LogStoteConfig, sizeof(LOGSTORE_CFG));
		u1_printf("\r\n LogWrite the default value for the first power on\r\n");
	}
}

void CommunicatinLogInit(void)	//通信断线记录初始化
{
	float SuccessRate = 0.0;
	
	if (Check_Area_Valid(COM_LOG_ADDR))
	{
		EEPROM_ReadBytes(COM_LOG_ADDR, s_ComLogBuf, sizeof(COMLOG_CFG));
		
		p_ComLog = (COMLOG_CFG *)s_ComLogBuf;
		
		ComLogConfig = *p_ComLog;
		
		if(ComLogConfig.CommunicationCount > 0 && ComLogConfig.SuccessCount <= ComLogConfig.CommunicationCount)
		{
			SuccessRate = ((float)ComLogConfig.SuccessCount/(float)ComLogConfig.CommunicationCount)*100.0;
			
			u1_printf("\r\n 设备上报次数:%d,成功次数:%d,成功率:%2.2f%%\r\n", ComLogConfig.CommunicationCount, ComLogConfig.SuccessCount, SuccessRate);
		}
	}
	else	//第一次上电初始化
	{
		ComLogConfig.Magic 				= VAILD;
		ComLogConfig.Length 			= 0X08;
		ComLogConfig.CommunicationCount = 0;
		ComLogConfig.SuccessCount		= 0;
		ComLogConfig.Chksum = Calc_Checksum((unsigned char *)&ComLogConfig.CommunicationCount, 8);
		EEPROM_WriteBytes(COM_LOG_ADDR, (unsigned char *)&ComLogConfig, sizeof(COMLOG_CFG));
		u1_printf("\r\n Communication Log First Initialization\r\n");
	}	
}

void WriteCommunicationCount(void)
{
	static unsigned char s_BuffData[20];
	
	if (Check_Area_Valid(COM_LOG_ADDR))
	{
		EEPROM_ReadBytes(COM_LOG_ADDR, s_BuffData, sizeof(COMLOG_CFG));
		
		p_ComLog = (COMLOG_CFG *)s_BuffData;
		
		ComLogConfig.Magic 				= VAILD;
		ComLogConfig.Length 			= sizeof(COMLOG_CFG) - 4;
		ComLogConfig.CommunicationCount = p_ComLog->CommunicationCount;
		ComLogConfig.SuccessCount 		= p_ComLog->SuccessCount;
		ComLogConfig.CommunicationCount++;
		
		ComLogConfig.Chksum = Calc_Checksum((unsigned char *)&ComLogConfig.CommunicationCount, ComLogConfig.Length);
		EEPROM_WriteBytes(COM_LOG_ADDR, (unsigned char *)&ComLogConfig, sizeof(COMLOG_CFG));
		u1_printf("\r\n CommunicationCount:%d\r\n", ComLogConfig.CommunicationCount);

	}
	else
	{
		u1_printf(" Write CommunicationCount Fail\r\n");
	}
}

void WriteCommunicationSuccessCount(void)
{
	static unsigned char s_BuffData[20];
	
	if (Check_Area_Valid(COM_LOG_ADDR))
	{
		EEPROM_ReadBytes(COM_LOG_ADDR, s_BuffData, sizeof(COMLOG_CFG));
		
		p_ComLog = (COMLOG_CFG *)s_BuffData;
		
		ComLogConfig.Magic 				= VAILD;
		ComLogConfig.Length 			= sizeof(COMLOG_CFG) - 4;
		ComLogConfig.CommunicationCount = p_ComLog->CommunicationCount;
		ComLogConfig.SuccessCount 		= p_ComLog->SuccessCount;
		ComLogConfig.SuccessCount++;
		
		ComLogConfig.Chksum = Calc_Checksum((unsigned char *)&ComLogConfig.CommunicationCount, ComLogConfig.Length);
		EEPROM_WriteBytes(COM_LOG_ADDR, (unsigned char *)&ComLogConfig, sizeof(COMLOG_CFG));
		u1_printf("\r\n SuccessCount:%d\r\n", ComLogConfig.SuccessCount);

	}
	else
	{
		u1_printf(" Write SuccessCount Fail\r\n");
	}
}

void ClearCommunicationCount(void)
{
	EEPROM_EraseWords(COM_LOG_NUM);
	
	u1_printf("\r\n Clear Communication Log Count!\r\n");
}

unsigned short ReadPointerAddr(void)
{
	if (Check_Area_Valid(LOG_STORE_POINTER_ADDR))
	{
		EEPROM_ReadBytes(LOG_STORE_POINTER_ADDR, s_LogData, sizeof(LOGSTORE_CFG));
		
		pthis = (LOGSTORE_CFG *)s_LogData;
		LogStoteConfig = *pthis;
		
		return LogStoteConfig.PointerAddr;
	}
	else
	{
		return 999;
	}
}
void WritePointerAddr(unsigned short NewAddr)
{
	pthis->PointerAddr = NewAddr;
	pthis->Chksum = Calc_Checksum((unsigned char *)&pthis->PointerAddr, 2);
	EEPROM_WriteBytes(LOG_STORE_POINTER_ADDR, (unsigned char *)pthis, sizeof(LOGSTORE_CFG));
}

void WriteLogData(unsigned char *DataBuf)
{
	unsigned short PointerAddr;
	unsigned int AbsoluteAddress = 0;
	PointerAddr = ReadPointerAddr();
	
	if(PointerAddr == 999)
	{
		u1_printf(" Pointer Addr Err\r\n");
	}
	
	AbsoluteAddress = LOG_STORE_START_ADDR + PointerAddr*LOGBLOCKMAX;
	EEPROM_WriteBytes(AbsoluteAddress, (unsigned char *)DataBuf, DATA_MAX);
	
	u1_printf(" Write Log (%d)\r\n", PointerAddr);
	PointerAddr++;
	if(PointerAddr >= LOG_ROOM_MAX-1)
	{
		PointerAddr = 0;
	}
	WritePointerAddr(PointerAddr);
	
}
void StoreOperationalData(void)
{
	unsigned short Temp = 0, Ill = 0, Press = 0, Vol = 0;
	unsigned char StoreBuf[50], i = 0;
	
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	
	if(Sht31Status != SHT_ERROR)
	{
		Temp = Get_Temp_Value()*10;		
	}
	else
	{
		Temp = STORE_ERR_CODE;
	}
	
	if(MaxStatus != MAX_ERROR)
	{
		Ill  = Get_Ill_Value();
	}
	else
	{
		Ill = STORE_ERR_CODE;
	}	
	
	if(BmpStatus != BMP_ERROR)
	{
		Press = GetBMPPress()*10;
	}
	else
	{
		Press = STORE_ERR_CODE;
	}


	Vol = Get_Battery_Vol();
	
//	GetPosition(&f_Latitude, &f_Longitude);
	
	StoreBuf[i++] = RTC_DateStruct.RTC_Date;
	StoreBuf[i++] = RTC_TimeStructure.RTC_Hours;
	StoreBuf[i++] = RTC_TimeStructure.RTC_Minutes;
	StoreBuf[i++] = RTC_TimeStructure.RTC_Seconds;
	
//	val_float = encode_float(f_Latitude);	//Data
//	memcpy(&StoreBuf[i], (void *)&val_float, sizeof(float));
//	i += 4;
//	val_float = encode_float(f_Longitude);	//Data
//	memcpy(&StoreBuf[i], (void *)&val_float, sizeof(float));
//	i += 4;
	StoreBuf[i++] = s_LinkNetTime >> 8;
	StoreBuf[i++] = s_LinkNetTime&0xff;
//	StoreBuf[i++] = s_CSQValue >> 8;
//	StoreBuf[i++] = s_CSQValue&0xff;
	StoreBuf[i++] = Temp >> 8;
	StoreBuf[i++] = Temp&0xff;
	StoreBuf[i++] = Ill >> 8;
	StoreBuf[i++] = Ill&0xff;
	StoreBuf[i++] = Press >> 8;
	StoreBuf[i++] = Press&0xff;
	StoreBuf[i++] = Vol >> 8;
	StoreBuf[i++] = Vol&0xff;
	StoreBuf[i++] = s_LogErrCode >> 8;
	StoreBuf[i++] = s_LogErrCode&0xff;
	
	WriteLogData(StoreBuf);
	
	SetLogErrCode(LOG_CODE_CLEAR);
}

void ShowLogContent(void)
{
	unsigned short i;
	unsigned char Buf[DATA_MAX];
	float f_Latitude = 0, f_Longitude = 0;
	
	u1_printf(" Pointer Addr:%d\r\n", ReadPointerAddr());
	for(i=0; i<LOG_ROOM_MAX; i++)
	{
		if(i%30 == 0)
		{
			IWDG_ReloadCounter();
		}
		memset(Buf, 0, DATA_MAX);
		EEPROM_ReadBytes(LOG_STORE_START_ADDR + i*LOGBLOCKMAX, Buf, DATA_MAX);
		
		if(((Buf[12]) + Buf[13]))	//只是判断有无数据
		{
			u1_printf("%03d.(%d)[%02d:%02d:%02d] Time:%.2fs  Temp:%.1f  Ill:%d Pre:%.1f Vol:%d ", i, Buf[0], Buf[1], Buf[2], Buf[3], ((Buf[4]<<8) + Buf[5])/100.0 , ((Buf[6]<<8) + Buf[7])/10.0, ((Buf[8]<<8) + Buf[9]), ((Buf[10]<<8) + Buf[11])/10.0, ((Buf[12]<<8) + Buf[13]));
//			u1_printf("%03d.(%d)[%02d:%02d:%02d] CSQ:%d  Temp:%.1f  Ill:%d Pre:%.1f Vol:%d ", i, Buf[0], Buf[1], Buf[2], Buf[3], ((Buf[4]<<8) + Buf[5]) , ((Buf[6]<<8) + Buf[7])/10.0, ((Buf[8]<<8) + Buf[9]), ((Buf[10]<<8) + Buf[11])/10.0, ((Buf[12]<<8) + Buf[13]));

			memcpy(&f_Latitude, (void *)&Buf[4], sizeof(float));
			f_Latitude = encode_float(f_Latitude);
			memcpy(&f_Longitude, (void *)&Buf[8], sizeof(float));
			f_Longitude = encode_float(f_Longitude);
//			u1_printf("%03d.(%d)[%02d:%02d:%02d] Temp:%.1f Humi:%.1f Ill:%d Pre:%.1f Vol:%d ", i, Buf[0], Buf[1], Buf[2], Buf[3], f_Latitude, f_Longitude, ((Buf[12]<<8) + Buf[13]));
//			u1_printf("%03d.(%d)[%02d:%02d:%02d] Lo:%.6f,%.6f Vol:%d ", i, Buf[0], Buf[1], Buf[2], Buf[3], f_Latitude, f_Longitude, ((Buf[12]<<8) + Buf[13]));		
			if(Buf[15] &0x01)
			{
				u1_printf(" No Module |");
			}
			if(Buf[15] &0x02)
			{
				u1_printf(" Weak Signal |");
			}
			if(Buf[15] &0x04)
			{
				u1_printf(" Unattach |");
			}
			if(Buf[15] &0x08)
			{
				u1_printf(" Timeout |");
			}
			if(Buf[15] &0x10)
			{
				u1_printf(" Success |");
			}			
			if(Buf[15] &0x20)
			{
				u1_printf(" Reset |");
			}
			if(Buf[15] &0x40)
			{
				u1_printf(" No Server Ack |");
			}
			if(Buf[15] &0x80)
			{
				u1_printf(" No Card |");
			}

			if(Buf[14] &0x10)
			{
				u1_printf(" Retry |");
			}			
			if(Buf[14] &0x20)
			{
				u1_printf(" SensorErr |");
			}
			if(Buf[14] &0x40)
			{
				u1_printf(" MOBILE |");
			}
			if(Buf[14] &0x80)
			{
				u1_printf(" UNICOM |");
			}			
			if(Buf[14] &0x01)
			{
				u1_printf(" HardErr |");
			}
			if(Buf[14] &0x02)
			{
				u1_printf(" GPS OK |");
			}
			if(Buf[14] &0x04)
			{
				u1_printf(" GPS Fail |");
			}
			if(Buf[14] &0x08)
			{
				u1_printf(" No GPS |");
			}

			
			u1_printf("\r\n");
		}
	}
}


























