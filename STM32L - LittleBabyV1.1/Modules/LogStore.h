#ifndef		_LOGSTORE_H_
#define		_LOGSTORE_H_

#define		LOG_CODE_CLEAR				0XFF		//清错误码
#define		LOG_CODE_NOMODULE			0		//模块不存在
#define		LOG_CODE_WEAKSIGNAL			1		//信号弱
#define		LOG_CODE_UNATTACH			2		//附着不上
#define		LOG_CODE_TIMEOUT			3		//连接超时
#define		LOG_CODE_SUCCESS			4		//上报数据成功
#define		LOG_CODE_RESET				5		//复位标记
#define		LOG_CODE_NOSERVERACK		6		//没有服务器应答
#define		LOG_CODE_NO_CARD			7		//没有SIM卡

#define		LOG_CODE_HARDERR			8		//硬件错误
#define		LOG_CODE_GPS_SUCCESS		9		//定位成功
#define		LOG_CODE_GPS_FAIL			10		//定位失败
#define		LOG_CODE_NOGPS				11		//没有检测到GPS
#define		LOG_CODE_RETRY				12		//重试标记
#define		LOG_CODE_SENSOR_ERR			13		//传感器错误
#define		LOG_CODE_MOBILE				14		//营运商为移动
#define		LOG_CODE_UNICOM				15		//营运商为联通



void LogStorePointerInit(void);

void SetLogErrCode(unsigned short ErrCode);

void ClearLogErrCode(unsigned short ErrCode);

void StoreOperationalData(void);

void ShowLogContent(void);

void CommunicatinLogInit(void);	//通信断线记录初始化

void WriteCommunicationSuccessCount(void);

void WriteCommunicationCount(void);

void ClearCommunicationCount(void);

void SetCSQValue(unsigned short data);

void SetLinkNetTime(unsigned short Time);

unsigned short GetCSQValue(void);

unsigned short GetLinkNetTime(void);



#endif

