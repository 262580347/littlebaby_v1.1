#include "Protocol.h"
#include "main.h"

void Mod_Send_Ack(USART_TypeDef* USARTx, CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth)
{
	u16 nMsgLen, i;
	char str[256];
	unsigned char send_buf[128];
	unsigned char Pack[128];
	unsigned int len = 0;

	send_buf[len++] = hdr->protocol >> 8;
	send_buf[len++] = hdr->protocol;
	
	send_buf[len++] = ((hdr->device_id >> 24) | 0x10000000);
	send_buf[len++] = hdr->device_id >> 16;	
	send_buf[len++] = hdr->device_id >> 8;
	send_buf[len++] = hdr->device_id;	
	
	send_buf[len++]  = 1;
	
	send_buf[len++]  = hdr->seq_no >> 8;
	send_buf[len++]  = hdr->seq_no ;
	
	send_buf[len++]  = hdr->payload_len >> 8;
	send_buf[len++]  = hdr->payload_len;

	send_buf[len++] = hdr->cmd;									//命令字

//	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

    for(i=0; i<lenth; i++)	
		send_buf[len+i] = data[i];
		
	len += lenth;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, Pack, 120);		//数据包转义处理	
	USART2_DMA_Send(Pack, nMsgLen);	

	sprintf(str, "\r\n [发送服务端ACK] from %ld(%d) ->: ", (hdr->device_id)&0xfffffff, nMsgLen);
	Debug_Out(str);
	for(i=0;i<nMsgLen;i++)
	{
		sprintf(&str[i*3], "%02X ",Pack[i]);
	}
	Debug_Out(str);
	Debug_Out("\r\n");		
}

unsigned int Mod_Report_LowPower_Data(USART_TypeDef* USARTx)
{
	u16 nMsgLen;
	char str[768];
	static u16 seq;
	unsigned int send_len;
	unsigned int payload_len;
	u8 i, count, Packet[256], send_buf[256];
	CLOUD_HDR *hdr;
	unsigned char *ptr;
	uint16 val_u16, ch;
	float val_float;
	RTC_TimeTypeDef RTC_TimeStructure;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(PROTOCOL_CONTROL_ID);			//通信版本号
	hdr->device_id = swap_dword((p_sys->Device_ID)|0x10000000);	
	hdr->dir = 0;												  //方向
	hdr->seq_no = swap_word(seq);							  //包序号
	seq++;
	hdr->cmd = CMD_REPORT_D;									  //命令字

	ptr = (unsigned char *)&send_buf[sizeof(CLOUD_HDR)];
	ptr++;
	
	count = 7;
		
	ch = 1;		//空气温度
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(KQW_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(Get_Temp_Value());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);


	ch = 2;		//空气湿度
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(KQS_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(Get_Humi_Value());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	
	ch = 3;		//光照度
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(GZD_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(Get_Ill_Value());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	
	ch = 4;		//大气压力
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(AIR_PRESS_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(GetBMPPress());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	
	ch = 5;		//信号强度CSQ
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(CSQ_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(GetCSQValue());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	
	ch = 6;		//连接时长
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(LINKTIME_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(GetLinkNetTime()/100.0);	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	
	
	ch = 7;		//电池电量
	val_u16 = swap_word(ch);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(BAT_VOL);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_UINT16;//数据标识
	ptr++;
	val_u16 = swap_word(Get_Battery_Vol());	//数据
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	
	if(GetGPSDataFlag())
	{	
		if(GetLongitude() !=0 && GetLatitude() != 0)
		{
			ch = 8;
			val_u16 = swap_word(ch);		//通道
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			val_u16 = swap_word(LONGITUDE_ID);//类型
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			(*ptr) = MARK_FLOAT;	//数据标识
			ptr++;
			val_float = encode_float(GetLongitude());	//数据
			memcpy(ptr, (void *)&val_float, sizeof(float));
			ptr += sizeof(float);

			ch = 9;
			val_u16 = swap_word(ch);		//通道
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			val_u16 = swap_word(LATITUDE_ID);//类型
			memcpy(ptr, (void *)&val_u16, sizeof(uint16));
			ptr += sizeof(uint16);
			(*ptr) = MARK_FLOAT;	//数据标识
			ptr++;
			val_float = encode_float(GetLatitude());	//数据
			memcpy(ptr, (void *)&val_float, sizeof(float));
			ptr += sizeof(float);
			
			count = 9;	
		}		
	}
	
	send_buf[sizeof(CLOUD_HDR)] = count;
	send_len = ptr - send_buf;
    payload_len = send_len - sizeof(CLOUD_HDR);
	hdr->payload_len = swap_word(payload_len);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,256);
	//发送消息

	USART2_DMA_Send(Packet, nMsgLen);
	
	u1_printf("\r\n 温度:%2.2f℃ 湿度:%2.2f%% 光照:%.1fLux 压力:%2.2fhPa CSQ:%d 时间:%ds 电压:%dmV 经度:%f 纬度:%f\r\n", Get_Temp_Value(), Get_Humi_Value(), Get_Ill_Value(), GetBMPPress(), GetCSQValue(), GetLinkNetTime()/100, Get_Battery_Vol(), GetLongitude(), GetLatitude());
	
	sprintf(str, "\r\n [%02d:%02d:%02d][Mod][上报数据] %d -> %d(%d): ", RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, (p_sys->Device_ID)&0xfffffff, p_sys->Gateway, nMsgLen);
	Debug_Out(str);
	for(i=0;i<nMsgLen;i++)
	{
		sprintf(&str[i*3], "%02X ",Packet[i]);
	}
	Debug_Out(str);
	Debug_Out("\r\n");	
	return nMsgLen;	
}

void Mod_Send_Alive(USART_TypeDef* USARTx)
{
	u16 nMsgLen, i, CellVol;
	char str[256];
	unsigned char send_buf[64];
	unsigned char AlivePack[64];
	unsigned int len;
	static uint16 seq_no = 0;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(SENSOR_TYPE_COLLECTOR);							//通信协议版本号
	hdr->device_id = swap_dword((p_sys->Device_ID)|0x10000000);		//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(6);							//信息域长度
	hdr->cmd = CMD_HEATBEAT;									//命令字

	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

	//if(g_sys_ctrl.sys_info )
	//信息域
	CellVol = (Get_Battery_Vol()*2/100);	
	if(CellVol > 255)
		CellVol = 255;
	send_buf[len++] = 	(unsigned char)CellVol;	//电池电压，传输数据=电压*10;
	
	Int32ToArray(&send_buf[len], p_sys->Gateway);
	len +=4;
	send_buf[len++] = VAILD;	//收到网关数据时的信号强度

	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, AlivePack, 100);		//数据包转义处理	
	sprintf(str, "\r\n [心跳包] -> %d(%d): ", p_sys->Gateway, nMsgLen);
	Debug_Out(str);
	for(i=0;i<nMsgLen;i++)
	{
		sprintf(&str[i*3], "%02X ",AlivePack[i]);
	}
	Debug_Out(str);
	Debug_Out("\r\n");	

	USART2_DMA_Send(AlivePack, nMsgLen);	
}

//读取设备通信配置信息 --0x04 =========================================
unsigned int System_get_config(USART_TypeDef* USARTx, CLOUD_HDR *pMsg, unsigned char *data, unsigned int len)
{
	char str[256];
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	u16 nMsgLen, i;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *sys_cfg;
	COM_SYSTEM_CFG  *com_sys_cfg;
	
	
	memcpy(send_buf, pMsg, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->protocol = swap_word(hdr->protocol);	
	hdr->device_id = swap_dword(hdr->device_id);
	hdr->seq_no = swap_word(hdr->seq_no);	
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+1);
	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;
	com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID | 0x10000000);
	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_topology = sys_cfg->Topology;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);		
	
	send_len+=sizeof(SYSTEMCONFIG)+1;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息

	if(sys_cfg->Type == NETTYPE_LORA)
	{
		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
	}
	else
	{
		USART2_DMA_Send(Packet, nMsgLen);	
	}	
	
	u1_printf("[Mod][系统配置上报] -> %d (%d):", (sys_cfg->Device_ID)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", Packet[i]);
	}	
	u1_printf("\r\n");
	u1_printf("\r\n");	
	return 1;
}
//写入通道配置信息 0x05 ===============================================
unsigned int System_set_config(USART_TypeDef* USARTx, unsigned char *data, unsigned int len)
{
	char str[256];
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	u16 nMsgLen, i;
	
	CLOUD_HDR *hdr;
	unsigned char *ptr;
	
	SYSTEMCONFIG new_sys_cfg;
	SYSTEMCONFIG *sys_cfg;
	COM_SYSTEM_CFG *com_sys_cfg;
	
	uint16 val_u16;
	unsigned int val_u32;
	unsigned int payload_len;

	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	ptr = &data[sizeof(CLOUD_HDR)];
	hdr = (CLOUD_HDR *)data;
	
	payload_len = swap_word(hdr->payload_len);
	
	sys_cfg = GetSystemConfig();
	memcpy(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG) - 3);
//	new_sys_cfg.Device_ID = (new_sys_cfg.Device_ID)&0xfffffff;
	
	while(payload_len)
	{
		switch((*ptr))
		{
			case CFG_ID_1:	  	//系统信息分组ID
				ptr++;
				payload_len--;
				val_u32 = *(unsigned int *)ptr;
				new_sys_cfg.Device_ID = (swap_dword(val_u32)) & 0xfffffff ;
				ptr += sizeof(unsigned int);
				new_sys_cfg.Type = (*ptr);
				ptr++;
				payload_len-=5;

				u1_printf("[设置]:设备编号=%u,网络连接类型=%d\n",new_sys_cfg.Device_ID, new_sys_cfg.Type );
				if((new_sys_cfg.Type != NETTYPE_LORA) && (new_sys_cfg.Type != NETTYPE_SIM800C) && (new_sys_cfg.Type != NETTYPE_GPRS) )
				{
					u1_printf("\r\n 网络类型错误\r\n");
					return 0;
				}
				break;
			case CFG_ID_2: 	//Zigbee信息分组ID
				ptr++;
				payload_len--;
			
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Net = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Node = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gateway = swap_word(val_u16);
				ptr += sizeof(uint16);
				new_sys_cfg.Channel = (*ptr);
				ptr++;
				new_sys_cfg.Topology = (*ptr);
				ptr++;
				payload_len-=8;

				u1_printf("[设置]:Lora网络号=%u,节点=%d,主机=%d,频道=%d,拓扑=%d\n"
						,new_sys_cfg.Net
						,new_sys_cfg.Node
						,new_sys_cfg.Gateway
						,new_sys_cfg.Channel
						,new_sys_cfg.Topology
						 );
				if(new_sys_cfg.Channel > 32)
				{
					u1_printf("\r\n 频道错误\r\n");
					return 0;
				}		 
				if(new_sys_cfg.Gateway != new_sys_cfg.Net)
				{
					u1_printf("\r\n 网关号与网络号不符合\r\n");
					return 0;
				}
				
//				if(new_sys_cfg.Node != ((new_sys_cfg.Device_ID) & 0xfffffff))
//				{
//					u1_printf("\r\n 节点号与设备ID不符合\r\n");
//					return 0;
//				}
				break;
			case CFG_ID_3:		//GPRS分组信息ID
				ptr++;
				payload_len--;
			
				memcpy(new_sys_cfg.Gprs_ServerIP, ptr, 4);
				ptr += 4;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gprs_Port = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=6;

				u1_printf("[设置]:GPRS服务器地址=%u.%u.%u.%u,端口=%d\n"
						,(new_sys_cfg.Gprs_ServerIP[0])
						,(new_sys_cfg.Gprs_ServerIP[1])
						,(new_sys_cfg.Gprs_ServerIP[2])
						,(new_sys_cfg.Gprs_ServerIP[3])
						,new_sys_cfg.Gprs_Port
						 );
				break;
			case CFG_ID_4: 	//通信参数分组ID
				ptr++;
				payload_len--;
			
				new_sys_cfg.Retry_Times = (*ptr);
				ptr++;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Heart_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Data_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.State_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=7;

				u1_printf("[设置]:重发次数=%u.心跳间隔=%u.数据间隔=%u.状态间隔=%u\n"
						,new_sys_cfg.Retry_Times
						,new_sys_cfg.Heart_interval
						,new_sys_cfg.Data_interval
						,new_sys_cfg.State_interval
						 );
						 
				if(new_sys_cfg.Heart_interval > 30000)
				{
					u1_printf(" 心跳间隔超量程\r\n");
					return 0;
				}
				
				if(new_sys_cfg.Data_interval > 30000)
				{
					u1_printf(" 数据间隔超量程\r\n");
					return 0;
				}
				
				if(new_sys_cfg.State_interval > 30000)
				{
					u1_printf(" 状态间隔超量程\r\n");
					return 0;
				}
				
				break;
			default:
				ptr++;
				payload_len--;
				break;
		}
	}
	
	if (memcmp(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG) - 3))
	{
		u1_printf("\r\n 更新参数...");
		new_sys_cfg.AutoResetFlag = sys_cfg->AutoResetFlag;
		new_sys_cfg.AutoResetTime = sys_cfg->AutoResetTime;
		Set_System_Config(&new_sys_cfg);
	}
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
//	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+1);	//2019.4.29 增加重启时间配置
	send_len = sizeof(CLOUD_HDR);
	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;
	com_sys_cfg->device_id = swap_dword((sys_cfg->Device_ID) | 0x10000000);
	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_topology = sys_cfg->Topology;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);	
	
	send_len+=sizeof(SYSTEMCONFIG)+1;	//增加重启时间配置
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息
	u1_printf("[Mod][更新系统配置上报] -> %d (%d):",  (sys_cfg->Device_ID)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", send_buf[i]);
	}	
	u1_printf("\r\n");
	
	if(sys_cfg->Type == NETTYPE_LORA)
	{
		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
	}
	else
	{
		USART2_DMA_Send(Packet, nMsgLen);	
	}
	
	return 1;
}




