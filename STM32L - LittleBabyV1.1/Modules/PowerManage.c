#include "main.h"
#include "PowerManage.h"

#define	SDA_PIN		GPIO_Pin_9
#define	SDA_TYPE	GPIOB
#define	SCK_PIN		GPIO_Pin_8
#define	SCK_TYPE	GPIOB

#define		VOLBUFCOUNT		10

static unsigned short s_Battery_Vol = 0;

static float s_VolBuff[VOLBUFCOUNT];

unsigned char s_PowerDetectFlag = TRUE;

unsigned char s_BQ24650_EN_Flag = 1;

unsigned char g_PluseChannel_1_Flag = FALSE, g_PluseChannel_2_Flag = FALSE;

unsigned char s_ForcedChargeFlag = 0;

BAT_STATUS BatStatus;	//电池电压检测状态

u8 s_CfgData[sizeof(CHARGING_CFG)] ;

CHARGING_CFG	ChargingConfig;
CHARGING_CFG	*p_this = NULL;

CHARGING_CFG* GetChargingStatus(void)
{
	return p_this;
}

unsigned char GetForcedChargeFlag(void)
{
	return s_ForcedChargeFlag;
}

unsigned char GetBQ24650ENFlag(void)
{
	return s_BQ24650_EN_Flag;
}

void SetBQ24650ENFlag(unsigned char isTrue)
{
	s_BQ24650_EN_Flag = isTrue;
}

void StartGetBatVol(void)
{
	s_PowerDetectFlag = TRUE;
}

void ReadBQ24650Flag(void)
{		
	p_this = &ChargingConfig;
	
	BatStatus= BAT_IDLE;
	
	if (Check_Area_Valid(BQ_FLAG_ADDR))
	{
		EEPROM_ReadBytes(BQ_FLAG_ADDR, s_CfgData, sizeof(CHARGING_CFG));
		
		p_this = (CHARGING_CFG *)s_CfgData;
		
		ChargingConfig.Magic 		= p_this->Magic;
		ChargingConfig.Length 		= p_this->Length;
		ChargingConfig.Chksum 		= p_this->Chksum;
		ChargingConfig.Enable 		= p_this->Enable;
		ChargingConfig.ForceFlag 	= p_this->ForceFlag;
		
//		u1_printf("Cfg:%02X %02X %02X %02X %02X ", ChargingConfig.Magic, ChargingConfig.Length, ChargingConfig.Chksum , ChargingConfig.Enable, ChargingConfig.ForceFlag);
//		
//		u1_printf("\r\n 读取充电配置:%02X : %02X\r\n", ChargingConfig.Enable, ChargingConfig.ForceFlag);
	}
	else
	{
		ChargingConfig.Magic 		= VAILD;
		ChargingConfig.Length 		= 0X02;
		ChargingConfig.Enable 		= BQ24650_ENABLE_FLAG;
		ChargingConfig.ForceFlag 	= FORCED_CHARGE_DISABLE;
		ChargingConfig.Chksum = Calc_Checksum((unsigned char *)&ChargingConfig.Enable, 2);
		EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)&ChargingConfig, sizeof(ChargingConfig));
		u1_printf("\r\n 第一次初始化芯片，写入充电配置\r\n");
	}
	
	if(ChargingConfig.ForceFlag == FORCED_CHARGE_ENABLE)
	{
		s_ForcedChargeFlag = 1;
		Debug_Out("\r\n 强制充电开启\r\n");
	}
	else if(ChargingConfig.ForceFlag == FORCED_CHARGE_DISABLE)
	{
		s_ForcedChargeFlag = 0;
		Debug_Out("\r\n 强制充电关闭\r\n");
	}
	
	if(ChargingConfig.Enable == BQ24650_ENABLE_FLAG)
	{
		s_BQ24650_EN_Flag = 1;
		u1_printf("\r\n 开启充电\r\n");
	}
	else if(ChargingConfig.Enable == BQ24650_DISABLE_FLAG)
	{
		s_BQ24650_EN_Flag = 0;
		u1_printf("\r\n 关闭充电\r\n");
	}
	else
	{
		s_BQ24650_EN_Flag = 0;
		u1_printf("\r\n First\r\n");
	}	
}

unsigned short Get_Battery_Vol(void)
{
	if(s_BQ24650_EN_Flag)
	{
		return (s_Battery_Vol | 0x0001);
	}
	else
	{
		return (s_Battery_Vol & 0xfffe);		
	}
}

void Power_Vol_Detect(unsigned short nMain10ms)
{
	u16 ADCdata[10], i, Sum_ADCdata = 0, BatVol = 0;
	char str[256];
	static u16 s_LastTime = 0;
	static u8 s_State = 1, s_BatFullCount = 0, s_DetCount = 0, s_DetNum = 0;
	
	if(s_PowerDetectFlag == TRUE)
	{				
		switch(s_State)	
		{
			case 1:
				BatStatus = BAT_WORKING;
			
				Adc_Init();	

				Open_AdcChannel();
			
				s_LastTime = GetSystem10msCount();
				s_State++;
			break;
		
			case 2:
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 50)//等待传感器完成测量再测量电压值
				{
					RCC_HSICmd(ENABLE);
					
					RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
					
					/* Enable ADC1 */
					ADC_Cmd(ADC1, ENABLE);
					
					/* Wait until ADC1 ON status */
					while (ADC_GetFlagStatus(ADC1, ADC_FLAG_ADONS) == RESET)
					{
					}
					
					for(i=0; i<10; i++)
					{
						/* ADC1 regular channel5 or channel1 configuration */
						ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 1, ADC_SampleTime_384Cycles);
						
						/* Start ADC1 Software Conversion */
						ADC_SoftwareStartConv(ADC1);

						/* Wait until ADC Channel 5 or 1 end of conversion */
						while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET)
						{	
						}	
						
						ADCdata[i] = ADC_GetConversionValue(ADC1);
					}
							
					for(i=0; i<10; i++)
					{
						Sum_ADCdata += ADCdata[i];
					}
					
					Sum_ADCdata /= 10;
					
					BatVol = (Sum_ADCdata*4.0)*3300/4095; 
										
					if(s_DetCount < 3)
					{
						s_VolBuff[s_DetCount] = BatVol;
						s_Battery_Vol = BatVol;
					}
					else
					{
						s_VolBuff[s_DetNum] = BatVol;
						s_Battery_Vol = Mid_Filter(s_VolBuff, s_DetCount);
					}

					sprintf(str, "\r\n Vol:%dmV Avg:%dmV\r\n", BatVol, s_Battery_Vol);	
					Debug_Out(str);
					
					s_DetCount++;
					s_DetNum++;
					
					if(s_DetCount >= VOLBUFCOUNT)
					{
						s_DetCount = VOLBUFCOUNT;
					}
					
					if(s_DetNum >= VOLBUFCOUNT)
					{
						s_DetNum = 0;
					}	
					
					Adc_Reset();

					if(s_Battery_Vol < 8000)//电池需要充电
					{
						s_BQ24650_EN_Flag = 1;
					}
					
					if(s_Battery_Vol > 8400)
					{
						s_BatFullCount++;
					}
					else
					{
						s_BatFullCount = 0;
					}
					
					if(s_BatFullCount >= 3)		//连续3次唤醒检测到电压大于8.4V，关闭充电电路，防止出现芯片一直给电池充电的问题
					{
						printf("\r\n 电池电量过高，保护电池，切断充电\r\n");
						s_BatFullCount = 0;
						s_BQ24650_EN_Flag = 0;
					}
					
					if(GPIO_ReadInputDataBit(BAT_FULL_TYPE, BAT_FULL_PIN) == Bit_RESET)//电池已满
					{
						s_BQ24650_EN_Flag = 0;	//关闭充电芯片的唯一入口
						u1_printf("\r\n 电池已充满\r\n");
					}
														
					s_State++;
					
				}				
			break;
				
			case 3:
				s_PowerDetectFlag = FALSE;
			
				if(s_BQ24650_EN_Flag)
				{
					BQ24650_ENABLE();
					p_this->Enable = BQ24650_ENABLE_FLAG;
					p_this->Chksum = Calc_Checksum((unsigned char *)&p_this->Enable, 2);
					EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)p_this, sizeof(CHARGING_CFG));
				}
				else
				{
					BQ24650_DISABLE();
					p_this->Enable = BQ24650_DISABLE_FLAG;
					p_this->Chksum = Calc_Checksum((unsigned char *)&p_this->Enable, 2);
					EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)p_this, sizeof(CHARGING_CFG));
			//		u1_printf("\r\n 充电关闭\r\n");
				}
				
				BatStatus = BAT_IDLE;				
				s_State = 1;
			break;
		}
	}
}

void SensorPowerOn(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = SENSOR_POWER_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(SNESOR_POWER_TYPE, &GPIO_InitStructure);

	GPIO_SetBits(SNESOR_POWER_TYPE,SENSOR_POWER_PIN);	
	
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	
	
	GPIO_ResetBits(SDA_TYPE, GPIO_Pin_6);
	GPIO_ResetBits(SCK_TYPE, GPIO_Pin_7);
	GPIO_ResetBits(SDA_TYPE, SDA_PIN);
	GPIO_ResetBits(SCK_TYPE, SCK_PIN);
}

void SensorPowerOff(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = SENSOR_POWER_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(SNESOR_POWER_TYPE, &GPIO_InitStructure);

	GPIO_ResetBits(SNESOR_POWER_TYPE,SENSOR_POWER_PIN);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	
	
	GPIO_ResetBits(SDA_TYPE, GPIO_Pin_6);
	GPIO_ResetBits(SCK_TYPE, GPIO_Pin_7);
	GPIO_ResetBits(SDA_TYPE, SDA_PIN);
	GPIO_ResetBits(SCK_TYPE, SCK_PIN);
}

void SensorPowerReset(void)
{
	SensorPowerOff();
	
	u1_printf("\r\n SensorPowerOff...\r\n");
	
	delay_ms(500);
	
	SensorPowerOn();
	
	u1_printf("\r\n SensorPowerOn!\r\n");
	
	delay_ms(100);
}
