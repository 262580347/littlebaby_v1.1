#include	"main.h"
#include 	"TCProtocolManage.h"

#define		MAX_DATA_BUF  500

static unsigned char s_RecDataBuff[300]; 

static unsigned char s_ServerConfigCmdFlag = FALSE;	//服务端即将下发配置信息
static unsigned char s_DataPackFlag = FALSE;

static unsigned char s_TCModuleReadyFlag = FALSE;

static unsigned char s_LockinTimeFlag = FALSE;

static u8 s_TCProtocolRunFlag = FALSE;

COM_RECSTATUS	g_TCRecStatus;

void SetTCModuleReadyFlag(unsigned char isTrue)
{
	s_TCModuleReadyFlag = isTrue;
}

unsigned char GetTCModuleReadyFlag(void)
{
	return s_TCModuleReadyFlag;
}

void SetDataPackFlag(unsigned char isTrue)
{
	s_DataPackFlag = isTrue;
}

unsigned char GetDataPackFlag(void)
{
	return s_DataPackFlag;
}

void SetTCProtocolRunFlag(unsigned char isTrue)
{
	s_TCProtocolRunFlag = isTrue;
	Clear_Uart2Buff();
}

unsigned char GetServerConfigCmdFlag(void)
{
	return s_ServerConfigCmdFlag;
}

void SetServerConfigCmdFlag(unsigned char isTrue)
{
	s_ServerConfigCmdFlag = isTrue;
}

void SetLockinTimeFlag(unsigned char isTrue)
{
	s_LockinTimeFlag = isTrue;	
} 

unsigned char GetLockinTimeFlag(void)
{
	return s_LockinTimeFlag;
}

static void DecodeAppMsg(uint8 *pBuf,uint16 uLen,TC_COMM_MSG *pMsg)
{
    u32 uPoint = 0;
	
    pMsg->Protocol = pBuf[uPoint++];
    pMsg->Protocol = (pMsg->Protocol<<8)|pBuf[uPoint++];
	
	pMsg->DeviceID = pBuf[uPoint++];
    pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	
	pMsg->Dir   = pBuf[uPoint++];
	
	pMsg->Seq = pBuf[uPoint++];
    pMsg->Seq = (pMsg->Seq<<8)|pBuf[uPoint++];
	
    pMsg->Length = pBuf[uPoint++];
    pMsg->Length = (pMsg->Length<<8)|pBuf[uPoint++];
	
    pMsg->OPType   = pBuf[uPoint++];
	
    if(pMsg->Length > TC_DATA_SIZE)
    {
	    pMsg->Length = TC_DATA_SIZE;
    }
	memcpy((void *)pMsg->UserBuf,(void *)&pBuf[uPoint],pMsg->Length);
}

static BOOL TCProtocol(unsigned char *Data, unsigned short Length, TC_COMM_MSG *s_RxFrame)
{
	unsigned char PackBuff[100];
	uint16   PackLengthgth = 0, i;
	
	UnPackMsg(Data+1, Length-2, PackBuff, &PackLengthgth);	//解包

	if (PackBuff[(PackLengthgth)-1] == Calc_Checksum(PackBuff, (PackLengthgth)-1))
	{		
		DecodeAppMsg(PackBuff, PackLengthgth-1, s_RxFrame);
		return TRUE;
	}
	else
	{
		u1_printf("COM3:");
		for(i=0; i<PackLengthgth; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLengthgth)-1));
		return FALSE;
	}	
	
}
//协议栈0 应用报文接收观察者处理任务
void TaskForTCRxObser(void)
{
	static TC_COMM_MSG s_RxFrame;
	unsigned char PackBuff[MAX_DATA_BUF];
	u16 RecPackNum, i, PackStartCount = 0, PackEndCount = 0, RecLength = 0;
	static u8 s_PackCount = 0, s_PackStartAddr[10], s_PackEndAddr[10];
	
	if(g_Uart2RxFlag == TRUE)
	{	
		if(g_USART2_RX_BUF[0] == BOF_VAL && g_USART2_RX_BUF[g_USART2_RX_CNT-1] == EOF_VAL && g_USART2_RX_CNT >= 15)
		{
			//连包处理
			RecPackNum = g_USART2_RX_CNT;
			if(g_USART2_RX_CNT < MAX_DATA_BUF)
			{
				memcpy(PackBuff, g_USART2_RX_BUF, RecPackNum);
			}
			else
			{
				u1_printf(" Data Count Over\r\n");
				return;
			}
			
			PackStartCount = 0;
			PackEndCount = 0;
			
			for(i=0; i<RecPackNum; i++)
			{
				if(PackBuff[i] == 0x7E)	//寻找包头个数
				{
					s_PackStartAddr[PackStartCount] = i;
					PackStartCount++;
				}
				else if(PackBuff[i] == 0x21)	//寻找包尾个数
				{
					s_PackEndAddr[PackEndCount] = i;
					PackEndCount++;
				}
			}
			
			if(PackStartCount == PackEndCount)	//是完整的数据包
			{
				if(PackStartCount > 1)
				{
					u1_printf("\r\n %d Packs\r\n", PackStartCount);
				}
				s_PackCount = PackStartCount;
			}
			else
			{
				u1_printf(" PackStartCount != PackEndCount\r\n");
				return;
			}
			
			if(s_PackCount >= 1 && s_PackCount <= 10)	//最多处理10个数据连包
			{
				for(i=0; i<s_PackCount; i++)
				{
					memset(s_RecDataBuff, 0 ,sizeof(s_RecDataBuff));
					
//					memcpy(PackBuff+s_PackStartAddr[i]+1, s_PackEndAddr[i] - s_PackStartAddr[i] - 1, NB_RxdBuff, &PackLength);
					
					RecLength = s_PackEndAddr[i] - s_PackStartAddr[i] + 1;
					memcpy(s_RecDataBuff, PackBuff+s_PackStartAddr[i], RecLength);
					
					if(TCProtocol(s_RecDataBuff, RecLength, &s_RxFrame))
					{
						OnRecTCProtocol(USART2, &s_RxFrame);
					}
				}
			}

		}
		else
		{
			u1_printf("Err:");
			for(i=0; i<g_USART2_RX_CNT; i++)
			{
				u1_printf("%02X ", g_USART2_RX_BUF[i]);
			}
			u1_printf("\r\n");
			
			u1_printf("%s\r\n", g_USART2_RX_BUF);
				
		}

		Clear_Uart2Buff();
	}
}


void OnRecTCProtocol(USART_TypeDef* USARTx, TC_COMM_MSG *pMsg)
{
	u8 i, sendbuf[100], sendlenth, ReturnVal = 0;
	TC_COMM_MSG *hdr;
	u32 RTCTime = 0;
	static unsigned char s_UpdataTimeFlag = FALSE;
	RTC_TimeTypeDef RTC_TimeStructure;	
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	//解析协议包
	u1_printf("\r\n [%02d:%02d:%02d][TC]<-Version:%02d Device:(%d)%04d Dir:%X Seq:%d Len:%d CMD:%02X Data:", \
	RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->Protocol, ((pMsg->DeviceID)&0x10000000) >> 28, (pMsg->DeviceID)&0xfffffff, pMsg->Dir, pMsg->Seq, pMsg->Length, pMsg->OPType);
	for(i=0; i<pMsg->Length; i++)
		u1_printf("%02X ", pMsg->UserBuf[i]);
	u1_printf("\r\n");
	
	g_TCRecStatus.LastLinkTime = GetRTCSecond();
	
	if(pMsg->OPType == CMD_HEATBEAT)
	{
		g_TCRecStatus.AliveAckFlag = TRUE;
		g_TCRecStatus.AliveAckFlag = TRUE;
		
		RTC_TimeStructure.RTC_H12     = RTC_H12_AM;				
		RTC_TimeStructure.RTC_Hours   = pMsg->UserBuf[2];
		RTC_TimeStructure.RTC_Minutes = pMsg->UserBuf[1];
		RTC_TimeStructure.RTC_Seconds = pMsg->UserBuf[0];;
		u1_printf(" Time:[%02d:%02d:%02d]\r\n", pMsg->UserBuf[2], pMsg->UserBuf[1], pMsg->UserBuf[0]);
		RTCTime = pMsg->UserBuf[2]*3600 + pMsg->UserBuf[1]*60 + pMsg->UserBuf[0];
		
		if(RTCTime > GetRTCSecond())		//与服务器误差超过120S则同步服务器时间
		{
			if(DifferenceOfRTCTime(RTCTime, GetRTCSecond()) > 20)					
			{
				s_UpdataTimeFlag = TRUE;
			}
			else 
			{
				s_UpdataTimeFlag = FALSE;
			}
		}
		else
		{
			if(DifferenceOfRTCTime(GetRTCSecond(), RTCTime) > 20)
			{
				
				s_UpdataTimeFlag = TRUE;
			}
			else 
			{
				s_UpdataTimeFlag = FALSE;
			}
		}
		
		if(s_UpdataTimeFlag)
		{
			if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure)  != ERROR)
			{
				u1_printf(" ---Synchronize remote time successfully---\r\n");
				RTC_TimeShow();
				s_LockinTimeFlag = TRUE;
			}
			else
			{
				u1_printf(" Failed to synchronize remote time\r\n");
			}
		}
		else
		{
			u1_printf(" --Unsynchronized server time--\r\n");
		}
		
		g_TCRecStatus.LastLinkTime = GetRTCSecond();		//防止更新时间后重启
	}

	if(pMsg->OPType == CMD_REPORT_D)
	{
		u1_printf(" Received server report data reply, report data successfully\r\n");
		
		SetGetGPSDataFlag(FALSE);
		
		s_DataPackFlag = TRUE;
		
		g_TCRecStatus.DataAckFlag = TRUE;
	}
	
	if(pMsg->OPType == CMD_CLOSE_SOCKET)	//关闭Socket指令
	{		
		u1_printf(" Receive (%d) server close Socket command\r\n", ((int)pMsg->DeviceID)&0xfffffff);
		
		Mod_Send_Ack(USARTx, (CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
				
		delay_ms(100);		//等待发送完成
		
		s_ServerConfigCmdFlag = TRUE;	//收到关闭Socket指令，可以休眠
		
	}
	
	if(pMsg->OPType == CMD_GET_CONFIG)//获取设备配置
	{
		Mod_Send_Ack(USARTx, (CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);

		u1_printf("\r\n Receive a server fetch configuration instruction\r\n");	
		
		System_get_config(USARTx, (CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
	}
	
	if(pMsg->OPType == CMD_SET_CONFIG) //设置设备配置
	{	
		Mod_Send_Ack(USARTx, (CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);			
		u1_printf("\r\n Receive server modify configuration instructions\r\n");
		
		memcpy(sendbuf, pMsg, sizeof(CLOUD_HDR));
		sendlenth = sizeof(CLOUD_HDR);
		
		hdr = (TC_COMM_MSG *)sendbuf;
		hdr->Protocol = swap_word(hdr->Protocol);	
		hdr->DeviceID = swap_dword((hdr->DeviceID | 0x10000000));
		hdr->Seq = swap_word(hdr->Seq);	
		hdr->Dir = 1;
		hdr->Length = swap_word(sizeof(SYSTEMCONFIG)+4);
		memcpy(&sendbuf[sizeof(CLOUD_HDR)], pMsg->UserBuf, pMsg->Length);
		ReturnVal = System_set_config(USARTx, sendbuf, pMsg->Length + sendlenth);
		if(ReturnVal == 0)
		{
			return;
		}
		u1_printf("\r\n Updata Config\r\n");
		delay_ms(1500);
		delay_ms(1500);
		SetLogErrCode(LOG_CODE_RESET);
		StoreOperationalData();
		while (DMA_GetCurrDataCounter(DMA1_Channel4));
		Sys_Soft_Reset();
	}
}
	
void TCProtocolProcess(void)
{
	if(s_TCProtocolRunFlag)
	{
		TaskForTCRxObser();
	}
}
//static void AddTCRxObser(TCCOMMRXOBSER hRxObser)
//{
//	if(hRxObser!=NULL)
//	{
//		s_TCRxObser = hRxObser;
//		Task_Create(TaskForTCRxObser,1);				
//	}
//}

//static void RemoveTCRxObser(TCCOMMRXOBSER hRxObser)
//{
//	if(hRxObser!=NULL)
//	{
//		s_TCRxObser = hRxObser;
//		Task_Kill(TaskForTCRxObser);				
//	}
//}

//void TCCommunicationStart(void)
//{
//	AddTCRxObser(OnRecTCProtocol);
//}

//void TCCommunicationEnd(void)
//{
//	RemoveTCRxObser(OnRecTCProtocol);
//}
























